+++
title = "En vrac"
date = 2019-10-02T14:52:09+02:00
modified = 2025-01-04
draft = false
show_toc = true
+++

## Des liens

### Bash

- [Greg's Wiki, plein de choses sur bash assez poussées](https://mywiki.wooledge.org/)
- [Shellscript.sh](https://www.shellscript.sh/) : Des cours, des astuces, ...
- [Shell scripts matter](https://dev.to/thiht/shell-scripts-matter)
- [Pure bash bible](https://github.com/dylanaraps/pure-bash-bible)
- [Bash exit traps](http://redsymbol.net/articles/bash-exit-traps/)
- [Bash "strict" mode](http://redsymbol.net/articles/unofficial-bash-strict-mode/)
- [Collection de ressources Bash](https://www.cyberciti.biz/open-source/learning-bash-scripting-for-beginners/)
- [Du formatage Bash pour le PS1](https://misc.flogisoft.com/bash/tip_colors_and_formatting)

### Zabbix

- [Templates communautaires](https://github.com/zabbix/community-templates)
- [Opensource ICT Solutions : Scripts / Templates / Etc.](https://github.com/OpensourceICTSolutions/)

### Transfert / partage de gros fichiers

- [SwissTransfer](https://www.swisstransfer.com/fr) (Suisse)
- [TransferNow](https://www.transfernow.net/) (Français)

Plus d'informations : [Quelles alternatives à WeTransfer ?](https://news.gandi.net/fr/2022/01/quelles-alternatives-a-wetransfer/) sur le blog de Gandi.net

### Question sysadmin pour entretien + réponses

- [Test your sysadmin skills](https://github.com/trimstray/test-your-sysadmin-skills)
- [Autres questions pour entretien](https://dev.to/kaydacode/interview-questions-you-should-ask--3dkc)

### Libs pythons qui valent le coup

- [Zabbix Utils](https://github.com/zabbix/python-zabbix-utils)
- [PyZabbix](https://pypi.org/project/pyzabbix/)
- [Tableaux ascii](https://pypi.org/project/terminaltables/)
- [Couleur dans le terminal](https://pypi.org/project/colorclass/)

__Attention__, pour des variables faut utiliser le formatage à l'ancienne : `"{autored}%s{/autored}" % (ma_var)`

### Encore plus

- [Générateur de lorem ipsun en mode Cthulhu](https://ephemer.kapsi.fi/FhtagnGenerator.php?count=500&format=text&fhtagn=yes)
- [Le motarologue : 10 erreurs à ne pas faire quand on organise une balade à moto](/10-erreurs-balade-moto.txt)
- [Ma Check-list de Festival](/check-list-fest.txt)
- [Calculateur de Masque IPv4 et IPv6 du CNRS](https://cric.grenoble.cnrs.fr/Administrateurs/Outils/CalculMasque/)
- [IT TOOLS (petits outils en web)](https://it-tools.tech/)
- [Astuces sur Joplin](https://itsfoss.com/joplin-tips/)
- [One time Secret](https://onetimesecret.com/)

## Outils microsoft

- [sdelete](https://docs.microsoft.com/en-us/sysinternals/downloads/sdelete)

### Tester des ports avec PowerShell

Une sorte d'équivalent (léger) à netcat pour tester si une connexion réseau se fait bien avec une machine distante :

```powershell
Test-NetConnection -ComputerName hote_distant -Port port
```

Avec « hote_distant » un nom DNS ou une IP et « port » le numéro de port distant.

_Remarque_ : à priori disponible seulement à partir de Windows Server 2012.
Peut-être 2008, mais je n'ai pas encore pu tester.

## Cacher curseur bash ##

Si besoin de cacher le curseur, voici quelques lignes :

{{< highlight shell >}}
# Pour ne pas oublier de remettre le curseur dans son état initial à la sortie du script
function unhide_cursor() {
printf '\e[?25h'
}
trap unhide_cursor EXIT

# Cacher le curseur
printf '\e[?25l'

# ... Le script ici ...
{{< / highlight >}}

## Un prompt Bash qui se voit bien ##

[Explications sur Le hollandais volant](https://lehollandaisvolant.net/?d=2017/02/13/16/36/20-linux-rendre-le-terminal-plus-lisible)

La variable :

```shell
PS1="\n\[\e[1;48;5;31m\] \u \[\e[48;5;233m\] \h \[\e[48;5;240m\] \w \[\e[0m\]\$(__git_ps1) \$(__cmd_err_code)\n» "
```

Et puis la fonction :

```shell
__cmd_err_code(){
    local ERRCODE=$?
    if [[ ${ERRCODE} -ne 0 ]]; then
        echo -e " \e[1;31m${ERRCODE}\e[00m"
    else
        echo ""
    fi
}
```

Ce que ça peut donner :

- Exemple 1 :

{{< image src="bash_prompt_ex01.png" caption="Premier exemple de ce que ça peut donner" >}}

- Exemple 2 :

{{< image src="bash_prompt_ex02.png" caption="Second exemple de ce que ça peut donner" >}}

- Exemple 3 :

Un peu plus facétieux, puisqu'avec des caractères UTF-8 pour faire des petites icônes.
Rien de bien sorcier dans la variable :

```shell
PS1="\n\[\e[38;5;0m\]\[\e[48;5;32m\]  \u \[\e[00m\]\[\e[38;5;0m\]\[\e[48;5;11m\]   \w \[\e[00m\]\$(__git_ps1) \$(__cmd_err_code)\n» "
```

{{< image src="bash_prompt_ex03.png" caption="Troisième exemple de ce que ça peut donner" >}}

## Thème vim sympa : xoria256 ##

[Source](https://www.vim.org/scripts/script.php?script_id=2140)

{{< fold "Colorscheme xoria256" >}}
{{< highlight vim >}}
set background=dark
highlight clear
syntax reset
set t_Co=256
hi Normal       ctermfg=252 guifg=#d0d0d0 ctermbg=234 guibg=#1c1c1c cterm=none gui=none"}}}
hi Cursor                                 ctermbg=214 guibg=#ffaf00
hi CursorColumn                           ctermbg=238 guibg=#444444
hi CursorLine                             ctermbg=237 guibg=#3a3a3a cterm=none gui=none
hi ColorColumn  ctermbg=235 guibg=#2c2d27
hi Error        ctermfg=15  guifg=#ffffff ctermbg=1   guibg=#800000
hi ErrorMsg     ctermfg=15  guifg=#ffffff ctermbg=1   guibg=#800000
hi FoldColumn   ctermfg=247 guifg=#9e9e9e ctermbg=233 guibg=#121212
hi Folded       ctermfg=255 guifg=#eeeeee ctermbg=60  guibg=#5f5f87
hi IncSearch    ctermfg=0   guifg=#000000 ctermbg=223 guibg=#ffdfaf cterm=none gui=none
hi LineNr       ctermfg=247 guifg=#9e9e9e ctermbg=233 guibg=#121212
hi MatchParen   ctermfg=188 guifg=#dfdfdf ctermbg=68  guibg=#5f87df cterm=bold gui=bold
" TODO
" hi MoreMsg
hi NonText      ctermfg=247 guifg=#9e9e9e ctermbg=233 guibg=#121212 cterm=bold gui=bold
hi Pmenu        ctermfg=0   guifg=#000000 ctermbg=250 guibg=#bcbcbc
hi PmenuSel     ctermfg=255 guifg=#eeeeee ctermbg=243 guibg=#767676
hi PmenuSbar                              ctermbg=252 guibg=#d0d0d0
hi PmenuThumb   ctermfg=243 guifg=#767676
hi Search       ctermfg=0   guifg=#000000 ctermbg=149 guibg=#afdf5f
hi SignColumn   ctermfg=248 guifg=#a8a8a8
hi SpecialKey   ctermfg=77  guifg=#5fdf5f
 hi SpellBad     ctermfg=160 guifg=fg      ctermbg=bg                cterm=underline               guisp=#df0000
hi SpellBad      ctermfg=252                         ctermbg=160
hi SpellCap     ctermfg=189 guifg=#dfdfff ctermbg=bg  guibg=bg      cterm=underline gui=underline
hi SpellRare    ctermfg=168 guifg=#df5f87 ctermbg=bg  guibg=bg      cterm=underline gui=underline
hi SpellLocal   ctermfg=98  guifg=#875fdf ctermbg=bg  guibg=bg      cterm=underline gui=underline
hi StatusLine   ctermfg=15  guifg=#ffffff ctermbg=239 guibg=#4e4e4e cterm=bold gui=bold
hi StatusLineNC ctermfg=249 guifg=#b2b2b2 ctermbg=237 guibg=#3a3a3a cterm=none gui=none
hi StatusLineTerm ctermfg=15  guifg=#ffffff ctermbg=239 guibg=#4e4e4e cterm=bold gui=bold
hi StatusLineTermNC ctermfg=249 guifg=#b2b2b2 ctermbg=237 guibg=#3a3a3a cterm=none gui=none
hi TabLine  ctermfg=fg  guifg=fg      ctermbg=237 guibg=#3a3a3a cterm=none gui=none
hi TabLineSel   ctermfg=15 guifg=#9e9e9e ctermbg=233 guibg=#121212 cterm=bold gui=bold
hi TabLineFill  ctermfg=fg  guifg=fg      ctermbg=237 guibg=#3a3a3a cterm=none gui=none
" FIXME
hi Title        ctermfg=225 guifg=#ffdfff
hi Todo         ctermfg=0   guifg=#000000 ctermbg=184 guibg=#dfdf00
hi Underlined   ctermfg=39  guifg=#00afff                           cterm=underline gui=underline
hi VertSplit    ctermfg=237 guifg=#3a3a3a ctermbg=237 guibg=#3a3a3a cterm=none gui=none
" hi VIsualNOS    ctermfg=24  guifg=#005f87 ctermbg=153 guibg=#afdfff cterm=none gui=none
" hi Visual       ctermfg=24  guifg=#005f87 ctermbg=153 guibg=#afdfff
hi Visual       ctermfg=255 guifg=#eeeeee ctermbg=96  guibg=#875f87
" hi Visual       ctermfg=255 guifg=#eeeeee ctermbg=24  guibg=#005f87
hi VisualNOS    ctermfg=255 guifg=#eeeeee ctermbg=60  guibg=#5f5f87
hi WildMenu     ctermfg=0   guifg=#000000 ctermbg=150 guibg=#afdf87 cterm=bold gui=bold

"" Syntax highlighting {{{2
hi Comment      ctermfg=244 guifg=#808080
hi Constant     ctermfg=229 guifg=#ffffaf
hi Identifier   ctermfg=182 guifg=#dfafdf                           cterm=none
hi Ignore       ctermfg=238 guifg=#444444
hi Number       ctermfg=180 guifg=#dfaf87
hi PreProc      ctermfg=150 guifg=#afdf87
hi Special      ctermfg=174 guifg=#df8787
hi Statement    ctermfg=110 guifg=#87afdf                           cterm=none gui=none
hi Type         ctermfg=146 guifg=#afafdf                           cterm=none gui=none

"" Special {{{2
""" .diff {{{3
hi diffAdded    ctermfg=150 guifg=#afdf87
hi diffRemoved  ctermfg=174 guifg=#df8787
""" vimdiff {{{3
hi diffAdd      ctermfg=bg  guifg=bg      ctermbg=151 guibg=#afdfaf
"hi diffDelete   ctermfg=bg  guifg=bg      ctermbg=186 guibg=#dfdf87 cterm=none gui=none
hi diffDelete   ctermfg=bg  guifg=bg      ctermbg=246 guibg=#949494 cterm=none gui=none
hi diffChange   ctermfg=bg  guifg=bg      ctermbg=181 guibg=#dfafaf
hi diffText     ctermfg=bg  guifg=bg      ctermbg=174 guibg=#df8787 cterm=none gui=none
""" HTML {{{3
" hi htmlTag      ctermfg=146  guifg=#afafdf
" hi htmlEndTag   ctermfg=146  guifg=#afafdf
hi htmlTag      ctermfg=244
hi htmlEndTag   ctermfg=244
hi htmlArg	ctermfg=182  guifg=#dfafdf
hi htmlValue	ctermfg=187  guifg=#dfdfaf
hi htmlTitle	ctermfg=254  ctermbg=95
" hi htmlArg	ctermfg=146
" hi htmlTagName	ctermfg=146
" hi htmlString	ctermfg=187
""" XML {{{3
hi link xmlTagName	Statement
" hi link xmlTag		Comment
" hi link xmlEndTag	Statement
hi link xmlTag		xmlTagName
hi link xmlEndTag	xmlTag
hi link xmlAttrib	Identifier
""" django {{{3
hi djangoVarBlock ctermfg=180  guifg=#dfaf87
hi djangoTagBlock ctermfg=150  guifg=#afdf87
hi djangoStatement ctermfg=146  guifg=#afafdf
hi djangoFilter ctermfg=174  guifg=#df8787
""" python {{{3
hi pythonExceptions ctermfg=174
""" NERDTree {{{3
hi Directory      ctermfg=110  guifg=#87afdf
hi treeCWD        ctermfg=180  guifg=#dfaf87
hi treeClosable   ctermfg=174  guifg=#df8787
hi treeOpenable   ctermfg=150  guifg=#afdf87
hi treePart       ctermfg=244  guifg=#808080
hi treeDirSlash   ctermfg=244  guifg=#808080
hi treeLink       ctermfg=182  guifg=#dfafdf
""" rst #{{{3
hi link rstEmphasis Number

""" VimDebug {{{3
" FIXME
" you may want to set SignColumn highlight in your .vimrc
" :help sign
" :help SignColumn

" hi currentLine term=reverse cterm=reverse gui=reverse
" hi breakPoint  term=NONE    cterm=NONE    gui=NONE
" hi empty       term=NONE    cterm=NONE    gui=NONE

" sign define currentLine linehl=currentLine
" sign define breakPoint  linehl=breakPoint  text=>>
" sign define both        linehl=currentLine text=>>
" sign define empty       linehl=empty
""" vimHelp {{{3
hi link helpExample Number
hi link helpNumber String
hi helpURL ctermfg=110 guifg=#87afdf                           cterm=underline gui=underline
hi link helpHyperTextEntry helpURL
{{< / highlight >}}
{{< /fold >}}

## Thème Putty façon Zenburn ##

À mettre dans un fichier .reg :

{{< fold "putty.reg" >}}
{{< highlight registry >}}
Windows Registry Editor Version 5.00
[HKEY_CURRENT_USER\Software\SimonTatham\PuTTY\Sessions\Default%20Settings]
"Colour0"="220,220,204"
"Colour1"="220,220,204"
"Colour2"="58,58,58"
"Colour3"="58,58,58"
"Colour4"="0,13,24"
"Colour5"="143,175,159"
"Colour6"="0,0,0"
"Colour7"="85,85,85"
"Colour8"="215,135,135"
"Colour9"="215,135,135"
"Colour10"="160,192,160"
"Colour11"="160,192,160"
"Colour12"="255,255,135"
"Colour13"="255,255,135"
"Colour14"="101,121,142"
"Colour15"="101,121,142"
"Colour16"="241,140,150"
"Colour17"="241,140,150"
"Colour18"="140,208,211"
"Colour19"="140,208,211"
"Colour20"="255,255,255"
"Colour21"="255,255,255"
{{< / highlight >}}
{{< /fold >}}

## Pour activer le micro sur mon Pc portable ##

Un peu de contexte : Carte Realtek ALC255.
Bien reconnue par Alsa.
Prise micro / casque / micro en jack couplée tout en un.

Conf kernel que j'ai ajoutée avant avec `vim /etc/modprobe.d/alsa-base.conf` :

{{< highlight txt >}}
options snd_hda_intel index=0 model=alc255-acer
{{< / highlight >}}

L'installation des paquets `alsa-firmware` et `pavucontrol`

Puis reboot.

Après cela, les quelques commandes utiles que j'ai du passer :

{{< highlight shell >}}
# Identifier les éléments sources
pacmd list-sources
# Forcer le port (faudra changer le alsa_xxxx chez vous probablement)
pacmd set-source-port alsa_input.pci-0000_00_1f.3.analog-stereo analog-input-headset-mic
{{< / highlight >}}

Si toujours rien, vérifier (via `pavucontrol`) que le contrôle Capture n'est pas en sourdine.
D'ailleurs ce dernier peut aider à choisir le microphone du casque comme étant celui à choisir par défaut,
et couper l'interne.

Et **surtout** éviter de mettre le volume de capture à fond.

## Git

### Supprimer tous les vieux commits

Un nettoyage aggressif pour repartir de zéro :

```shell
git checkout --orphan temp
git add -A  # tout ajouter
git commit -m "nettoyage aggressif"
git branch -D master  # supprimer master
git branch -m master  # renommer temp en master
git push -f origin master  # Force push master
git gc --aggressive --prune=all     # virer les vieux fichiers
```

Petit point à vérifer : la branche principale est peut-être protégée contre le force-push sur le serveur (ou la forge).

### Supprimer la référence locale à une branche distante

Utile après qu'une fusion ait eu lieu sur le dépôt (après une _merge_/_pull_ request terminée),
histoire de ne plus avoir en local de vieilles références à des branches distantes qui n'existent plus.

```shell
git remote prune origin
```

_Remarque_ : `origin` est le nom de _remote_ par défaut,
mais si le nom est différent il faudra changer ça.

Pour vérifier :

```shell
git remote -v
# et aussi
git branch -a
```

## SSH : faire communiquer deux serveurs via son PC

En cas de firewall pas encore ouvert entre deux serveurs, une astuce via ssh :

- Ouvrir depuis son poste vers le serveur A une première session : `ssh -L 2022:127.0.0.1:22 user_srv_a@serveur_a`
- Ouvrir depuis son poste vers le serveur B une seconde session : `ssh -R 2022:127.0.0.1:2022 user_srv_b@serveur_b`
- Depuis le serveur B, ouvrir une session ssh sur le port choisi, en local, avec l'utilisateur du serveur A : `ssh -p 2022 user_srv_a@127.0.0.1`
- Tadaaam ! 

_Remarque_ : Le port 2022 est choisi arbitrairement.

{{< image src="double_tunnel_ssh.png" caption="Illustration d'un double tunnel ssh" >}}

## Auchan télécom : recevoir les MMS

La configuration de base avec la carte SIM n'est pas bonne (constaté en septembre 2024),
et il ne m'était pas possible de recevoir les MMS.
L'utilisation des données mobiles fonctionnait.

Pour corriger ça, utiliser cette configuration :

```txt
Nom : bouygues telecom
Apn : mmsbouygtel.com
Proxy : ne rien saisir
Port : ne rien saisir
Nom d’utilisateur : ne rien saisir
Mot de passe : ne rien saisir
Serveur : ne rien saisir
Mmsc : http://mms.bouyguestelecom.fr/mms/wapenc
Proxy mms : ne rien saisir
Port mms : ne rien saisir
Mcc : 208
Mnc : 20
Type d’authentification : pap
Type d’apn : default,supl,mms
Protocole apn : ipv6
Protocole d’itinérance apn : ipv4
Type d’opérateur du réseau virtuel mobile : Ne rien saisir

Effacer l'adresse proxy et le port mms afin de recevoir les MMS.
```

Astuce trouvée sur [la fibre.info](https://lafibre.info/mvno/auchan-telecom-problemes-mms-et-chat-rcs-a-cause-dune-nouvelle-sim/msg1091293/#msg1091293)
