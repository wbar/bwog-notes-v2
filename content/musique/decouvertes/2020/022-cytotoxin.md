+++
Categories = ["découvertes"]
title = "Cytotoxin"
date = 2020-09-04T18:37:07+02:00
draft = false
bands = ["Cytotoxin"]
+++

Il y a des événements graves qui laissent des marques indélébiles et inspirent des générations entières.
Là, on parle plus précisément du 26 avril 1986.
De l'explosion du réacteur numéro 4 de la centrale nucléaire de Tchernobyl.
24 ans plus tard apparait en Allemagne un groupe de << Chernobyl death metal >> : Cytotoxin.

Voici donc un groupe de Technical/Brutal Death Metal dont les paroles et l'univers gravitent autour de cette catastrophe.
Enfilez votre combinaison et votre masque, puis tentons d'explorer sans ramasser trop de radiations.

<!--more-->

Pour cette découverte, vu que le groupe n'a que 3 albums dont le dernier est sorti cette année, je les ai écoutés dans l'ordre de parution.

D'entrée de jeu, ça tabasse. Le groupe tient la promesse annoncée par le genre.
Au niveau des titres, ambiances sonores, visuels, etc.
Tout est fait pour coller au ton donné : les ravages dus à cette catastrophe.
L'immersion dans leur univers est donc facile.
Les constructions musicales sont typiques du Death, avec une alternance de schémas typiques du Brutal Death et de passages bien techniques
(pas mal de shred, mais pas que).
Au niveau du travail à la batterie, il me plait énormément.
Il est lourd et précis.
Sur le chant, nous avons une alternance entre du bon gros growl et du cri aigu importé du Grind[^1].
C'est toujours impressionnant le passage de l'un à l'autre quand il s'agit d'une seule et même personne au chant.
Et enfin, des petites surprises : il y a quelques passages d'ambiance qui permettent de ponctuer habilement les albums.


Entre le premier et le second album on assiste à une montée en gamme sur l'éventail des techniques et jeux déployés dans les compos, notamment la batterie.
Mais pour être honnête, sur les guitares aussi.
Le chant développe bien plus de growl classique que dans l'album précédent.
On a aussi droit à quelques ponts planant en mode atmosphérique, et puis BAM !
Ça repart !
La qualité a grimpé, et ça c'est cool.

Dans le dernier opus en date on note un travail surtout axé sur le mixage.
Le son est un peu plus métallique sur la batterie, on dirait un changement ou un ajout de fût.
C'est assez discret mais ça ajoute un petit quelque chose niveau ambiance.
Des petits passages plus Death Mélo qu'avant sont également à découvrir.
Globalement la qualité est toujours au rendez-vous.
On est sur un peaufinage, plus qu'une montée de niveau par rapport au second album.
La prod' est plus léchée, mais le changement est moins marquant qu'entre les deux premiers albums.
On sent que le groupe maitrise ce qu'il cherche à nous faire écouter.


Après cette découverte, je peux dire sans mentir que j'ai directement accroché à ce groupe !
C'est un genre de métal que j'aime beaucoup, mais dans lequel je connais assez peu de groupes pour l'instant.
Cytotoxine vient donc compléter ma liste de groupes, sans aucune question ni doute.

Si je le conseille ?
Aux personnes amatrices ou curieuses de Technical/Brutal Death oui carrément.
Pour mettre un pied dans ce genre, pas sûr, à moins d'être prêt(e) à en prendre plein les oreilles !

Échauffez bien votre nuque, vos épaules, votre dos.
Détachez vos cheveux et c'est parti pour du gros heabang dans le salon[^2] !
Pour les concerts (quand ça pourra reprendre ...) vous n'oublierez pas d'échauffer également bassin, jambes, genoux et chevilles !


Merci à [@Christ_OFF_](https://diaspodon.fr/@Christ_OFF_/104801411355037795) et [@Yahiko](https://framapiaf.org/@Yahiko/104801409542048736), pour la recommandation.

## Liens

- [Site officiel](https://www.cytotoxin.de/)
- [Page sur l'Encyclopaedia Metallum](https://www.metal-archives.com/bands/Cytotoxin/3540325917)


[^1]: Mais si vous savez de quoi je parle : le fameux cri de cochon. Que nos amis anglophones appellent poétiquement << pig squeal >>.
[^2]: Attention aux distances de sécurité avec les meubles !


