+++
Categories = ["découvertes"]
title = "Cellar Darling"
date = 2020-06-15T22:37:58+02:00
draft = false
bands = ["Cellar Darling"]
+++

Aujourd'hui nous allons parler d'un groupe qui mélange allègrement le métal folk, le prog' et un peu de heavy aussi : Cellar Darling.
[Via un pouet de @Aznorth](https://framapiaf.org/@Aznorth/104313339953061229) j'ai découvert un trio composé d'anciens de Eluveitie,
ce qui m'a tout de suite intrigué car je me demandais depuis longtemps ce que faisait maintenant Anna Murphy.
Honte à moi, je n'avais pas tilté au début que le groupe était aussi composé de Ivo Henzi et Merlin Sutter, respectivement guitariste et batteur partis en même temps que Anna.

Maintenant que les présentations sont faites, passons au vif du sujet : a-t-on un split bête et méchant de Eluveitie ou bien un projet bien différent ?
Indice, la réponse est évidente.
Pour un développement plus constructif, suivez votre serviteur.

<!--more-->

Le groupe a déjà sorti 2 albums, que j'ai écoutés dans l'ordre.
Voici donc mon ressenti, d'abord particulier puis global.

## This is The Sound

Or donc, voici le premier albums de 3 musiciens chevronnés, habitués du métal folk.
Éléments qu'on retrouve beaucoup, surtout le folk en appui du chant.
Le tout entrecoupés de bouts de prog' savamment distillés.
Au final cela donne un mélange dans les instrus de folks avec des ponts parfois heavy, de temps en temps death et donc prog, propres et agréables.
Une compo discrète, sobre, moderne, très efficace.

Au chant on sent un travail totalement différent de Anna sur sa voix par rapport à ce qu'elle faisait avant.
Par rapport aux habitudes de son timbre ça donne un renouveau intéressant avec un rendu de qualité.
Tantôt éthérée, tantôt puissant, mais pas agressif.
D'ailleurs elle semble moins forcer sur sa voix que lorsqu'elle chantait avec Eluveitie et cela donne une précision bienvenue dans les notes.
Détail amusant : mon oreille décèle quelques passages proches de ce que faisait Lacuna Coil.
Peut être une illusion auditive.

Au niveau des morceaux, chaque chanson a son univers, sa sonorité.
Et d'un titre un l'autre on se promène à travers les ressentis, les émotions.
Comme un voyage.
Des fois cela semble même emprunter au doom (voir la chanson Rebels par exemple), ce qui donne des passages lancinants, bien à contrepied d'autres chansons
Cela donne une œuvre qui propose donc beaucoup d'émotions différentes, sans tomber dans la facilité.

Petit bémol toutefois : j'ai noté parfois un mixage étrange.
Sur Under the Oak Tree par exemple, lors du pont de fin, la voix de Anna est très reculée par rapport aux instruments.
Je ne sais pas si c'est fait exprès ou non mais du coup ça fait un peu tâche vu la qualité globale de l'album.
Pour continuer dans les points négatifs, il y a aussi la toute dernière chanson, je l'ai beaucoup moins aimée que les autres.
La voix cristalline avec le piano puis la montée d'instruments en répétant tout le temps la même chose ... Bof bof ...


## The Spell

Sur cet album, on reprend les mêmes ingrédients que précédemment, mais mieux maitrisés et plus poussés.
J'ai noté un peu moins de puissance dans le chant, mais c'est à vérifier par des oreilles plus habituées que les miennes
car c'est plus une impression qu'une observation précise.

Techniquement c'est un album intéressant, mais je l'ai moins apprécié que le premier.
Notamment à cause du chant, mais aussi des des 2 derniers morceaux :
Le côté folk/prog laisse place à du piano et des sonorités typées sympho.
Et comme ce n'est pas ce que j'ai envie d'entendre pour un tel groupe, ça me sort de l'écoute et donc j'aime moins.

Cela dit, il faut reconnaitre l'effort de composition car les ambiances collent bien aux titres des morceaux.


## Mon avis global


J'étais fâché avec les chanteuses sans saturation / growl dans leur voix, mais je crois que je suis un peu réconcilié, en tout cas avec le travail de Cellar Darling.
A écouter et apprécier d'urgence ! Mais attention, il faut savoir se faire chambouler lors de l'écoute, car on sera souvent prit à contre-pied, ça ne sera pas tout le temps calme, pas toujours dynamique.
Le groupe oscille habilement entre les rythmes et les puissances dans la compo. Un régal.


## Liens

- [Site officiel](http://www.cellardarling.com/)
- [Page Wikipedia (en Anglais)](https://en.wikipedia.org/wiki/Cellar_Darling)

