+++
Categories = ["découvertes"]
title = "Forest Of Fog"
date = 2020-07-28T18:11:21+02:00
draft = false
bands = ["Forest Of Fog"]
+++

Projet Solo de Ivo Henzi qui a été initié en 2003, voici une collection d'albums de Black Metal qui pourraient passer inaperçus, et ça serait bien dommage.
Bien que Ivo soit l'ancien guitariste et compositeur chez Eluveitie et qu'il bosse aussi avec Cellar Darling,
on a là des éléments totalement éloignés de ce qu'il fait et a fait avec les autres groupes.

Alors du coup, ça donne quoi ?

<!--more-->

Pour me faire mon propre avis, j'ai procédé comme à mon habitude avec une méthode éprouvée et totalement infaillible :
une écoute du plus grand nombre de morceaux dans un ordre totalement pifométrique.

Dans tout ce joyeux bordel saturé appuyé à coups de bonne grosse batterie comme on les aime, il y a des morceaux purement instrumentaux.
Même ainsi ça tabasse bien, et sans les voix, on sent une envie de s'éclater avec juste les riffs et structures clichés typiques du black.
Le rendu est vraiment sympa.
On sent également des différences d'ambiances notables et bienvenues entre les morceaux d'un même album.
Globalement, il y a une espèce de fausse Lo-FI en mode hommage au vieux black metal.
Bien que dérangeante sur les plus vieux morceaux, elle s'estompe quand on progresse dans la discographie et ça en devient même une petite touche sonore sympatique.
Comme pour les morceaux sans chant, le fait de le renvoyer en arrière-plan permet un peu de s'en affranchir.
Les instruments sont du coup naturellement en premier plan et sont le point fort de Forest Of Fog.
    
Le résultat est donc un mélange de compos et voix à l'ancienne, mais avec un traitement moderne sur le son.
On oscille entre l'hommage au TRVE BLACK et l'envie de proposer une réinterprétation de ce qui se faisait aux débuts du style.
Dans tous les cas, on se retrouve avec des morceaux au tempo rapide et aux riffs diaboliques.
La voix d'outre tombe est également là pour appuyer le propos.
Plus quelques très bonnes surprises distillées ici et là.
L'ensemble forme une alchimie qui fonctionne à merveille sur moi. 
On est transporté pendant l'écoute et malgré le nom du groupe, tout est énergique. Nous aussi après l'écoute j'ai envie de dire.

Il n'y a que le dernier single, Awake, qui dénote complètement avec le reste.
Je dois admettre que je ne sais pas quoi dire dessus alors je vous laisse le découvrir et vous faire votre propre avis.

Du neuf avec du vieux donc.
Mais foncez, car même si ce n'est pas **LE** gros groupe de la décennie, vous passerez un bon moment.
En tout cas c'est ce qu'il s'est passé pour moi. 
Comme dirait l'autre : C'est d'la bonne !

Un grand merci à [@Aznorth](https://framapiaf.org/@Aznorth/104313450019860149) pour la découverte.

## Liens

- [Page de Forest Of Fog sur L'encyclopaedia Metallum](https://www.metal-archives.com/bands/Forest_of_Fog/17815)
- [Page Bandcamp](https://forestoffog.bandcamp.com/)


