+++
Categories = ["découvertes"]
title = "Vale of Pnath"
date = 2020-07-03T19:48:14+02:00
draft = false
bands = ["Vale of Pnath"]
+++


Aujourd'hui, j'avais envie d'écouter du technical death venant d'un groupe que je ne connais pas.
Il y a des jours comme ça, il vaut mieux ne pas chercher pourquoi.
En plus j'avais noté Vale Of Pnath depuis un petit moment (en plus de l'aguichage honteux fin mai).

Échauffez vos larynx, montez votre cardio si vous êtes à la batterie et mettez vos poteaux qui débutent la musique en PLS dans un coin,
nous plongeons dans un univers annoncé comme sombre, horrifique voire Lovecraftien.
Le tout saupoudré de virtuosité sur fond de gros tempo.

<!--more-->

Le groupe qui nous intéresse aujourd'hui en est à son 3e album.
Nous avons une discographie encore courte dont le premier élément date de 2011 (notons une démo en 2006 et un EP en 2008 tout de même) composée de 4 musiciens.
A priori le lineup a beaucoup changé, je vais donc essayer de voir l'évolution au fil des années, en essayant de ne pas trop comparer à Allegaeon ni Archspire.

## The Prodigal Empire

Pas le temps de niaiser, ça tabasse d'entrée de jeu.
C'est le minimum quand on veut du death technique, mais quand même.
Par contre, on se rend compte que les morceaux de ce 1er album me semblent un peu convenus.
C'est propre, pas de soucis là-dessus, mais il n'y a pas encore de démarcation particulière dans le son qui permet d'identifier le groupe rapidement à l'oreille.

Malgré tout, les mélodies sont agréables et prenantes.
Et vers la fin de l'album on commence à sentir une augmentation du niveau dans la compo, façon accroche pour les prochains albums.
La batterie appuie correctement les compos et est parfois même l'élément que l'on prendra plaisir à écouter le plus.
Quelques idées intéressantes trainent de-ci, de-là : comme des ponts (ou des intros) acoustiques,
façon scène de brume de vieux films d'épouvante ou un petit effet de voix << démoniaque >>.

On reste quand même devant un album de qualité, qui ne demande qu'à être un prélude à d'autres, bien plus forts.
À noter que du coup, pour faire découvrir le technical death en douceur, ça peut être un bon point d'entrée.


## II

Qu'est-ce qui se cache derrière ce nom cryptique ?

D'abord une petite intro acoustique dissonante, puis montée en puissance.
Ça s'annonce bien !
Ça tire un peu sur le black pour brouiller les pistes, puis on enchaine.
Le groupe n'hésite pas à prendre à contrepied  en ajoutant par exemple du piano (voire d'autres surprises si on tend bien l'oreille) pendant des passages calmes.
Globalement l'impression est bien meilleure que sur le premier album.

Au niveau du mixage, la voix semble avoir été mise un peu en retrait pour faire une plus grande place à l'instrumentation.
Ce qui est plutôt positif, car malheureusement je ne lui trouve rien de transcendant.
L'instrumentation se lâche bien plus qu'avant !
Les morceaux sont mieux construits, incitent bien plus à se laisser porter par la vague propre à chaque chanson.
Tantôt lourde et poisseuse, tantôt vive et puissante.

Une excellente évolution par rapport au premier album.

## Accursed

Et voici la fin de ce petit voyage, en la présence de leur dernier album en date.
L'attente est haute après ces 2 premiers opus.
Est-elle comblée ?
La sauce va prendre ou bien tout va s'effondrer comme un soufflé raté ?

Déjà, l'intro lourde et poisseuse laisse deviner une suite non moins intéressante.
Ensuite on sent que le côté percutant des morceaux est encore un cran au-dessus.

Mais au début de l'album on est quand même plus proche du black que du death technique.
On a aussi un changement de voix curieux, mais pas désagréable.
Le côté death technique reprend le dessus très vite dans les morceaux, mais on a toujours cette sorte de surcouche de black qui revient.
Moi ça me gêne pas car j'aime les deux styles, mais attention si vous cherchez du pur technical death

C'est tout ? Oui, l'album est très percutant, très rapide, très agréable, mais m'a semblé très court.
Avec les quelques changements artistiques qui s'entendent beaucoup, impossible pour moi de dire s'il est mieux construit que le précédent ou non.
Mais il est au moins tout aussi appréciable, et là aussi j'ai passé un très bon moment.

## Et du coup ?

Après avoir fait le tour de leur discographie, je peux dire que, même si pour beaucoup de morceaux la fin est brütale, rarement annoncée, C'est de la bonne !
Bien que typé technical death, je trouve que Vale of Pnath lorgne allègrement sur d'autres styles pour importer de temps à autre des idées de compo ou sonorités.
Ce qui donne de belles surprises.
L'ensemble n'est peut-être pas aussi poussé techniquement que d'autres groupes du style, mais là je pinaille et leur place dans ma liste est largement méritée.

Une excellente découverte pour ma part.
Vous pouvez foncer !
C'est du lourd !
Même si je recommande de ne commencer qu'à partir du second album, et de se méfier du 3e qui peut être déroutant.

## Liens

- [Page Bandcamp](https://valeofpnath.bandcamp.com/)
- [Page sur l'Encyclopaedia Metallum](https://www.metal-archives.com/bands/Vale_of_Pnath/3540304453)


