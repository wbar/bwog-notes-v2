+++
Author = "Chagratt"
categories = ["découvertes"]
title = "Découverte : Old Man's Child"
date = 2019-08-22T14:26:48+02:00
draft = false
bands = ["Old Man's Child"]
+++

En écoutant du Dimmu Borgir l'autre jour, j'ai eu comme suggestion Old Man's Child. Je me suis dit « Pourquoi pas ? », et je pense que j'ai bien fait.

J'ai découvert là un groupe de black métal moderne (avec des passages symphoniques), mené par Galder qui, surprise, joue actuellement avec Dimmu Borgir !
Hélas au détriment de son autre groupe, mais il parait que ce n'est qu'une pause. Quelque part c'est bien, ça laisse largement le temps de découvrir tranquillement ce groupe.

<!--more-->

## Les morceaux écoutés

Il ne m'en a pas fallu beaucoup pour me faire une idée (et adopter le groupe !) :

* Black Seeds On Virgin Soil (In Defiance of Existence)
* The Millennium King (The Pagan Prosperity)
* Felonies of the Christian Art (In Defiance of Existence)
* Towards Eternity (III-Natured Spiritual Invasion)
* Agony of Fallen Grace (In Defiance of Existence)
* In Defiance of Existence (In Defiance of Existence)
* War of Fidelity (Vermin)
* Path of Destruction (Slaves of the World)
* Unholy Foreign Crusade (Slaves of the World)
* On the Devil's Throne (Slaves of the World)

## Mon impression

On trouve des compos intégrant des changements de rythmes et de hauter dans les morceaux.
Des riffs et arpèges mélodiques, avec une teinte dissonnante intéressante qui prend carrément l'oreille en contre-pied sur certains morceaux.
On sent clairement l'influence du death, ce qui donne des construction percutantes.

Et malgré tous ces éléments, le résultat est propre, et même la voix est très bien intégrée au son global du groupe. Chapeau !
Voix qui a son propre ton et grain[^1], ce que je trouve assez rare sur les side-projects.
Ajoutez à ça un synthé au son froid et savamment dosé, et vous obtenez un excellent complément à l'ambiance globale.
Bref ! Tout pour une ambiance qui porte.
Pas aussi pesante que d'autres groupes de black, certes, mais c'est normal, c'est du black sympho.

Old Man's Child complète bien le paysage du genre à mon sens, et arrive à garder un poids dans le son qui est nécessaire au black (et qu'on recherche généralement à l'écoute).

Forcément, on est pas au niveau d'un Dark Throne, Satyricon ou Mayhem, qui sont des poids-lourds, mais ce n'est pas grave car ce n'est pas la même finalité sonore qui est explorée par ces groupes. Et tant mieux ! Ca permet de varier les ambiances en fonction des humeurs !

## Verdict ?

Si vous êtes un peu curieux du black métal mais avez peur du « pur » black[^2] foncez, c'est d'la bonne !
Les amateurs de death « léger » apprécieront aussi je pense.

## Liens

La page du groupe [sur L'encyclopaedia metallum](https://www.metal-archives.com/bands/Old_Man's_Child/362)

[^1]: Bon, ok, pas sur les toutes premières chansons, mais c'est normal ça ...
[^2]: Comprendre ici le black **pas** symphonique.

