+++
categories = ["découvertes"]
title = "Découverte : Rimfrost"
date = 2019-12-04T07:30:00+02:00
draft = false
bands = ["Rimfrost"]
+++

Power trio Suédois formé en 2002, voici Rimfrost. Le groupe nous a gratifié de son 4è album (Exepdition: Darkness) cette année.
Trouvé grâce à [un pouet de @il sur Mastodon](https://framapiaf.org/@il/103214923216523539),
me voici face à une nouveauté qui ne me laisse pas insensible.

<!--more-->

## Morceaux écoutés

* Veraldar Nagli (sur Veraldar Nagli)
* The Black Death (sur Veraldar Nagli)
* The Raventhrone (sur Veraldar Nagli)
* Damned Jaws (sur Expedition: Darkness)
* Dawnbreaker (sur Expedition: Darkness)
* Expedition: Darkness (sur Expedition: Darkness)
* Natten (sur Expedition: Darkness)
* As The Silver Curtain Closes (sur Rimfrost)
* Saga North (sur Rimfrost)

## Ce que ça donne de mon point de vue

On se retrouve là avec un groupe qui mélange allègrement Black, Thrash et ... Heavy[^1].
C'est pas banal !
Voire « Mais c'est n'importe quoi ça ! » si on s'attarde uniquement à la case du genre.
Case allègrement explosée par le groupe pour mon plus grand plaisir, et pourquoi pas le votre.

En tout cas, même si le line-up a été très changeant pour la basse (comme par hasard ...),
les fondateurs Throllv Väeshiin et Hravn Decmiester sont là depuis le lancement du groupe en 2002.
Je n'ai pas pu trouver beaucoup d'infos sur ces musiciens lors de la rédaction de cet article, mais on sent qu'ils n'en sont pas à leur premier groupe,
ou en tout cas pas à leur première expérience musicale.
C'est carré, ça s'enchaine bien et le mixage est satisfaisant, on est loin de la démo réalisée au fond d'un garage avec un seul micro pour tout le monde.
Je me suis facilement laissé emporté par l'univers proposé croisant du Immortal,
Emperor et un grain de folie qui vient greffer des riffs à la Dio, Iron Maiden voire Judas Priest.
Fallait oser !
Et personnellement je suis très content qu'ils aient osé !

Une mise en garde toutefois car c'est un groupe au son finalement bâtard.
Les morceaux sont rarement totalement orientés Black, ou pur Heavy.
Je vous le déconseille donc si vous êtes à fond dans un seul style par groupe et n'aimez pas les mélanges agressifs.
A noter quand même que ces mélanges ne sont pas faits sur toutes les chansons.
Il y en a quand même certaines qui sont entièrement typées Black, avec cependant un son beaucoup moins sombre que chez beaucoup d'autres groupes.
On dirait une sorte de petit frère fou de Immortal, qui aime beaucoup trainer dans les autres rayons du disquaire.

Allez hop ! Un groupe de plus à ma liste.
Et s'ils passent à un fest où je suis ou passent dans un concert près de chez moi, je pense que j'assisterai à leur prestation avec plaisir.
Ou tout du moins avec une grosse curiosité.

## Liens

La page du groupe [sur L'encyclopaedia metallum](https://www.metal-archives.com/bands/Rimfrost/22858)

[^1]: Avec de temps à autre des cris façon Glam' ... Ou bien comme le faisait parfois Ihsahn pour Emperor. Surprenant !

