+++
title = "Wintersun : Time II"
date = 2024-09-20T07:25:49+02:00
draft = false
bands = ["Wintersun"]
+++

Après 12 ans d'attente depuis Time I et un excellent The Forest Seasons pour patienter,
Wintersun est de retour avec un album très attendu de 48 minutes : Time II !

Est-ce que cette attente valait le coup ?
Mettons la galette dans le lecteur, et c'est parti pour le découvrir !

<!--more-->

Dès l'ouverture, nous sommes en terrain connu.
La première piste commence doucement, mais monte petit à petit en intensité.
Le tout sur 4 minutes pour nous préparer au premier véritable morceau.
On retrouvera également les sonorités asiatiques.
Pour ces dernières je me pose encore la question de savoir si elles sont électroniques ou produites par de véritables instruments.

Comparé aux anciens albums, pour le second titre on a là une coupure franche avec l'intro.
A peine le temps de se rendre compte de cette non-transition que ça s'envole.
Guitares, synthé, batterie à un rythme effréné et puis ... Le chant !
Bienvenue dans Time II, et cette fois, on va parler du feu.
La coupure avec l'instrumentale d'intro est plutôt habile finalement, car elle rend le morceau indépendant.
Au prix cela dit de la continuité qu'on aurait attendue en commençant l'écoute directement par la première piste.
Musicalement, on est en terrain connu.
Vous aimiez les tempos rapides, les longs ponts, et les moments de virtuosité ? Vous allez avoir tout cela.
Pour l'instant, on reste en terrain connu, c'est bien un album de Wintersun que nous sommes en train d'écouter. Et tant mieux !
Concernant le lien entre le titre du morceau et les sonorités, je ne le vois pas tant que ça.
On est bien sur des sonorités un poil plus chaudes qu'habituellement, mais globalement le son de Wintersun reste froid. Il ne faut pas oublier que c'est leur marque de fabrique.

La troisième piste débute lentement, avec une couleur très sombre, et un tempo lent.
Et ça tombe bien, il est titré « One With The Shadows ». On est dans le thème, mi-épique, mi-mélancolique.
Notons que le groupe nous a largement habitués à cette impression.
Ici, la basse est particulièrement présente sans avoir besoin de tendre l'oreille, mais sans déborder non plus.
Un très bon mixage.

Ce 4è morceau est une transition entre la fin du second et le prochain qui arrive.
Globalement, il se suffit à lui-même.
Mais pour moi il aurait largement pu avoir sa place sur la même piste que le troisième.
Cela dit, je vois totalement pourquoi cette partie a été mise à part.
Elle prépare au morceau suivant.

Morceau qui enchaîne là aussi directement avec le précédent, tout en pouvant être indépendant si l'on écoute au quotidien son audiothèque en mode aléatoire.
Nous entrons ici dans le domaine de la tempête.
Avec le calme des débuts cachant justement le déchainement proche.
On continue avec un tempo très rapide et une batterie qui revient prendre beaucoup de place.
Les guitares sont légèrement en retrait, mais c'est pour mieux laisser place à des choeurs qui ont toute leur place ici.
Un très bon morceau, qui se termine par un passage illustrant très bien la fin de la tempête, et le retour du beau temps.
Pré-transition pour le prochain morceau ? Le côté surprenant est d'avoir ce passage en fin de piste et pas dans une autre comme cela a été fait juste avant.

La réponse à la question est : oui. C'était une pré-transition. On retrouve là les sonorités asiatiques de l'intro pour ce dernier morceau.
Le titre ne m'évoque rien, mais on est ici dans le calme et la contemplation.
Un paysage montagneux épique qui s'ensoleille.
On pourrait croire à une outtro pour l'album, mais à 3 minutes et 31 secondes on se rend compte qu'il ne s'agit pas de ça.
Il faudra patienter un peu pour que les guitares arrivent, puis que le chant se lance.
Ce morceau plutôt aérien semble être une ode au passage du temps, des saisons, de la météo, et de ses effets sur le monde, ainsi que sur nous.

Et voilà, c'est terminé !

Conclusion ?
C'est du Wintersun.
Un peu brut et peu argumenté, mais il n'en faut pas plus.
La qualité est constante, comme le line-up.
D'un point de vue simpliste, le temps passé à écouter l'album est passé tout seul,
comme pour les autres albums lorsque je les ai découverts.

Est-ce que ça valait le coup d'attendre ?
Je pense que oui.
Ça n'aurait rien apporté de sortir l'album plus vite, et nous avions largement de quoi attendre.

Vous aimez Wintersun ? Vous aimerez très certainement ce nouvel album.

