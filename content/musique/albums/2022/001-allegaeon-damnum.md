+++
Categories = ["albums"]
title = "Allegaeon : Damnum"
date = 2022-03-03T08:27:23+01:00
bands = ["Allegaeon"]
+++

Quand j'ai découvert Allegaeon par hasard, j'étais loin de me douter que ce groupe allait devenir une de mes références en matière de Death Technique (avec des gros morceaux de mélodique dedans).
Ayant poncé et aimé leur discographie complète,
malgré un changement quasi-complet de line-up (il ne reste que le guitariste [Greg Burgess](https://www.metal-archives.com/artists/Greg_Burgess/7135) de la formation originale),
j'ai complètement loupé l'actualité du groupe.

Me voilà donc très surpris de voir un nouvel album arriver.
Je ne pouvais pas laisser passer ça.

Échauffez votre nuque, éloignez tout ce qui va gêner dans les mouvements, et plongeons dans ce nouvel opus musical.

<!--more-->

Le groupe a rajouté des orchestrations à la guitare acoustique depuis quelques albums,
et d'entrée de jeu on sent qu'ils vont continuer dans cette voie.
Ça permet quelques variations sympas et rajoute un côté « construit » aux morceaux, mais ça diminue le côté brutal qu'on avait avant.
Même s'il faut se rappeler qu'Allegaeon a toujours posé des passages et ambiances assez aériens dans leurs compos.

Il y a bien plus de chant clair qu'avant.
À ces moments-là, ça ressemblen à du Opeth mais en moins bien par contre.
C'est dommage car le chanteur actuel est capable de bons growls.
Fun fact, sur un des morceaux j'ai cru que le chanteur d'Archspire était de passage. :D

Je note également qu'il y a moins de gros solos ou de structures complexes qu'avant.
On peut parfois noter la présence de petites folies. Cet album est surtout dans la grosse structure rythmique.

Le boulot sur la batterie est plus précis qu'avant, plus dense, mais semble moins varié aussi.
À noter que le batteur semble avoir rejoint le groupe en 2021.
Il doit être en train de se caler par rapport au groupe, ça explique peut-être la différence de jeu.

Je n'ai pas compris le morceau « Called home », je n'ai même pas eu l'envie de l'écouter jusqu'au bout.
C'est bien la première fois que ça m'arrive avec le groupe.
Heureusement, la chanson « Saturnine » est l'excellente surprise qui vient peu de temps après.
C'est personnellement pour ce genre de morceaux que j'aime Allegaeon.

Conclusion mitigée sur cet album pour moi malheureusement.
Le son typique du groupe est toujours là et l'album reste globalement bon mais il est en vraiment dessous des autres pour moi.
Quelque part, heureusement que je ne l'attendais pas et que je l'ai vu par hasard.
Je pense que j'aurais été bien plus déçu sinon.

## Liens

- [Page du groupe sur l'Encyclopaedia Metallum](https://www.metal-archives.com/bands/Allegaeon/3540261016)

