+++
title = "Wolves In The Throne Room : Primordial Arcana"
date = 2022-07-08T11:30:15+02:00
bands = ["Wolves In The Throne Room "]
+++

Sorti le 20 aout 2021 (ne me jugez pas !), voici venu le 7e album de WITTR.

Avec ce dernier-né d'une discographie qui n'a fait que monter en puissance et ambiance de mon point de vue,
je vous propose de découvrir comment le groupe s'en sort, quelles sont les promesses de cet album, et si elles sont tenues.
Éteignez la lumière, fermez les volets, et plongeons !

<!--more-->

Au tout début, le groupe lance un petit sample pour lancer une ambiance.
Petit échauffement en vérité, car très rapidement, la batterie arrive, suivi par un autre son, puis la guitare.
Enfin, tout s'emballe, et avant même l'arrivée du chant, qui est accompagné d'une petite transition dans le rythme, nous le savons : nous y sommes !
Le début du voyage musical.

Mi-lourd, mi-onirique, l'album débute très bien et transporte immédiatement, comme lors des anciens morceaux.
Alternance de sombre relativement lourd et de gris-bleuté plus léger, cette entrée en matière est très agréable.
Quoique qu'un peu perturbante, puisque les couplets s'enchainent, sans réellement se ressembler.
Ce qui est un très bon point selon moi.
Cela dit, on reste en terrain connu si l'on a l'habitude d'écouter WITTR.

Après une fin brusque, voici le second morceau.
Celui-ci propose une autre couleur sombre par rapport à la première piste, avec les mêmes enchainements non linéaires entre les couplets.
Dit ainsi, on pourrait croire que le groupe a copié ses partitions sans s'embêter afin de se faciliter la tâche, 
mais la composition est belle et bien différente.
Ce second morceau, bien que plus long d'une trentaine de secondes semble par contre se dérouler plus vite que le précédent.

Voici maintenant venu le 3è morceau dont le début me fait un peu penser à du Alcest.
Mais dès que la voix arrive, et une fois passé 1 minute 30, non, c'est bon, le lecteur n'est pas parti chercher une chanson qui n'a rien à voir.
Ici, nous pouvons nous poser par rapport au début de l'album.
Une ambiance lancinante nous traine, avec des effets rajoutant de la lourdeur ressentie, mais pourtant entrainante.
Un peu comme un morceau de Primordial plus lent et dépressif à souhait.
La patte de WITTR est clairement là et reprend rapidement le dessus, même si je viens de citer deux autres groupes.
Encore une fois, dans l'enchainement des couplets et leur articulation, nos oreilles sont prises au dépourvu car il y a des ponts auxquels on ne s'attend pas, même en connaissant bien le groupe.

Encore un changement d'ambiance et surtout de couleur pour cette 4e piste.
Avec néanmoins un fil conducteur, déjà au niveau du chant, mais également dans les enchainements imprévisibles.
On pourra noter également les apparitions aussi soudaines que courtes d'autres instruments en fond, qui viennent ajouter un peu de profondeur aux compositions.

Ce n'est que vers le milieu de l'album par contre que l'on a enfin un semblant de continuité dans l'univers distillé au travers des chansons.
Cependant, l'enchainement continue de se faire naturellement lors de la poursuite de l'écoute.
Le plaisir de l'écoute et du voyage est toujours là !

Un virage s'opère ensuite, brisant l'impression de continuité à travers les morceaux.
Tout en continuant de nous transporter, la suite de l'album rompt avec le lien entre les chansons mais tout en continuant le même type d'enchaînement.
Difficile à décrire donc, continuité ou rupture ?
Un peu des deux.
C'est toujours le même album, les chansons continuent de changer brusquement, elles gardent donc leur fil conducteur, celui qui consiste à nous prendre en contre-pied entre deux couplets.
Passé cette étrange impression, on se rend compte que le plaisir de l'écoute est toujours là.
Amplifié par un désir et une curiosité de savoir comment le groupe jouer avec nous dans les morceaux restants.

Finalement, à la fin de l'album, après ne pas avoir trop compris comment les morceaux s'imbriquaient entre eux,
je me suis penché sur les titres.
Et je me suis rendu compte qu'ils collaient parfaitement avec l'ambiance proposée à chaque fois.
En fait, ce n'est peut-être pas une histoire en un bloc, mais un ensemble de nouvelles sur un thème commun.
Le voilà, le vrai fil conducteur, celui qui explique les enchainements et les ruptures franches entre deux chansons.
Dommage de n'avoir compris ça qu'à la fin.

Un excellent album sur lequel j'ai passé un très bon moment.
Je recommande chaudement !

