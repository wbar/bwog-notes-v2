+++
categories = ["albums"]
title = "Borknagar : True North"
date = 2019-10-15T21:59:07+02:00
draft = false
bands = ["Borknagar"]
+++

Seconde critique sur ce blog.
Pour moi, elle est un peu plus complexe à rédiger vu que j'ai du mal à comprendre ce que je ressens à l'écoute de cet album.

Je vous souhaite néanmoins une bonne lecture.

<!--more-->

## Ma découverte du groupe

J'ai connu Borknagar à un festival, le Ragnarök Festival, qui se tient généralement début avril, en Allemagne.
C'était en 2014.
Ma première édition pour ce fest.
Et, hnonêtement, c'était une excellente découverte.
Le son était impeccable, et l'ambiance, totalement au rendez-vous.
Forcément, il y a de quoi être conquis.

Le groupe possède un classement un peut batard, un mélange de black/folk, avec quelques bouts de progressif[^1].

## Depuis ce jour

J'ai beaucoup écouté leur discographie.
J'aime plutôt bien ce qu'ils font, sans être fanboy acharné, mais la plupart du temps, ça me porte.
J'apprécie l'écoute régulière, ça me fait passer le temps (surtout dans les transports) et je trouvais leur façon de faire intéressante.

Il faut avouer aussi, que leur avant dernier album maintenant, intitulé Winter Thrice
(que j'ai découvert au pif, le même jour où parlait de Wintersun qui mettait beaucoup de temps à sortir « Time 2 »)
m'a un peu marqué puisque c'est le genre de son que je recherchais à ce moment là.

## Et donc, cet album ?

Et bien, pour être honnête ... je ne sais pas.
Je ne suis pas conquis.

Mais, pour autant, tout n'est pas à jeter.
Loin de là.
Déjà, c'est leur son.
On reconnait leur patte, on sait que c'est leur chanteur, et dans chaque morceau, il y a une ambiance.

Le côté progressif, je trouve qu'il faut le chercher.
Il y a aussi beaucoup de dissonances entre la composition rythmique et les voix.
Des fois elles partent en mode pop chiant.
Des fois, comme à l'accoutumée, elles prennent de belles hauteurs planantes, typiques de ce qu'est capable ce groupe.


## Et donc, ton avis ?


Et bien, je dirais qu'il faut leur laisser leur chance.
Attendez-vous à des surprises.
Ce n'est pas mauvais, c'est perturbant.
C'est pas du pur black, c'est pas non plus du croisement black/folk « pouet pouet », et la patte progressive, on la cherche un peu.

Du coup, si vous avez envie de vous surprendre, de finir un album en vous disant qu'il faut y réfléchir y revenir pour savoir, il est pour vous.
Vous aurez une impression étrange, comme celle de ne pas avoir compris l'idée derrière l'abum.
Par contre, vous voulez du prêt à consommer, passez votre chemin.

Je pense que cet album demande plus d'investissement que les autres, mais risque de décevoir beaucoup de gens tout de même.


[^1]: C'est pas pour faire mon casse-pieds, mais franchement, vite-fait hein. Ça dépend des morceaux.

