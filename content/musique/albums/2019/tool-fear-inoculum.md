+++
Author = "Chagratt"
categories = ["albums"]
title = "Tool : Fear Inoculum"
date = 2019-09-02T14:27:51+02:00
draft = false
bands = ["Tool"]
+++

Le dernier album de Tool est ENFIN sorti !
De mon point de vue, ça valait totalement cette longue attente.

Ce billet sera totalement subjectif.
Ne me remerciez pas, c'est cadeau !

<!--more-->

## Pouvoir enfin écouter cet album

Je crois que ce qui m'impressionne le plus, c'est l'auto-hype qui s'est dégagée toute seule sur le groupe le jour où des fans ont vu que le site avait enfin été mis à jour.
Faut dire que le ravalement du design façon début 2000, avec plus aucune information fraîche, ça fait tout de suite jaser quand c'est fait.
Ajoutez à ça une communication discrète sur l'entrée en studio des artistes, puis des dates dans de grandes salles et festivals pour mi-2019, et ça devient vite la fête du slip, sans aucun effort.
Et alors quand il y a une date de sortie annoncée pour un album, attendu depuis 13 ans ...

Alors, forcément, il y a toujours une petite appréhension lorsqu'on appuie sur le bouton « lecture » pour lancer l'album.
Et bien franchement, cette petite peur s'envole dès les premières notes.
Et au même instant, on commence immédiatement à attendre la suite, « ça commence bientôt ? C'est long l'intro là ! Je veux écouter du Tool ! ».

Hé oui, après tout, on l'attendait ... du coup, on est pressé.
Mais il faut absolument se détendre !
On est déjà en train d’en écouter, et comme les anciens albums, celui-ci ne se dévoile pas immédiatement.

## Se poser, et profiter de l'écoute !

Une fois passé ce sentiment d'impatience, ça y est, c'est là, le nouveau Tool arrive tranquillement.
Avec ses introductions lancinantes, installant une ambiance quasi-mystique, surtout envoutante.
Et puis PAF ! C'est parti, l'ensemble complet joue et charme nos oreilles, le chanteur nous flatte de sa voix.
Notre cerveau s'envole.

Puis l'heure et demie est partie !
La réalité revient.
Il est temps d'atterrir et de retrouver le reste de la journée.
On se sent comme presque abasourdi, mais aussi content, apaisé, limite heureux.
On a enfin eu l'album qu'on attendait tant.

Merci Tool !

## Des nuances tout de même

Alors, forcément, on va me dire « ouiiiiiiii tout ça pour çaaaaaa ! » ou alors « C'est pas comme avant en fait ! BOUUUUH ! ».
Je vais répondre qu'il faut se calmer !
Certes, le son n'est pas aussi sombre que sur Undertow ou Ænima, mais franchement ça ne dénote pas par rapport à avant.
On sent que c'est du Tool !
C'est toujours leur patte, aux compos, au mixage, à l'univers qu'on nous propose !
Et puis les artworks ... Il ne fait pas les oublier.

Alors oui, on l'a beaucoup attendu, oui c'est un peu « différent » (si on veut hein ...), mais le groupe et les musiciens ont évolué depuis leurs débuts.
Et j'aurais carrément été déçu qu'ils nous ressortent la même chose qu'il y a 15 ans !
Personnellement, j'adore déjà cet album, et je ne pense pas qu'il faille être déçu[^1].
Profitons, il est là, il aurait pu ne jamais voir le jour !

[^1]:Contrairement à de nombreuses sorties cette année ...

