+++
Categories = ["albums"]
title = "Avatar : Hunter Gatherer"
date = 2020-08-20T19:47:52+02:00
draft = false
bands = ["Avatar"]
+++

Après un teasing de l'enfer à base de trois morceaux pour donner envie, le tout dernier album d'Avatar est enfin sorti.

Je dois admettre que je suis sur la réserve.
Même si j'adore ce qu'ils ont fait jusqu'ici, sauf l'album Feathers & Flesh (il est objectivement bon, mais trop concept pour moi),
les singles m'ont donné cette impression que justement, nous risquions d'avoir un album du même tonneau.
Une œuvre expérimentale, difficile à comprendre.

Est-ce réellement le cas ? Je vais tenter d'y répondre.
Et j'annonce d'entrée de jeu que ça ne sera pas facile.

<!--more-->

L'album commence débute par deux des trois singles : << Silence in the Age of Apes >> et << Colossus >>.
Ces morceaux ne me vendent pas tellement du rêve pris indépendamment.
Ça joue peut être sur mon ressenti ... Mais ça annonce quand même la couleur : Avatar va faire les choses bien, mais différemment, ils tentent des trucs.
Mais on va dire que prit dans le flux de l'album, ça passe, car ces titres se font oublier.

Le 3e morceau, le premier original de l'album, est plus léger en termes d'ambiance.
On y retrouve cependant les bouts de folie typiques du groupe que l'on est habitués à recevoir de leur part.
En 4e voici le troisième single : << God of Sick Dreams >> ...
Bon, au moins c'est passé (hé oui, la surprise n'est pas là vu le teasing fait depuis des semaines) et on peut donc attaquer le reste de l'album en laissant tout ça derrière nous.
Après pour être honnête, le morceau porte bien son titre en termes de rendu d'ambiance.
Même si la composition pourrait être plus cauchemardesque pour accompagner le propos.

Que nous réserve donc la suite ?
On remarque vite que chaque morceau a sa propre ambiance, avec à chaque fois la patte caractéristique du groupe.
Aucun risque de confondre avec un autre.
L'excellent niveau musical est toujours là,
et au chant, Johannes nous gratifie toujours de chouettes performances vocales, avec de sacrés écarts entre son chant clair et ses growls.
On entendra parfois quelques bizarreries dans le mixage de la voix par rapport aux instruments.
Peut-être que c'est une volonté artistique ?
En parlant de volonté artistique,
je n'ai pas compris l'idée derrière la chanson << Gun >> ... Le piano n'est pas ouf, et le chant semble volontairement moyen.

Clairement, dans chaque morceau, on sent une volonté de tester des sons, des arrangements, par rapport aux albums d'avant.
Et très c'est perturbant, bien que bien fait.
Il n'y a réellement que le titre << When all but force has failed >> qui me plait vraiment.
Le seul morceau plus _classique_ si on peut dire, par rapport aux habitudes du death, death-mélo..

Bien que bon techniquement et ne me semblant pas opter pour la facilité, cet album ne me convainc pas.
En concert, je pense que ça va en jeter :
il y a de l'ambiance dans chaque morceau, des alternances de bourrinage puis de moments plus lents (pour reprendre son souffle :p ).
Mais l'écoute du CD en lui-même ... Meh. C'est trop concept pour moi.

Si vous aimez Avatar et avez adoré Feathers & Flesh, allez-y testez-le.
Moi perso, je passe, je vais sagement attendre le prochain.
Dommage.


