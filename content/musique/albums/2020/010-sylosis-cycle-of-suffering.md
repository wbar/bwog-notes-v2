+++
Categories = ["albums"]
title = "Sylosis : Cycle of Suffering"
date = 2020-03-16T17:11:34+01:00
draft = false
bands = ["Sylosis"]
+++

Voilà 5 ans que Sylosis ne nous avait pas gratifié d'un nouvel album.
Le groupe a début avec deux albums surtout axés Thrash, puis a continué en glissant allègrement vers le Death Mélo.
Je les ai découverts au pif lors d'une soirée à ce moment là.
Et j'ai pu les voir en concert plusieurs fois aussi (en première partie de groupes comme Children Of Bodom et DevilDriver par exemple).
Inutile de dire que j'ai adoré et que lorsque j'ai vu sortir cet album, j'étais impatient !

Alors, ça valait le coup d'attendre ?
Fallait-il être sur la réserve ou sauter dessus ?

<!--more-->

Dès le premier morceau on a pas le temps de réfléchir : ça tabasse.
Puis le morceaux se pose, nous offre un petit pont très typé djent/moderne, et ça repart.
Alternance de riffs puissants, de ponts mélodiques, et toujours le growl caractéristique du chanteur.
Pas de doute, Sylosis est de retour !
Quand c'est ce qu'on adore, ça met immédiatement en confiance.

De retour donc, mais avec de nouveaux éléments dans le son.
Oui, je fais un peu redite avec ce que j'ai signalé juste avant, mais il faut un peu de contexte.
Le chanteur Josh Middleton était avec Architects depuis fin 2016 / début 2017 et ça se sent.
Heureusement, bien que ce nouvel album intègre des éléments metalcore, il reste très axé death mélodique.
Les alternances entre les deux styles se font plutôt bien et permettent une certaine alternance de rythme,
en fonction de ce que le groupe souhaite nous transmettre.

On ne retrouve pas vraiment << l'ancien >> Sylosis tel qu'on a pu le connaitre.
Les musiciens ont eut le temps évolué en 5 ans.
Donc le groupe aussi.
Normal.
Tant mieux même, rien de pire que d'avoir une stagnation je trouve.
Ça se traduit par les changements dont j'ai parlé dans le son et la compo, ce qui pourrait dérouter plus d'une personne (et j'en fais un peu partie).
De mon point de vue, je me suis potentiellement trop auto-hypé par la sortie de ce nouvel album.
Mais ça fait plaisir de retrouver un groupe que j'ai beaucoup aimé.

Plus qu'à espérer que je puisse les revoir rapidement !

## Mon conseil pour vous

Si le Death mélo vous manque, mais que vous n'êtes pas fermés aux éléments modernes vous pouvez y aller.
Sinon la déception vous guette.

## Liens

- [Page sur l'Encyclopaedia Metallum](https://www.metal-archives.com/bands/Sylosis/79468)
- [Page Wikipedia](https://en.wikipedia.org/wiki/Sylosis) (en Anglais)

