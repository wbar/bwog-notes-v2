+++
Categories = ["albums"]
title = "Soilwork : A whisp of the Atlantic"
date = 2020-12-29T09:26:15+01:00
draft = false
bands = ["Soilwork"]
+++

Sorti discrètement début décembre, voici un petit EP de la part de Soilwork.
Avec 5 titres, presque 37 minutes d'écoute et l'album Verkligheten sorti en 2019, on peut dire que le groupe ne s'ennuie pas.

Qu'en est-il de nous qui les écoutons ?

<!--more-->

L'album s'ouvre sur une chanson de 16 minutes qui possède de bons enchainements d'ambiances et de rythmes.
À la manière d'une histoire que l'on raconte, avec son intro, son déroulement et sa conclusion.
On ne les sent pas passer.
Il y a même une petite surprise à la fin.
Elle fait un peu sens avec l'intro, c'est un gros contrepied avec le reste du morceau
(et globalement de l'habitude du son qu'on peut avoir venant de Soilwork),
mais en vrai je ne l'ai pas comprise.
À la limite je l'aurais mise aussi en intro pour lui faire un véritable écho et pas juste lancée comme ça à la fin.
Cela dit, je pinaille.
Le pari d'ouvrir ainsi un EP est osé, mais franchement, il est tenu et même réussi.

La suite se déroule de manière très agréable, le groupe s'amuse parfois avec les sonorités, tout en nous proposant des morceaux d'une durée plus classique,
mais tout aussi agréables à écouter.
On termine par le single déjà sorti plus tôt dans l'année qui a déjà fait ses preuves et vient très bien conclure cet EP.

Au niveau sonore et dans la composition, on peut entendre quelques tests dans les arrangements, mais rien de radical.
Ça reste du Soilwork.
C'est donc, comme depuis longtemps, propre et carré.
Tantôt puissant, voire bourrin comme on peut aimer, tantôt calme, comme pour reprendre son souffle.
Et pourtant, malgré la simplicité de ce résumé, cet EP fait mouche.

Pour finir ce qu'il faut retenir, à mon sens, c'est que lorsque l'on aime Soilwork on ne peut qu'adorer.
C'est mon coup de cœur 2020.

Mention spéciale pour le quatrième morceau qui est mon préféré.

