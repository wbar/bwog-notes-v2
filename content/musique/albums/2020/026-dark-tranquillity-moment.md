+++
Categories = ["albums"]
title = "Dark Tranquillity : Moment"
date = 2020-11-27T08:27:25+01:00
draft = false
bands = ["Dark Tranquillity"]
+++

Sorti le 20 novembre 2020, voici le 12e Album studio d'un groupe que j'aime beaucoup.
Moment sort dans une période compliquée, et vu les antécédents de Dark Tranquillity,
on peut s'attendre sans surprise à un son qui va refléter ça.

<!--more-->

La première chose que l'on remarque, et qui fait toujours plaisir, c'est que le rendu est toujours aussi propre et travaillé.
Au niveau des compos, il n'y a pas spécialement de surprises par contre.
Cela dit, elles sont toujours aussi efficaces.
C'est du Dark Tranquillity, ça s'entend, et ça tombe bien c'est ce que je recherche et je suis immédiatement transporté.

Même si c'est leur marque de fabrique, cet album là est très mélancolique, et ça se ressent particulièrement ici.
Il est même parfois lancinant, voire triste.
Le tout en étant posé, contemplatif.
De quoi exactement, à vous de voir.

Au début de l'album on a l'impression qu'il n'y a pas de progression ou de changement entre les chansons.
Effectivement, il y a beaucoup de morceaux avec un tempo calme, un rythme qui traine.
Parfois des accords puissants passent, avec le chanteur qui pousse sa voix pour donner une force évocatrice à certains passages.
Puis on revient à un moment plus éthéré.
Enfin, quelques changements dans les derniers morceaux de l'album, comme une lueur d'espoir.
Ça fait une excellente conclusion à l'œuvre.

Cet album est un pur produit de son époque,
un moment où mis à part se poser et se remémorer des instants d'avant, il est compliqué de faire autre chose.
Un très bel ajout à leur discographie déjà longue.
Une sorte de beau voyage introspectif, mais pas forcément à écouter si l'on n'est pas au top de sa forme.

Dans tous les cas, un très bon Dark Tranquillity qui ravit les oreilles et l'esprit.
Ils ont déjà prouvé qu'ils savaient le faire, ils viennent de le reconfirmer.

