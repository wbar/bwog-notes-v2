+++
categories = ["basse"]
title = "La basse en Novembre"
date = 2019-12-05T20:54:08+01:00
draft = false
+++


PAF ! On a changé de mois, ça y est !
Il est donc temps de se pencher sur ce qu'il s'est musicalement passé le mois dernier.
C'est fou ce que ça passe vite ...

Un nouveau morceau enfin choisi, et la première répète avec notre chanteuse !
Ho ouais !

<!--more-->

## Le 4è morceau

Or donc, petit changement pour ce 4è morceau : on passe à Stand Ablaze[^1] (album Subterranean) de In Flames au lieu de Rise And Fall de The Agonist.
C'était trop galère de bien écrire les tabs pour The Agonist et puis le niveau requis était également trop haut, faut pas se leurrer.
Là, ça va aller mieux ... Même s'il me faudra sans doute garder un médiator à portée de main pour celui-là (ARRRRGH !).
Les guitaristes et le batteur semblent confiants de leur côté.
Ça va être chouette !

## La chanteuse

Alors oui, aussi étonnant que ça puisse paraitre nous n'avons pu répéter avec elle qu'une seule fois jusqu'ici.
Les aléas des emplois du temps de chacun(e).
Du coup, même si on a bossé que sur un seul morceau avec elle, c'était déjà super chouette pour nous, les musiciens.
Elle ne se sentait pas très à l'aise face à nous (pourtant elle a déjà de l'expérience devant du public),
mais ça ne nous a pas du tout empêché de tous trouver ça super.
Vivement les prochaines fois, car ça prend vraiment une autre dimension maintenant !

## L'avancement sur les morceaux

Pas beaucoup de changements hélas ...

* Old Man : {{< progressbar type="success" value="100" >}} C'est toujours bon, sauf quand je suis fatigué.
* Little Dreamer : {{< progressbar type="success" value="100" >}} Pareil !
* Heathen Horde : {{< progressbar type="success" value="92" >}} Je ne sais pas pourquoi, mais ça continue à ne pas vouloir rentrer entièrement ...

Allez, on se voit le mois prochain !

[^1]: Un [petit lien vers la chanson](https://www.youtube.com/watch?v=tShvBglbYC4)

