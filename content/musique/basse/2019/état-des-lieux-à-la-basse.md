+++
Author = "Chagratt"
categories = ["basse"]
title = "État Des Lieux À La Basse"
date = 2019-08-19T13:31:56+02:00
draft = false
+++

Cela fait quelques années que je joue de la basse, c'est un instrument qui m'intéresse énormément (voire me passionne quand je joue avec).

Cepdendant, je me considère comme un éternel débutant.
Je n'ai prit qu'une année de cours (en 2017), je n'ai jamais fait de solfège ailleurs qu'au collège (youpi ...), et je ne suis entré dans un groupe que très récemment.
Du coup, je vais tracer mes avancées sur ce blog, histoire d'avoir un suivi (qui devrait m'apporter une motivation supplémentaire), et pour partager ce petit monde avec vous.
En espérant ne pas être trop barbant.

Bonne lecture !

<!--more-->

## Mon passif musical ##

J'ai commencé la basse en tant qu'autodidacte, et par pure curiosité deux ou trois ans après ma sortie du lycée.
Pour ça j'ai acheté un pack entrée de gamme histoire d'accompagner mon frangin qui était à la guitare.
J'avais une petite expérience de guitariste rythmique au lycée (dans un groupe de potes monté à l'arrache[^1]), mais je voulais reprendre la musique autrement, bien plus sérieusement qu'avant, et c'est donc cet instrument qui s'est imposé à moi.

Après quelques mois plein d'entrain, j'ai déménagé loin, et j'ai eu beaucoup moins de temps pour moi à partir de là.
J'ai donc malheureusement perdu la motivation pendant ce laps de temps.

## La (presque) reprise ##

Quelques temps plus tard, il y a eu de gros changements dans mon emploi du temps personnel, et je me suis remit à pincer les cordes au petit bonheur la chance sur deux troix petits morceaux simples.
C'était pas glorieux, mais ça m'occupait et ça recommençait à me plaire.
Tellement même que j'ai vite été me casser les dents sur des OST de Final Fantasy, du Black Sabbath, du Nigthwish, et aussi les morceaux qu'on avait joué avec les potes au lycée.
Ca sonnait dégueulasse, mais ça donnait un petit quelque chose de plaisant à mes oreilles.
Moins à mes doigts étrangement ... Mais plus je jouais, moins ça donnait sale. 
Normal !
Par contre, j'avais l'impression de ne plus progresser au bout d'un moment. Un des paliers tristement célèbre chez les musiciens débutants.
Le truc qui décourage énormément, et qui a bien failli m'avoir.
Heureusement j'ai eu la présence d'esprit de chercher un prof de basse, et j'en ai trouvé un bon sur Toulouse, vu que j'habitais là bas à ce moment là.
Et depuis, tout roule. Il m'a donné des morceaux et exercices qui m'ont bien délié les doigts.

## Et maintenant ? ##

Bon ça commence à faire long cette histoire, mais depuis, je suis sur Lyon, et j'ai rejoint un groupe composé d'une bande de pote.
Leur niveau est haut, bien plus que le mien.
Mais l'ambiance est excellente, et on se concentre surtout sur la bonne entente et le fait de faire sonner les morceaux ensemble.
C'est un terreau propice à la progression pour moi.
Et donc au plaisir de jouer régulièrement et de manière assidue.

## Les morceaux ##

On se concentre sur du Ensiferum, groupe qu'on aime tous.
Voici les morceaux sur lesquels nous jouons actuellement (et mon avancement dessus) :

* Old Man : {{< progressbar type="success" value="100" >}} Quelques pains par-ci par-là quand je ne suis pas en forme, mais le morceau est aujourd'hui maitrisé.
* Little Dreamer : {{< progressbar type="warning" value="90" >}} J'ai encore parfois du mal dans l'ordre de certains riffs, mais globalement je tiens le rythme et je ne fais plus de fautes majeures dessus.
* Heathen Horde : {{< progressbar type="danger" value="50" >}} Celui-ci me donne du fil à retordre ! Je vais devoir prendre plus de temps pour être sur de progresser rapidement.



## L'objectif ? ##

Le fun avant tout ! La progression ensemble sur nos capacités de musiciens en groupe ensuite.
Et, pourquoi pas, jouer dans des bars, pour le plaisir et l'expérience que ça nous fera.
Si jamais ça arrive, passez nous voir. :)

J'espère que ce n'était pas trop long comme article.
Vu que les bases sont maintenant posées, les prochains de la série sur la basse seront assurément plus courts ! Promis.

[^1]: Le grand classique qui ne mène souvent nulle part une fois ces années terminées ... Mais on s'est bien marrés pendant tout ce temps, et c'est ça qui compte !

