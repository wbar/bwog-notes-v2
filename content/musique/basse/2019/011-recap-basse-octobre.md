+++
categories = ["basse"]
title = "Récap d'Octobre (non je ne me suis pas trompé)"
date = 2019-11-01T23:55:03+01:00
draft = false
+++

Je sais, j'ai déjà écrit « Récap d'Octobre » avant, et c'est là que je me suis trompé.
Par deux fois d'ailleurs ... Puisque je faisais le récap du mois d'avant en titrant avec le mois en cours.
Bravo !
Et du coup, puisque personne ne m'a rien dit, plus qu'à espérer que ça passe discrètement.

Bref, ça va aller mieux sur les titres.

Et à la basse ? Mieux aussi ?

<!--more-->

Et bien un peu.

Ce récapitulatif va être sans doute plus court que ce que j'imaginais à la base, mais en vrai, y'a pas grand chose à dire.
C'était un peu la rouille, mais au moins, j'ai progressé plutôt que stagné.

Et nous avons un nouveau morceau maintenant à travailler : Rise and Fall[^1] de The Agonist (album Once Only Imagined).
Ca va envoyer du pâté quand on l'aura en main, mais pour le moment, c'est plutôt la chasse aux canards !
D'ailleurs, on est pas encore en train de le travailler à proprement parler, on est en train de voir pour corriger les partitions qu'on a choppées.
Il y a un petit quelque chose de gênant par rapport à ce qui été couché dessus.

## L'avancement sur les autres morceaux

* Old Man : {{< progressbar type="success" value="100" >}} Plus rien à signaler, et heureusement !
* Little Dreamer : {{< progressbar type="success" value="100" >}} La même !
* Heathen Horde : {{< progressbar type="success" value="90" >}} Après la ré-écriture, ça allait mieux. Maintenant, il me manque quelque chose pour réussir à bien retenir ce morceau. Je ne sais pas pourquoi, mais ça bloque un peu, même en m'acharnant. Du coup je tente un autre moyen pour retenir.

J'espère que le mois prochain je pourrais ajouter une barre pas trop décevante pour le nouveau morceau,
et que celle de Heathen Horde passera à 100%.
Mais ça ne tient qu'à moi.

[^1]: Un [petit lien vers la chanson](https://www.youtube.com/watch?v=tMOTWd4TgJ4)

