+++
Categories = ["basse"]
title = "La basse (rapidement) en Février"
date = 2020-03-02T19:11:36+01:00
draft = false
+++

Février a été beaucoup rempli pour moi, mais au niveau de la basse par contre, c'était le calme très (très) plat, hélas.
Du coup cet article sera court.

<!--more-->

Je sais c'est moche, mais entre les concerts de mon côté et les soucis de matos chez mon guitariste, ben ... J'ai rien foutu.

## Le non avancement

Bien bien bien, je continue de dire qu'il me faut prendre plus de temps, mais je ne l'ai pas fait.
En plus j'ose me plaindre ... bravo ...
Je vous remet donc les barres du mois dernier en mode copier-coller honteux.

* Old Man : {{< progressbar type="success" value="100" >}} C'est toujours bon.
* Little Dreamer : {{< progressbar type="success" value="100" >}} Pareil !
* Heathen Horde : {{< progressbar type="success" value="94" >}} Bientôt plié !
* Stand Ablaze : {{< progressbar type="danger" value="15" >}} Ça va demander du boulot mais ça sera marrant !

Voilà voilà ... Bon, cette fois je m'y met vraiment !

