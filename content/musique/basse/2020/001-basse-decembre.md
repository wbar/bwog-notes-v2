+++
Categories = ["basse"]
title = "La basse en Decembre (2019)"
date = 2020-01-05T19:30:55+01:00
draft = false
+++

Et bien nous y voilà !
Le récap' mensuel, avec un changement d'année en prime. Youpi !
En vrai, ça ne change pas grand chose.
Mis à part que forcément, avec les fêtes, quelques vacances pour les membres du groupe et moi, forcément l'activité a été ... ralentie.

<!--more-->

Voire inexistante pour la seconde quinzaine pour être réellement honnête.
Du coup, fallait pas s'attendre à des miracles de mon côté.

## L'avancement sur les morceaux

Bah ...

* Old Man : {{< progressbar type="success" value="100" >}} C'est toujours bon.
* Little Dreamer : {{< progressbar type="success" value="100" >}} Pareil !
* Heathen Horde : {{< progressbar type="success" value="93" >}} Pas réellement d'évolution.
* Stand Ablaze : {{< progressbar type="danger" value="10" >}} Je commence à peine le déchiffrage ...

Soignez bien votre système digestif ainsi que votre fois, et à la prochaine !

