+++
Categories = ["basse"]
title = "la basse en Avril"
date = 2020-05-02T17:16:02+02:00
draft = false
+++

Avec tout ce temps devant moi, je ne sais pas pourquoi mais je n'ai pas vraiment réussi à me motiver tous les jours.
Ni sur beaucoup de temps à chaque fois.
C'était parcellaire, quasi inexistant et peu reluisant.
Je me suis pourtant prit de nouvelles cordes et quelques produits d'entretien.
Mais ça n'a pas suffit ... mouerf ...

Voilà que je culpabilise un peu même.
Bien fait pour moi !

<!--more-->

## Le minuscule avancement

* Old Man : {{< progressbar type="success" value="100" >}} C'est toujours bon.
* Little Dreamer : {{< progressbar type="success" value="100" >}} Pareil !
* Heathen Horde : {{< progressbar type="success" value="96" >}} Je stagne un peu. Normal vu mon niveau d'activité ...
* Stand Ablaze : {{< progressbar type="danger" value="18" >}} Y'a toujours du boulot hélas ...

Voilà voilà.
Prochain récap' en juin.


