+++
Categories = ["basse"]
title = "La basse en Mars"
date = 2020-04-04T11:11:06+02:00
draft = false
+++

On pourrait croire, avec le début du confinement que j'aurais prit **beaucoup** de temps pour la basse,
mais le fait est que pas tant que ça en vérité.
Un petit peu d'avancement donc, mais c'est vraiment minime.
Je me suis surtout attelé au déchiffrage.
Et je me suis également prévu un programme pour _réellement_ progresser au mois d'avril.

<!--more-->

## Le minuscule avancement

* Old Man : {{< progressbar type="success" value="100" >}} C'est toujours bon.
* Little Dreamer : {{< progressbar type="success" value="100" >}} Pareil !
* Heathen Horde : {{< progressbar type="success" value="96" >}} Presque plié !
* Stand Ablaze : {{< progressbar type="danger" value="18" >}} Y'a du boulot ...

C'est tout !
Rendez-vous le mois prochain pour le récap' d'Avril.

