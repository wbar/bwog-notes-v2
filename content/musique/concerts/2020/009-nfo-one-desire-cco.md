+++
Categories = ["concerts"]
title = "The Night Flight Orchestra et One Desire au CCO (Villeurbanne)"
date = 2020-03-15T12:42:06+01:00
draft = false
bands = ["The Night Flight Orchestra", "One Desire"]
+++

Après avoir mit le feu au Warmaudio à Lyon le 5 décembre 2018, NFO atterit de nouveau à Lyon pour défendre son dernier album et nous endiabler par la même occasion.
La formation Suédoise était accompagnée pour l'occasion d'un groupe de rock tout droit venue de Finlande.

Retour sur ce concert qui s'est déroulé jeudi 12 Mars.

<!--more-->

Malgré le contexte difficile de ces derniers temps, les groupes et surtout le public étaient au rendez-vous.
On ne s'attendait pas à faire une grosse date, mais finalement, il y avait pas mal de monde.
Environ 200 personnes.
A vrai dire, on en espérait pas autant.

Au niveau des groupes présents ce soir (seulement 2), il y avait One Desire en première partie,
et The Night Flight Orchestra en tête d'affiche.

One Desire est une formation de rock classique / mélodique.
Bien qu'à l'oreille ça sonne un peu daté[^1], nous avons eu droit à une prestation plus qu'honorable.
La salle était loin d'être remplie à ce moment là, mais le groupe à fait fi de la situation.
Sur scène, ça se sentait qu'ils se faisaient plaisir et souhaitaient tout donner pour faire plaisir aux spectateurs.
Bien que n'étant pas particulièrement fan du genre, il faut admettre que la construction des morceaux est bonne, et le rendu agréable.
La balance était bonne également, permettant de découvrir un groupe que l'on peut largement faire passer en soirée, 
même quand on a des ami(e)s qui ne sont pas adeptes du style, afin d'avoir une ambiance agréable et festive.
Le public semblait également réceptif :
il y avait peu de monde en dehors de la salle par rapport au nombre de personnes présentes et tout le monde jouait le jeu avec le groupe.

Après un set relativement court de sa première partie (et un inter groupes où on ne s'est pas tant fait déchirer que ça au bar), NFO a débarqué sur la scène.
Intro de synthé pour faire monter la pression, jeux de lumière (façon bâton pour guider les avions au sol) de la part des choristes, 
arrivée « massive » du public et PAF ! En avant !
Petit voyage dans le temps au niveau sonore car les 80's étaient bien passées faire coucou pendant 1h45.
Une set-list assez bien dosée, bien qu'on puisse regretter la trop grande présence d'anciens morceaux par rapport aux ceux du nouvel album.
Cela dit, elle était très efficace, car c'était réellement le feu dans la fosse.
Le public était très festif, et même nous derrière le bar on se prenait au jeu !
Le groupe semblait d'ailleurs très content de réussir à faire autant sautiller son public.
En plus, le son était excellent, même au niveau du bar, alors qu'habituellement vu qu'on est bien sur le côté on perd beaucoup de qualité sonore.
Le petit point noir, c'est le projo côté droit de la scène qui nous en foutait plein la gueule au bar.
Bon, je me plains un peu, mais les artistes ça devait être pire après tout.

En bref, un excellent concert qui mettait de très bonne humeur !

## Et côté Orga ?

Au final, 5 fûts[^2].
C'était pas si mal.
Et malgré un sous-effectif dans la journée, l'équipe a pu tout organiser correctement.

Dans l'ensemble la date était « tranquille » comparée à beaucoup d'autres, et chacun a pu y aller de sa petite pause ou aller voir les groupes.
C'était assez rare ces derniers mois, alors on en a bien profité !

D'ailleurs, fun fact, quand on a remarqué que le tour bus était rentré en marche avant dans la cour du CCO, on a pas pu s'empêcher de faire nos mauvaises langues,
et de venir voir comment il s'en sortait pour partir.
Ah bah mince ... Du premier coup en mode tranquille la main dans le slip !
Chapeau le conducteur !

## Liens

- [The Night Flight Orchestra : Le site officiel](https://thenightflightorchestra.com/)
- [One Desire : site officiel](http://www.onedesire.net/)

[^1]: Par rapport aux évolutions du genre et des compositions de morceaux en 2020.
[^2]: Je sais, j'ai [poueté « 4 »](https://framapiaf.org/@chagratt/103812370528572786), mais c'était sans compter le 5è qui était encore branché à la tireuse à ce moment là ! :p

