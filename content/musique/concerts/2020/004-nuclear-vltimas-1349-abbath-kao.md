+++
Categories = ["concerts"]
title = "Nuclear + Vltimas + 1349 + Abbath au Ninkasi Kao (Lyon)"
date = 2020-02-10T18:06:28+01:00
draft = false
bands = ["Nuclear", "Vltimas", "1349", "Abbath"]
+++

Après le Death de samedi 1 place au Black metal dimanche 2 avec une affiche composée de poids lourds du milieu ou tout du moins de musiciens (très) expérimentés.
On pouvait s'attendre à une date bancale avec un faible remplissage de salle (Behemoth et Slipknot jouaient juste à côté ce soir là !) mais il n'en était rien.
Les fans étaient au rendez-vous !

<!--more-->

## Quelques précisions

J'ai indiqué "Black" en introduction de cet article, mais il faut savoir que Nuclear est un groupe qui se veut être orienté Thrash.
Je ne peux pas vous dire ce que ça donnait sur scène, mais avec un groupe actif depuis 1995 et qui vient du Chili,
on peut sans problème imaginer que le sujet est maitrisé !
En tout cas, un rapide tour sur la discographie oriente dans cette direction[^1].

## Le concert en lui-même

Étant arrivé en plein milieu du set de Vltimas je me suis retrouvé face à un groupe de Blackened Death avec un chanteur à haut de forme, chemise et veston.
Le tout servi par une lumière à l'ambiance froide et un son relativement tantôt calme, tantôt puissant mais planant.
Par rapport au son envoyé par le groupe, le chanteur semble en décalage.
C'est un bon chanteur c'est pas le soucis[^2], mais on dirait un chanteur d'opéra qui avait envie de growler un coup pour se marrer.
Quoique le chant est puissant, sans arracher les cordes vocales, avec quelques envolées hautes.
On retiendra que je ne suis pas fan, mais pour être honnête pendant le concert ça passait bien, notamment grâce aux musiciens qui ont envoyé du lourd.

Conclusion de cette première partie : Une bière à la main, une place pas trop mal et hop, on passe un bon moment.

## Les gros noms de la soirée

Hop hop, 15 minutes dehors le temps pour toute l'équipe technique de changer le plateau et moi de prendre une autre binouze et on y retourne.
A peine le temps de repérer une bonne place que la quantique se lance, la lumière s'éteint et le public s'auto chauffe.
Ambiance sombre et lourde pour l'arrivée des musiciens, et pas le temps de réaliser quoi que ce soit que BOUM !
Frost te growle dans les oreilles pendant que tu te prends l'instru dans la face.
Pas subtil, bien typique de ce qu'on aime dans le Black Norvégien, presque un rouleau compresseur pendant 45 minutes.
Un régal ! Le groupe tient la baraque et le charisme du chanteur sublime le tout.
Ah merde, c'est déjà fini ? Dommage ...
Mais une excellente prestation pour le groupe, et une très bon moment pour nous, le public.
Vivement leur prochain passage !

Bon, on se remet de ses émotions, on laisse le temps aux techs de préparer le plateau pour Abbath.
Dans la fosse, on sent que tout le monde est fébrile.
On a tous entendu parler de l'histoire en Amérique du Sud mi-novembre 2019[^3] et du renvoi de la bassiste qui a suivi[^4].
Déjà le concert n'a pas été annulé, le reste de la tournée Européenne non plus.
Et puis il a été annoncé que la cure de Abbath se passait bien.
Mais sera-t-il à la hauteur ? Suspense ... Mais pas trop, on est pas là pour niaiser !
Nous avons eu droit à un concert carré, des morceaux et un jeu maitrisé, et un Abbath plutôt en forme compte tenu des événements récents.
Pour les fans, c'était un régal, même si on sentait une certaine fatigue.
Le jeu avec le public était bien là.
Il ne manquait que son fameux pas en crabe, c'est ballot !


En bref, un concert qui faisait peur, mais une très bonne date.


## La mention spéciale

Oui, cette fois il y en a une : le design du backdrop de 1349.
Le doré sombre sur fond noir ... Pas terrible pour la lisibilité.
Je ne sais pas si leur designer s'est fait remonter la remarque, mais c'est le petit détail rigolo quand on s'imagine en fest à se demander qui passe,
mais qu'on ne peut pas savoir car c'est illisible.

## Des liens sur les groupes

- [Abbath](http://www.abbath.net/)
- [1349](http://www.legion1349.com/)
- [Vltimas](https://www.metal-archives.com/bands/Vltimas/3540448233)
- [Nuclear](https://www.metal-archives.com/bands/Nuclear/47481)

[^1]: Prenez des pincettes avec cette information car je ne suis pas un spécialiste du Thrash, loin de là.
[^2]: On parle de David Vincent, c'est pas un type qui en est à son coup d'essai niveau groupes.
[^3]: [Article à ce propos sur Lambgoat (en anglais)](https://lambgoat.com/news/32312/Abbath-ends-show-after-two-songs-reportedly-intoxicated)
[^4]: https://lambgoat.com/news/32628/Abbath-kick-out-bassist-Mia-Wallace


