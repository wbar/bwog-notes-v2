+++
title = "Ryujin, Eleine, Ensiferum et Pain à la Rayonne (Villeurbanne)"
date = 2023-11-05T16:44:00+02:00
bands = ["Ryujin", "Eleine", "Ensiferum", "Pain"]
+++

Dimanche 22 octobre 2023, première date pour Sounds Like Hell à La Rayonne,
salle toute neuve qui prend le relais suite à la fermeture et la démolition du CCO Jean-Pierre Lachaise.
Première date de métal tout court à cet endroit d'ailleurs.

Voyons ensemble ce que cela a donné.

<!--more-->

## Côté concert

### Ryujin

Des Japonais proposant du « Samurai Metal » (ce n'est pas moi qui invente),
voilà qui n'est pas banal.
Je ne connaissais absolument pas, mais je me suis laissé tenter.
Et je ne regrette pas.
Ce set d'une demi-heure était survolté.
Les musiciens avaient une énergie folle sur scène, et un niveau musical au top.

On sent que le groupe n'en est pas à ses débuts, et c'est vrai.
Avant février 2023, la formation s'appellait GYZE et a débuté vers 2012.
L'impression de se retrouver devant un groupe bien rôdé était donc juste.

En termes de ressenti musical, je peux comparer ce groupe à un enfant illégitime entre Dragon Force et Ensiferum,
qui a été concu, enfanté et abandonné au Japon.
Nous sommes donc face à un mélange de power et de death mélodique, mais en plus rapide.
Le côté power n'est habituellement pas du tout ma came,
mais il faut avouer que le groupe donnait envie de rester voir.
Des solos endiablés, des ponts mélodiques très bien fichus, et un style de jeu du chanteur / lead gratteux ressemblant fort à ce que faisait Alexi Laiho au top de sa forme.
Mais sans aucune fausse note.
Impressionnant !
Je regrette presque que le groupe était en premier avec seulement 30 minutes.

À mon sens un groupe de concert.
Ça risque d'être moins intéressant en CD cela dit.

Si vous avez l'occasion de les voir, n'hésitez pas.

### Eleine

C'est une formation de métal symphonique montée en 2014.
Mis à part la voix de la chanteuse que j'ai pu entendre lors des balances,
je ne pourrais pas en dire plus, ayant fait l'impasse,
et n'ayant pas eu de retour des potes qui ont été voir.

### Ensiferum

{{< image src="ensiferum.jpg" caption="Ensiferum sur scène. Le batteur au fond à gauche. Le bassite à gauche, le chanteur au milieu. Le second guitariste à droite. En guise de décor, deux drapeaux avec le logo du groupe, de part et d'autre de la scène." >}}

Après deux groupes dont l'ambiance est allée croissante,
il faisait déjà super chaud dans la salle ! Argh !
Cependant, le groupe ne s'est pas laissé démotiver et a commencé sobrement avec Andromeda.
Bon, pourquoi pas.
Ce n'est pas leur meilleure, mais il faut croire que ça a largement suffi au public.
En effet, des pogos ont immédiatement démarré !
Ils ont ensuite enchainé avec In my sword I trust.

Bon, je ne vais pas vous faire la setlist complète.
Mais on sent qu'ils ont un peu honte du dernier album n'empêche. Il n'y a pas eu tant de chansons que ça provenant de Thallasic,
mais énormément de classiques de leur discographie. 
Et un petit Two of Spades pour fini.
Un pari réussi niveau ambiance, car la chaleur a largement augmenté vu le nombre de pogos qu'il y a eu.
Et pendant tout ce temps, le bassiste jouait énormément avec la foule. Il mettait quasiment l'ambiance à lui tout seul.

Si je n'avais pas prévu de garder mon énergie pour bien faire mon boulot en fin de soirée, je pense que je serais resté dans la fosse jusqu'à en crever (de chaud) !

### Pain

J'ai voulu aller voir, mais je ne suis resté que pour la première chanson : trop chaud, et trop de stroboscopes !
Par contre, ma Dame qui était présente a assisté à une bonne partie du concert.
Cette section a été rédigée par ses soins.

Ils ont commencé avec Let Me Out, un très bon classique.
La chaleur dans la salle était lourde, c'était dur de profiter à cause de ça, même sur la mezzanine.
Le groupe a quand même tout donné, et c'était un show très sympa.
Depuis la dernière fois que je les ai vus au Kao, le show lumière a bien évolué.
La setlist était au top, et le dernier titre enregistré par le groupe, « Revolution »,
donne très envie d'écouter l'album à paraître.
Ce dernier titre a été composé par le fils de Peter Tägtgren, Sebastian, qui est aussi le batteur du groupe.

À la fin du premier tiers du concert,
tous les musiciens troquent leur tenue de scène pour quelque chose de plus léger : jean et t-shirt.
Ils prennent des tabourets hauts et disent : « On fait une petite pause »,
tout en jouant la chanson Walking on Glass si je me souviens bien.
C'est dire s'ils devaient crever de chaud à cause de l'absence de ventilation.
Sur la dernière chanson, la plus attendue car la plus connue (Shut Your Mouth),
l'alarme incendie de la salle se déclenche, distillant une certaine confusion dans la salle.
Pas de feu à l'horizon heureusement, mais on pense que quelqu'un avait fumé pas loin d'une des alarmes.
Le groupe s'assoit sur scène et patiente, priant pour que le public reste.
Le responsable de la sécurité valide le fait que le groupe peut finir le concert.

Tout se finit bien, sur des pogos encore farouches malgré la chaleur présente.
Un très bon concert, même si j'ai trouvé le son saturé et pas à la hauteur de ce qu'ils font habituellement (à mon avis l'ingé son s'en foutait).
Pour tout le reste, c'était parfait.

Je tiens à souligner la présence de l'écran en fond de scène qui diffusait des petits clips en lien avec la chanson jouée,
ça rajoutait un certain plus pas désagréable du tout.
Et toujours un très bon lien avec le public !

## Côté orga

Et maintenant, place aux observations côté backstage.
Je ne pensais pas avoir à en raconter autant.
C'est plutôt surprenant pour des lieux neufs je trouve.

### La salle en elle-même

Une belle salle toute neuve donc pour prendre le relais du CCO JPL qui était devenu franchement insalubre.
Chouette !
Avec une augmentation de la jauge à 1000 personnes.
Il y a des places de parking réservervées PRM juste devant et beaucoup d'accès depuis les transports en communs à 500m.
Également à l'étage (avec ascenseur), on trouve une mezzanine avec des places assises et un espace suffisant pour placer plusieurs personnes en fauteuil roulant au besoin.

Les locaux sont donc propres, 
l'espace cuisine et restauration pour les groupes est plus grand qu'avant, avec une grande baie vitrée (qui court tout le long des backstages en vérité),
et un espace extérieur pour les artistes.
Ça rend le tout bien plus chaleureux.
Les loges sont également très correctes.

Enfin, niveau salle, la sonorisation est chouette et permet aux ingés son de nous régaler les esgourdes.

### Les couacs

Place maintenant à la partie qui fâche, car oui, il y a de nombreuses choses qui ne vont pas.
Un comble !

En détails pas méchants, mais tout de même gênant, on peut déjà relever le manque de rideaux aux vitres des loges.
Elles profitent de l'énorme baie vitrée dont j'ai parlé précédemment. 
Ça permet d'éviter aux groupes de se retrouver dans une salle sans fenêtres, c'est toujours plus agréable,
mais il y a à cause de ça un vis-à-vis avec le batiment d'en face qui ne va peut-être pas contenir que des bureaux (le batiment n'était pas terminé quand on y est allés).

Ensuite, il y a le couloir qui dessert les loges, le quai de chargement, la scène et l'espace restauration.
Il est dans les normes de largeur, mais dans des coulisses, il y a vite du matos qui traine et des gens qui circulent.

En autre détail idiot, il y a les douches des loges.
Le plancher est parfaitement horizontal !
Je vous laisse imaginer ce que ça implique, puisque l'écoulement de l'eau ne peut pas se faire ...
Au niveau des zones techniques, on a aussi la blague des seuils des portes coupe-feu qui dépasse allègrement du sol d'un bon centimètre.
Pratique avec les caisses à roulettes bardées de matos.

Nous allons arriver au pire maintenant : l'absence **totale** d'aération et de clim dans la salle !
Ce qui fait que la chaleur a rapidement été infernale dès la moitié de la soirée, voire même avant.
Les membres de Pain ont réellement souffert de ça.
Je suis d'ailleurs étonné qu'il n'y ait pas eu plus de problèmes que ça, du genre des malaises dans le public.
Fort heureusement ça n'a pas été le cas, mais j'ai pu noter beaucoup de monde dehors pour reprendre le frais pendant Pain.

Bref, pour aider un peu, on a voulu ouvrir des portes en backstage pour capter du frais depuis la baie de chargement.
Hélàs, avec la chaleur et la fumée (qui n'a donc pa été évacuée par la traditionnelle aération),
cela a déclenché l'alarme incendie juste avant la dernière chanson du groupe.
Fort heureusement, ils ont pu terminer, mais avec environ 20 minutes de retard, le temps de refermer, s'assurer qu'il n'y avait effectivement aucun feu, et couper l'alerte en cours.

Le fin du fin : pendant ce temps, l'arrivée électrique du second tour-bus a lâché.
Heureusement que les gars de la salle ont pu la remettre assez vite uen fois le concert relancé.

Hé bé, on espère fort que ces problèmes seront réglés pour les prochains événements qui auront lieu dans cette salle !


## Liens

- [Ryujin](https://en.wikipedia.org/wiki/Ryujin_(band))
- [Eleine](https://en.wikipedia.org/wiki/Eleine_(band))
- [Ensiferum](http://www.ensiferum.com/)
- [Pain](http://www.painworldwide.com/)
- [La Rayonne](https://larayonne.org/)

