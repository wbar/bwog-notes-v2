+++
title = "The Road Crew au Rock N Eat (Lyon)"
date = 2023-01-02T09:10:00+01:00
bands = ["The Road Crew"]
+++

Mercredi 28 décembre 2022 au soir, c'était hommage à Lemmy et Motörhead en foutant un beau bordel.
Rien de mieux pour y parvenir qu'une soirée spéciale avec The Road Crew, un tribute band Lyonnais.

<!--more-->

À l'origine, il devait y avoir une première partie avec Little Man ( Richard Nury ),
mais ce dernier a dû annuler.
Nous n'avions donc qu'un seul concert au lieu de deux.
The Road Crew avait donc deux fois plus de pression et pas que dans les verres.
D'autant plus qu'ils étaient largement attendus vu qu'ils avaient déjà mit le feu l'an dernier.

Je vais immédiatement répondre à la question : oui, ils ont assuré !

Malgré leur fatigue impossible à ne pas voir, le trio a retourné le bar hier soir en proposant un show énergique et quasi continu d'1h30.

Gros son, chant guttural, pogos, set-list au poil et échanges endiablés avec le public étaient bien évidemment de la partie.
Le tout avec un très bon réglage sonore malgré les difficultés à le faire au Rock N Eat.
En autre difficulté, une corde a pété sur la guitare pendant Ace Of Spades.
Le temps de prendre une seconde guitare et de monter la sangle sur cet autre instrument aura été la seule pause pour le groupe.
Ce petit moment fut d'ailleurs mis à contribution pour haranguer encore un peu la foule et faire monter l'ambiance d'un cran.
Cela dit, il n'y en avait limite pas besoin, car le public était déchaîné.

Par rapport à l'an dernier, clairement, le concert était un cran au-dessus.

Si vous aimez Motörhead,
le hard-rock,
les concerts
et les grosses ambiances,
guettez leurs dates car ça vaut réellement le coup.
Sinon, réservez votre 28 décembre 2023 et venez voir sur place !

## Liens

- [Page Facebook du groupe](https://www.facebook.com/theroadcrewtributeband/)
- [Page du groupe sur Concerts-Metal](https://www.concerts-metal.com/g-52183__The_Road_Crew.html)
- [Site de Little Man](https://littleman.fr/)

