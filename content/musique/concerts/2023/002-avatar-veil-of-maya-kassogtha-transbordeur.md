+++
title = "Avatar, Veil of Maya et Kassogtha au Transbordeur (Villeurbanne)"
date = 2023-03-27T19:36:22+02:00
bands = ["Avatar", "Veil of Maya", "Kassogtha"]
+++

Jeudi 23 mars fut une date sold-out pour le Transbordeur,
et comme c'était, pour ma Dame et moi notre permier concert en tant que spectacteur depuis un long moment,
nous l'attendions énormément.
Surtout la tête d'affiche.
Et visiblement, nous n'étions pas les seuls !

En avant pour une soirée placée sous le signe du gros son, de la chaleur (humaine) et ... de la transpiration !

_Note_ : Ce retour de concert a été co-rédigé avec ma Dame.

<!--more-->

## Les premières parties

Il y avait deux groupes annoncés sur l'affiche : Kassogtha et Veil of Maya.
Vu que nous les avons loupés (nous ne sommes arrivés que pour Avatar),
j'ai tenté  de glaner des infos auprès des connaissances présentes à ce moment-là.
Tout ce que je peux en dire, c'est que ces groupes n'ont pas fait l'unanimité.
Je ne peux pas en retranscrire plus, aussi, je vous invite à vous faire votre propre idée si vous en avez l'occasion.

## Avatar

Entrons maintenant dans le vif du sujet : la tête d'affiche.

Déjà, un petit mot sur le placement.
Vu que le concert se déroulait dans un Transbordeur plein,
il a été difficile de trouver une place correcte.
La fosse n'était hélas pas une option réalisable.
Nous nous sommes donc rabattus sur le haut des gradins,
et finalement, ce non-choix s'est révélé être bon.
Déjà, par rapport à d'habitude, le son était excellent, même tout là-haut.
Et nous avions une vue imprenable sur la scène, ainsi qu'un point de vue sur la fosse.

Le décor sur scène comportait 4 sortes de portes, deux de chaque côté de la batterie.
Cette dernière était au centre sur une estrade assez haute.
Ces portes ont servi d'entrée aux guitaristes, au bassiste et au chanteur.
Elles sont aussi servies de sortie ou de cachette pour les interludes.

Tout au long du concert, un superbe mélange de jeu théâtral et musical a été déployé sur scène.
Sublimé par des jeux de lumières incroyables,
donnant des effets d'ombres et d'éclairages très bien faits.

Pendant leurs deux heures de présence sur scène,
Avatar nous a gratifié de bien plus qu'un simple concert.
L'interaction avec le public a été très forte pendant cette soirée.
Les échanges et petits jeux entre les musiciens (qui sont extrêmement bien synchronisés entre eux lors des headbangs) apportent un vrai plus qui montrent une bonne alchimie entre tous les membres du groupe.

Johannes, le chanteur, s'est amusé à monter dans les galeries techniques supérieures entre deux chansons pour faire une sculpture en baudruche à lancer dans la foule.
Il a ensuite dégainé un trombone pour une des chansons, avant de redescendre sur scène.
Ensuite, lors d'un intermède, un des roadies est venu apporter un énorme paquet cadeau qu'il a déposé devant la batterie.
Et là, surprise ! Johannes est sorti de la boite, avec plusieurs ballons à la main. Effet garanti !
Plus tard, il nous a offert une version de Tower, tout seul sur scène avec un piano.
Un moment très intimiste, mais non moins chargé en émotions.

Tout au long du show, John, le batteur, a joué de manière à ressembler à un mannequin mécanique.
Cela pouvait sembler étrange visuellement, mais avec le décor rappellant le haut d'une horloge Suisse avec ses petits personnages mécaniques, c'était tout à fait approprié.
À noter que cela fait un moment qu'il joue comme ça, mais ce soir-là on sentait vraiment une montée de niveau sur cette partie du jeu de scène par rapport aux concerts précédents.
Deux des interludes étaient d'ailleurs menés par lui,
faisant le pitre sur scène avec un des roadies qui venait sur scène pour l'occasion.
Un de ces moments était d'ailleurs dédié à introduire la chanson _A Statue Of The King_.
Pour ce faire, John a joué avec deux énormes canons à confettis qu'ils a actionnés en direction de la foule.
Chacun des deux tirs était synchronisé avec une descente de banderole sur laquelle il y avait un énorme imprimé de Jonas en tenue de roi.
C'est d'ailleurs à ce moment-là, pour introduire la chanson, qu'il est apparu sur scène dans la tenue liée à ce personnage.

Malgré la fosse extrêmement dense, les pogos ont immédiatement pris, et ce, tout au long de la soirée.
Ça devait bien transpirer là-dedans, vu que même tout en haut des gradins, on transpirait sans bouger.
Il faisait vraiment chaud à l'intérieur.
Preuve qu'il n'y avait pas que le groupe qui se donnait à fond !

Mention spéciale au jerrican dont se sert toujours Johannes pour boire entre deux chansons.

## Liens

- [Kassogtha](https://kassogtha.com/)
- [Veil of Maya](https://www.metal-archives.com/bands/Veil_of_Maya/45084)
- [Avatar](https://avatarmetal.com/)
