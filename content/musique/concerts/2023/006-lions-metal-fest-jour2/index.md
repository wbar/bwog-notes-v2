+++
title = "Lions Metal Fest - Jour 2"
date = 2023-06-14T20:50:00+02:00
bands = ["Prismeria", "Strivers", "Nephren-Ka", "Deliverance", "Jours Pâles", "Catalyst", "Dust Bolt", "Hatesphere", "Belphegor"]
+++

Après une nuit ... Habituelle pour un festival, mais sans gueule de bois, c'est parti pour la seconde journée.
Celle-ci débute sous les meilleurs auspices : un ciel bleu, un sol à peine humide et les toiles de tentes déjà sèches.

En avant pour un retour de ce dimanche 4 juin.

<!--more-->

Je ne vais toutefois pas mentir : 
malgré la tente fresh & black, je me suis réveillé tôt.
Ce qui m'a largement laissé le temps d'émerger.

{{< image src="lmf-running-order.jpg" caption="Petit rappel du running order. Oui, on avait largement le temps avant la reprise programmée à 12h30." >}}


## Prismeria

On débute avec, surprise, un autre groupe originaire de Lyon.
C'est vraiment l'ADN du festival, c'est cool.

Finalement, pas grand-chose à dire niveau son encore une fois.
C'était très bien réglé.
Le groupe avait une sacrée énergie.
Mais personnellement, j'ai passé mon tour.
Leur son trop orienté metalcore malgré de gros morceaux de thrash ne m'inspire pas plus que ça.

## Strivers

Là encore, un groupe lyonnais mixant thrash, death et metalcore.
Cela dit, j'ai été bien plus intéressé que le groupe précédent.
Notamment grâce à leurs influences type Machine Head qui me parlent bien plus.
Les compos tabassent, mais restent néanmoins équilibrées.
Leur passage fut un très bon moment pour ma part.


## Nephren-Ka

Alors là, on ne rigole plus du tout !
De nouveau du Brutal Death pour secouer un peu plus cette après-midi !
Cette fois en provenance d'Auvergne.

Au vu du logo du groupe, aura-t-on droit à un son sonnant un peu comme Nile ?
Raté ! À mon oreille, ça se rapproche en fait de Septicflesh.
Mais finalement, la comparaison s'arrêtera là, et c'est une bonne chose.
Au vu du son et des compos, on sentirait presque une note et des gammes qu'on entend dès que quelqu'un veut faire sonner égyptien.
En fait, on se retrouve avec une sorte de mélange entre l'occultisme lourd (type Cthulhu) et la SF brutale (façon Dune).
Et ça prend !

C'est brutal, c'est intéressant, ça fait bouger la fosse.
Si vous les voyez un jour passer près de chez vous, foncez sans hésiter !

## Deliverance

Pour moi, ce fut la douche froide.
Je n'ai absolument pas aimé.

Le groupe mélange du black métal avec du post, puis un incorpore du sludge, voire du doom,
avec du psychédélique.

C'est très vague comme description,
mais en même temps c'est difficile à décrire.
Le pari artistique du groupe est risqué.
Ça ne plaira pas à tout le monde.
Cela dit, il restait quand même un bon paquet de personnes devant la scène.
Je pense donc pouvoir recommander aux fans de sludge de tenter le coup, sait-on jamais.

Du coup, j'en ai profité pour aller plier mon matos.
C'était à la fois une erreur et une bonne idée : il faisait très chaud à ce moment-là,
mais au vu de la météo et de mon co-voit qui ne voulait par partir trop tard,
j'ai pris une bonne décision.

## Jours pâles

Après la douche froide, la grosse claquasse dans la gueule !

J'ai adoré ce qu'ils ont fait.
L'énergie du groupe est incroyable sur scène,
et le rendu sonore excellent,
malgré les soucis de retours du batteur sur les deux premières chansons.

Comme pour Aorlhac, les paroles sont en français.
Seulement ici ça ne m'a pas perturbé du tout.
L'immersion a été quasi immédiate et totale pour moi.
Attention, la mélancolie est extrêmement présente dans l'univers proposé par le groupe.
Ça ne change pas beaucoup du désespoir habituel du black métal, mais là, on touche à un autre niveau dans la prise aux tripes.
Probablement grâce aux éléments mélodiques incorporés dans les chansons.
Ça permet d'avoir plusieurs ambiances entre deux chansons,
et même d'astucieux changements au sein d'une seule.

Le chanteur donnait tout ce qu'il avait, guitaristes et bassistes étaient portés par la musique,
tout comme le batteur, mais lui il arborait un immense sourire sous sa moustache,
quand bien même les morceaux de ce concert étaient très exigeants.

Une excellente découverte pour ma part, et un set qui était bien trop court à mon gout.

Là aussi, s'ils passent près de chez vous, n'hésitez pas, vous ne le regretterez pas.

## Catalyst

Hop, encore du bon gros death, pour le peu que je peux en dire.
Je les ai regardés de loin seulement.
J'ai pu apercevoir une salle bien remplie, mais je ne me suis pas approché vu que j'avais un coup de barre.
Et puis il faisait chaud, alors je suis finalement sorti prendre l'air.
J'en ai profité pour parler avec le batteur de Jours Pâles.

Mais de ce que m'ont dit mes potes, Catalyst vaut vraiment le coup.

## Nouvel orage

Oui, encore, mais un petit cette fois,
apportant un rafraichissement de l'atmosphère qui a fait beaucoup de bien.
Heureusement que j'ai plié ma tente avant.

## Fin

Il m'aura manqué Dust Bolt, Hatesphere et Belphegor.
Mais comme j'avais un covoiturage pour rentrer à une heure qui m'arrangeait bien,
j'ai préféré faire l'impasse plutôt que de rentrer tard dans la nuit ou le lendemain.

## Conclusion

Eh bien c'était ma première fois à ce festival et j'en suis très content.
L'orga était bonne et l'affiche incroyable pour un festival de cette ampleur : on parle d'environ 300 à 400 personnes par jour !
On pouvait sentir que c'était déjà leur cinquième fois au total,
et pas la première sur ce lieu.

Mon seul regret ?
Ne pas avoir correctement prévu ma fin de week-end,
ce qui fait que j'ai dû couper court.

Ah oui, il a également fallu changer de lunettes de soleil après ça ...

{{< image src="lunettes-foutues.jpg" caption="R.I.P mes lunettes de soleil. Le filtre intérieur a décidé de buller et/ou fondre un peu." >}}

Je pense que je vais y retourner l'an prochain !

## Liens

- [La page des artistes du festival](https://www.lionsmetalfestival.com/artistes-2018)
- [Prismeria](https://prismeria.bandcamp.com/)
- [Strivers](https://strivers.bandcamp.com/)
- [Nephren-Ka](https://www.metal-archives.com/bands/Nephren-Ka/3540286938)
- [Deliverance](https://deliveranceplease.bandcamp.com/)
- [Jours Pâles](https://www.metal-archives.com/bands/Jours_P%C3%A2les/3540479577)
- [Catalyst](https://catalystfr.bandcamp.com/)
- [Dust Bolt](https://www.metal-archives.com/bands/Dust_Bolt/3540304448)
- [Hatesphere](https://www.metal-archives.com/bands/HateSphere/1550)
- [Belphegor](https://www.metal-archives.com/bands/Belphegor/1952)

