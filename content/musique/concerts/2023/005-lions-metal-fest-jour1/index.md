+++
title = "Lions Metal Fest - Jour 1"
date = 2023-06-09T21:08:20+02:00
bands = ["Deos", "The Oath", "Lodz", "Cytotoxin", "Aorlhac", "Parasite Inc", "Pestilence", "Rotting Christ", "Necrotted"]
+++

Le Lions Metal Festival est un petit festival sur deux jours.
Il se situe à une demi-heure de route au sud de Lyon sur la commune de Montagny, qui jouxte Givors.

Deux jours à côté de chez moi avec plein de potes ?
Il ne m'en fallait pas plus pour foncer sur place !
D'autant plus que même avec de bonnes têtes d'affiches internationales,
le festival fait la belle part aux groupes français et surtout locaux.

C'est parti pour deux jours placés sous le signe du gros son : les 3 et 4 juin.

<!--more-->

_Remarque_ : Cette fois, les impressions sur les groupes seront bien moins développées qu'à l'accoutumée.
J'y suis allé en tant que festivalier, et j'avais décidé de profiter à fond,
sans réellement décortiquer ni retenir ce que j'allais écouter.
Si vous attendez des descriptions poussées pour chaque groupe, la déception va vous guetter.

En résumé (et pour la blague) : Petit festival, donc petite rétrospective !

## Un peu de contexte

C'est là la cinquième édition de ce festival.
Trois fois à cet endroit si j'ai bien compris,
et une tentative en plein-air dans un stade ailleurs sur la commune.
Visiblement il y a eu des soucis avec le voisinage, d'où le repli en intérieur cette année (spoiler : tant mieux au vu des surprises météorologiques).


{{< image src="lmf-running-order.jpg" caption="Running order effectif du festival" >}}

## Arrivée sur site et installation

Le site du festival se trouve dans une zone d'activités, dans l'enceinte d'un gymnase.
Pas de risques de déranger qui que ce soit donc !
Le parking n'était pas ouvert pour le public,
mais dans cette enceinte il y avait un coin d'herbe raisonnablement grand pour y planter un bon paquet de tentes.

{{< image src="lmf-arrivee-camping.jpg" caption="Le p'tit camping tout mignon au petit matin du premier jour. Youpi, il reste plein de place !" >}}

On ne le voit pas sur la photo ci-dessus,
mais il y a encore deux fois cette surface sur la gauche, en dehors du cadre.

Hop, on s'organise en cercle, on commence à tout installer, on bataille un peu avec les cailloux, et c'est fait.
Plus qu'à profiter d'être tous ensemble,
en attendant l'ouverture des portes pour récupérer son bracelet et faire un petit tour.

Le trait d'union est un complexe sportif disposant de plusieurs salles accolées les unes aux autres.
Au niveau du public nous avions accès aux WC qui servaient aussi de point d'eau (à toute heure),
au hall principal où il y avait les stands de merch, de rencontre avec les groupes,
mais également un bar.
Ensuite une première salle où il y avait la scène.
Derrière, il y a au moins deux salles, dont une visible depuis le hall,
réservée aux VIP.
Le reste du complexe n'était pas visible, mais après avoir discuté avec des bénévoles,
c'était assez grand pour accueillir les groupes et leur matériel.
En vrai, un bon endroit vu la petite taille du festival.

Et maintenance, place aux groupes !

## Deos

On commence tranquillement avec un groupe de death teinté tantôt de black ou de mélo sur fond de légendes et textes inspirés de l'empire romain.
Les costumes de centurions sont de rigueur.

Le son est lourd, l'ambiance porte immédiatement.
Le jeu de scène est minimaliste, mais finalement on s'en fiche, car ce groupe a été une très bonne entrée en matière.

Je les préfère même à Ex-Deo.

## Première pose inter-groupes

Un petit paragraphe qui ne reviendra pas à chaque fois.
Juste pour signaler qu'entre chaque groupe, il y avait une demi-heure de pause, le temps de changer le plateau.
Vu qu'on avait notre campement juste à côté, on en profitait à chaque fois pour aller s'asseoir, prendre l'air, et se poser un petit peu.
Un avantage indéniable de ce festival, et un moment plus que bienvenu.

Et quand je dis à côté, c'est littéralement à 50 mètres.

## The Oath

Le groupe commence à avoir de la bouteille et propose un mélange black / death / mélo mais pas que, qui est rudement efficace.
C'est presque surprenant d'avoir un tel groupe en second.
Cela dit je pense que vu les autres groupes sur l'affiche, le running order a dû être une plaie pour l'orga.

Dans tous les cas, le public (qui a commencé à bien être présent) semble avoir accroché.
Moi également, même s'il faut avouer que ça n'était pas encore beaucoup monté pour moi.

## Lodz

On redescend d'un cran dans la brutalité avec du post-metal atmosphérique.
Cependant, ça n'en était pas moins lourd.
Enfin, pas tout le temps.

Je dois par contre admettre que j'ai beaucoup moins accroché, vu que le groupe propose beaucoup de sonorités qui lorgnent allègrement sur le doom,
qui est un style que j'apprécie peu.

Après, force est de reconnaitre qu'avec le bon son qu'ils avaient à leur disposition,
et la qualité de leurs compositions,
ça faisait un très bon fond sonore pour profiter du moment sans être spécifiquement impliqué.
En fait j'ai surtout décroché à cause de la grande différence qu'il peut y avoir entre deux morceaux.

À l'inverse, je pense que les fans étaient transportés et n'ont pas vu le temps filer.

## Aorlhac

En tant que fan de black métal j'ai été plus que ravi de pouvoir les découvrir,
surtout après avoir eu de nombreuses incitations de la part de nombreuses connaissances.
Attention, les chants sont en français.
Ça fait limite bizarre de pouvoir comprendre ce qui est chanté sans avoir à faire d'effort particulier. :D

Le petit détail qui m'a amusé, c'est le batteur : on a l'impression qu'il se promène et que ça ne va pas assez vite pour lui.

Dans tous les cas, le son proposé et les compos tombent pile-poil dans ce que j'apprécie dans ce style et j'ai passé un super moment.

## Necrotted

Hop, on enchaîne sans transition avec du bon gros death comme j'aime.
Cela dit je n'ai pas profité autant que j'aurais voulu.
Ayant eu un coup de barre à ce moment, je les ai regardés de loin puis je suis allé rapidement me poser au camping.
C'est presque un regret de ma part.

Sur la partie à laquelle j'ai assisté, il en est ressorti que le groupe propose un son moderne,
avec des emprunts très présents au djent.

## Cytotoxin

(Oups, la petite faute sur le running-order :P )

Après une découverte en 2020, un premier concert au Rock n Eat (non chroniqué) et un autre concert au Summer Breeze 2022,
voilà que j'ai à nouveau l'occasion de les revoir.
Et je l'ai fait avec un plaisir non dissimulé.

Ça tabasse toujours autant.
Leur entrée sur scène avec leurs seaux et masques à gaz sur fond de fumée et bruits de compteur Geiger donnent toujours un petit effet.
Puis, comme attendu ça part fort.
C'est parti pour une grosse demi-heure de death brutal, de circles-pits et de sueur radioactive.
Et en prime, le retour du panneau de rond-point agité par le chanteur pour lancer la foule !

## Aparté météorologique

Un énorme orage a éclaté en début de soirée sur Lyon.
De notre côté, et fort heureusement, nous n'avons eu que des restes.
Ou bien une autre cellule fortement affaiblie par celle qui a éclaté au-dessus de la ville.
Ce qui fait que le sol était à peine mouillé et que nous n'avons pas eu à déplorer une quelconque inondation dans nos tentes[^1].
Cela dit, la pluie est quand même tombée jusqu'au milieu de la nuit.

## Parasite Inc

Un peu de death mélo pour la route ?
Allez c'est parti !

En vrai je ne sais pas comment décrire ce groupe.
On a une base d'arpèges tout au long des morceaux (ou presque),
accompagnés d'une rythmique forte et une batterie qui suit,
mais avec peu de variations je trouve.
Parfois du synthé.
Ouais bon, je décris du death mélo quoi ...
La voix me fait penser à du Heaven Shall Burn en un peu plus étouffé.

Cette description n'est pas spécialement flatteuse c'est vrai,
par contre dans les faits, il faut reconnaitre que c'était efficace,
et c'était une bonne idée de placer ce groupe à 20h10 pour redonner la pêche au public.

Bref, c'est allemand, c'est carré, efficace, mais ça ne déborde pas.
Je regrette l'utilisation de samples pour le synthé.


## Pestilence

Je les ai loupés, je faisais une petite sieste.

## Rotting Christ

Pas besoin de les présenter je pense.
Monstres grecs du black métal, c'est la seconde fois que je les vois.
La première c'était en 2019 à Bourg-En-Bresse, et c'était déjà une énorme claque.
Cette seconde fois n'a pas été différente, malgré la fatigue.
En plus il y avait des morceaux du nouvel album !

## Conclusion de cette fin de journée


Globalement le son a été au minimum satisfaisant, régulièrement très bon.
Même pour les premiers groupes où c'était l'ingé son du fest.
C'est assez rare pour être signalé.

On sent que l'orga est rodée, même pour un petit festival.
Après tout, c'est quand même la cinquième fois qu'il est organisé.
Je n'ai absolument rien à reprocher. Et ça, c'est pas souvent !

## Liens

- [La page des artistes du festival](https://www.lionsmetalfestival.com/artistes-2018)
- [Deos](https://www.metal-archives.com/bands/Deos/3540405114)
- [The Oath](https://www.metal-archives.com/bands/The_Oath/14152)
- [Lodz](https://lodztheband.bandcamp.com/)
- [Cytotoxin](https://www.metal-archives.com/bands/Cytotoxin/3540325917)
- [Necrotted](https://www.metal-archives.com/bands/Necrotted/3540306787)
- [Aorlhac](https://www.metal-archives.com/bands/Aorlhac/3540264432)
- [Parasite Inc](https://fr.wikipedia.org/wiki/Parasite_Inc.)
- [Pestilence](https://en.wikipedia.org/wiki/Pestilence_(band))
- [Rotting Christ](https://www.metal-archives.com/bands/Rotting_Christ/314)


[^1]: Celles de mes potes et moi en tout cas. Le reste du camping, aucune idée, mais personne n'avait l'air affolé donc je suppose que ça été pour tout le monde.


