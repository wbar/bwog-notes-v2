+++
categories = ["concerts"]
title = "Kissin' Dynamite et Blackrain au CCO (Villeurbanne)"
date = 2019-10-25T09:15:57+02:00
draft = false
bands = ["Kissin' Dynamite", "Blackrain"]
+++

Ça y est, la voici !
La première rétrospective de concert de ce blog !
Pour commencer, je vais vous parler de Kissin' Dynamite et de Blackrain, qui sont passés le 24 octobre au CCO à Villeurbanne.

Sortez vos plus beaux pantalons moulants (spandex autorisé) et vos bandanas fluos !

C'est parti !

<!--more-->

Déjà, il faut savoir que j'étais côté organisation[^1], car je suis bénévole dans une association de concerts sur Lyon.
Du coup, même en étant au bar (au CCO on y voit toute la scène ainsi que la fosse), j'aurais certainement loupé pas mal de choses.
Après tout, quand on est occupé à ravitailler les soiffard(e)s, on a pas trop l'esprit à analyser l'évènement.

## Les groupes

Blackrain est un groupe de Haute Savoie avec déjà de l'expérience (ils se sont formés en 2006),
et Kissin' Dynamite vient d'Allemagne (formé en 2007, mais actif depuis 2002) dont le genre est un mélange de Heavy / Glam Rock.

## Le concert

On va pas se mentir, je ne suis pas fan de ce genre.
Par contre, au niveau de la scène Lyonnaise, il y a beaucoup de gens qui apprécient.
Ce n'est pas un jugement, c'est un constat : la salle s'est bien remplie !
Et ça fait plaisir, vu que les groupes ont pu jouer devant suffisamment de personnes pour pouvoir s'éclater.
D'ailleurs, ça se voyait.
Les artistes ont beaucoup donné sur scène, contents d'être là, et de pouvoir se produire devant un public réceptif.
Clairement, ils n'étaient pas là pour juste encaisser leur chèque, et ça fait plaisir, car ces derniers temps,
l'équipe de bénévoles et moi avons vu beaucoup de groupes présents uniquement dans le but de toucher leur cachet[^2].

Le show était dynamique, le son satisfaisant, même depuis le bar, et le niveau musical était là.
Les compos sont carrées et maitrisées.
Le jeu de lumière a été très bien géré.

Au niveau de la fosse, c'était bien rempli (300 personnes à vue de nez), tous à fond pendant les prestations des groupes.
Après, j'ai eu l'impression de trouver ça ... Mou.
Forcément, quand on est habitué au Death (et quelques unes de ses déclinaisons) où ça bouge énormément (POGOOOOOOO !!!),
et bien là, juste quelques sautillements et tapages de mains en rythme, au dessus de la tête, ça fait bizarre.
Mais les interactions entre le public et les groupes étaient là.

Et ça n'a absolument pas désempli.
Très peu de gens sortaient, ou venaient nous voir au bar.
Sauf à l'inter-groupes, évidemment !

## Les roadies

Comme d'hab, ces acteurs de l'ombre n'ont pas réellement de présence du point de vue du public.
Mais côté orga, on est bien contents qu'ils soient là, parce que pendant les changements de plateau,
quand il s'agit de déplacer autre chose que le backdrop, avoir des pros qui savent quoi changer et comment pour que ça aille vite,
c'est un atout non négligeable pour le concert (et la santé du matériel).

C'est d'ailleurs assez amusant comme sensation, quand on monte sur scène, et qu'on jette un œil rapide à la foule depuis ce point de vue,
alors qu'on est en train de trimballer un gros machin qui sert à faire du bruit.

Mais le plus impressionnant, jusqu'ici c'était bien l'équipe de ces deux groupes.
Le déchargement de la scène à été tellement rapide et efficace que les caisses et autres objets étaient remplis et empilés au pied de la scène,
alors que la sécurité n'avait pas encore terminé de faire évacuer la salle[^3].
On a eu le temps de se poser un bon quart d'heure avant de pouvoir se mettre à charger le trailer.

Ensuite, tout est allé très vite.
Bouger les caisses, porter les quelques cartons du merch et sacs mous.
Monter les éléments dans le trailer en suivant les instructions (en Anglais _of course_) du gars qui est chargé de remplir.
Fermer le trailer du premier coup, car tout rentre au micro poil, alors qu'il n'y a plus d'espace disponible une fois tout le matos rangé.
C'est toujours aussi impressionnant !

## Les détails amusants

- Le fan hardcore qui essaie de passer la sécu, pour revenir dans la salle, pour tenter de voir le groupe
(qui était parti se doucher dans les loges), et indique qu'il les suit depuis le début de la tournée et fera le tour d'Europe avec eux.
- les fans qui étaient devant la salle plus de 2h avant l'ouverture des portes, qui se précipitent au premier rang quand ça ouvre,
et qui ne bougent plus pendant 3h[^4].
Sauf leur pote malchanceux / généreux qui est venu au bar pour ravitailler sa bande.

## Le détail qui pique

La voiture garée comme une merde devant la salle, ce qui a empêché le tour bus de repartir à l'heure ... Sérieusement ...
J'ignore comment ça a terminé (sûrement à la fourrière), mais ça a emmerdé tout le monde,
surtout le régisseur de la salle qui a mieux à faire de ses fins de soirées.
Ne faites pas ça ! Jamais !

## Le mot de la fin

On aura pas vendu beaucoup de bières (seulement 4 fûts), mais on a pu proposer un beau concert tout de même.
Le public semblait très content d'avoir passé la soirée ici.
Les groupes (pour avoir pu discuter vite fait) aussi.
Et leurs roadies, surpris qu'on ait des bras pour les aider, également.

En bref, une excellente soirée !


## Des liens sur les groupes

- [Blackrain](https://www.metal-archives.com/bands/Black_Rain/21769)
- [Kissin Dynamite](https://www.metal-archives.com/bands/Kissin'_Dynamite/3540258296)

[^1]: comme pour beaucoup d'autres dates à venir, mais je préciserait à chaque fois, n'ayez pas peur.
[^2]: Oui, je sais, une tournée c'est éreintant. Mais même fatigués, il y a heureusement des groupes qui donnent tout ce qu'ils ont.
[^3]: Non pas que la sécu ne faisait rien, au contraire. C'est juste que les roadies ont été trop efficaces par rapport à l'heure convenue d'évacuation de la salle.
[^4]: Je sais que c'est une pratique courante, dans les gros concerts. Mais vu que je suis surtout dans des petites salles, ça reste exceptionnel pour moi.

