+++
categories = ["concerts"]
title = "Skáld au Ninkasi Kao (Lyon)"
date = 2019-12-13T19:49:49+01:00
draft = false
bands = ["Skáld"]
+++

Mardi 10 décembre, les chants Nordiques emplissaient la salle du Kao et les coeurs des spectateurs présents ce soir là.
Nous avons pu être gratifiés de la présence de Skáld, groupe pagan/folk[^1] Français tout frais dans l'univers musical.

La curiosité était de mise, car en dehors de grands noms comme Fejd, Wardruna ou encore Heilung,
on a rarement l'occasion d'avoir ce genre de concerts en dehors des festivals.
Surtout que là, le groupe était seul.
Oui.
Un seul groupe pour la soirée.

<!--more-->

On se dit que c'est un peu court, dommage, perturbant, inhabituel, ou plein d'autres mots indiquant un certain décalage avec nos habitudes.
Mais pourtant, une fois le concert lancé, et bien on s'en fiche !

## Petits détails avant le concert

Le groupe devait initialement débuter à 19h30.
Mais il y a eu une affluence monstre ce soir là au niveau du public.
La file pour rentrer était monstrueusement longue.
J'ai rarement vu ça là bas[^2].
L'organisation a du retarder un peu le concert afin que les spectateurs puissent être suffisamment nombreux.

## Et le concert alors ?

Pour commencer, quelques mots à propos de la qualité du son.
Elle était irréprochable.
Les mauvaises langues diront que c'est un groupe plus facile à sonoriser que lorsqu'il y a une grosse batterie accompagnée de guitares bien saturées.
Il n'empêche que dès le départ, c'était nickel.
Ce qui a permis de rentrer dans l'univers du groupe dès les premières notes.

Il y avait aussi une certaine économie d'effets de lumière qui rendait le tout très posé, très aérien.
Parfait pour une ambiance quasi mystique, propre aux chants nordiques d'antan.
Ce monde était servi par une set-list très bien pensée, même si j'ai entendu quelques personnes piailler à propos de quelques chansons manquantes.
N'oublions pas que le groupe est récent, il n'y a donc pas (encore) matière à jouer pendant des heures.

Le son et la lumière c'est bien, mais qu'en est-il des musiciens ?
Et bien pour ça aussi, c'était la claque.
Chacun maitrise clairement ses instruments.
Car oui, ils sont multi-intrumentistes.
Et ce sont de vieux machins qu'ils utilisent là[^3] !
Et pour couronner le tout, la voix de la chanteuse est impressionnante !
Sans exagération, elle développe une puissance et une telle largeur de gammes ...
Il y a largement de quoi rester pantois.
Ajoutez à ça un excellent complément des voix masculines ainsi qu'une profondeur qu'on ne ressent pas sur le CD,
et vous avez là un sentiment et un voyage musical réellement forts.

A noter que cette puissance en plus est également présente au niveau des percussions, qui rajoutent encore plus de relief à l'ensemble.

## Et après ?

Et bien ... Mince ! C'est déjà terminé ?
Et oui !
Aussi surprenant que ça puisse paraître, la prestation des artistes n'a duré qu'une heure et demi.
Temps classique pour un groupe.
Mais comme il n'y avait que celui-ci ce soir, et bien forcément, c'était « un peu court ».

Mais finalement en y réfléchissant bien, on se dit facilement que même si on a adoré, plus, ça aurait été trop.
La set-list était largement suffisante.
On aurait apprécié une petite impro d'un des musiciens, mais vu la qualité du concert, c'est franchement pinailler.

On avait même l'occasion d'aller discuter avec le groupe à la fin à leur stand de merch.

Un concert extrêmement satisfaisant !

## Liens vers le groupe

- [site officiel](http://skaldvikings.com/)
- [page Wikipedia](https://fr.wikipedia.org/wiki/Sk%C3%A1ld_(groupe))

[^1]: Pas certain que ce soit la « vraie » classification, mais vu la complexité des genres on va s'en tenir à ça. Bien plus simple pour tout le monde.
[^2]: La dernière fois, c'était pour le concert anniversaire de la Rue Ketanou.
[^3]: J'ai pas encore creusé plus que ça, mais il semble que, évidemment, ce soient des versions modernes d'instruments historiques.

