+++
title = "Nile, Krisun, In Element et Naraka au CCO (Villeurbanne)"
date = 2022-11-26T22:44:49+01:00
bands = ["Nile", "Krisun", "In Element", "Naraka"]
+++

Jeudi 17, la soirée était placée sous le signe du Brütal Death,
avec des morceaux de technique ou de mélodique dedans, en fonction des groupes.

<!--more-->

La soirée a débuté avec Naraka, un groupe tout droit venu du Canada, pour nous délivrer un métal bien lourd.
Cela dit, devant seulement 20 personnes.
Un moment plutôt dur, car le groupe donnait beaucoup d'énergie, et franchement, ça donnait très envie de bouger dans la fosse.
Seulement là, avec si peu de gens, mais un groupe aussi motivé, ça faisait pleine à voir.
On a tenté de combler un peu et de ramener un peu plus les gens sur le devant de la scène avec plusieurs bénévoles, 
mais ça n'a pas vraiment prit.
Dommage pour le groupe, mais tant mieux pour le photographe qui pouvait choisir ses angles de vue sans problème.

Les gars de Naraka auront fait contre mauvaise fortune bon cœur en délivrant un petit set comme si la salle était totalement remplie.
En tout cas, leur son était propre, et leur compos très bien ficelées de mon point de vue.
Avec une très bonne puissance vocale de la part du chanteur, ainsi qu'une très bonne occupation de la scène.

Ensuite, ce fut au tour de In Element.
Un groupe venu d'Argentine.
Je pourrais moins en parler car le public a commencé à venir à ce moment-là, et on a commencé à avoir un peu d'affluence au bar.
J'ai aussi été beaucoup moins conquis par ce groupe.
Le mélange de métal moderne, empruntant beaucoup au djent, pour une soirée avec Nile en tête d'affiche, et après Naraka semble beaucoup trop léger pour moi.
Et pour un bon nombre de spectateurs également semble-t-il.
Les passages en voix claire ne doivent pas aider.
Cependant, force est de constater que pour la majorité, ça prend.
Tant mieux.
Ça aurait été dommage d'avoir un enchainement de groupes devant une salle vide.

Vient ensuite Krisun.
Ça se calme d'ailleurs au bar, du coup je peux commencer à en profiter.
Et bon sang, quelle claque !
Nos artistes brésiliens nous en mettent plein les oreilles avec des compos maitrisées et un son au poil.
On a clairement monté d'un cran en termes de maitrise.
Ça n'a pas des masses bougé dans la fosse, mais ça le méritait amplement.
On est pile-poil dans le gros brûtal death, et c'est précisément un groupe où j'aurais pu payer ma place pour être au centre si je connaissais avant.
La prestance des musiciens est là, accompagnée à partir de la moitié de leur set d'enfin des départs de pogos !

Il va falloir que je me penche plus sérieusement sur leur discographie !

Et pour terminer, Nile.
Le clou du spectacle ?
J'y croyais au début, les ayant découverts en 2015 au Fall of Summer.
J'en avais d'ailleurs gardé un excellent souvenir.
Pourtant, malgré un set carré et un death bien brutal avec des bouts un peu techinque, je ne sais pas pourquoi, la sauce n'a pas prit.
Je me demande encore pourquoi.
Dans un premier temps, c'est clairement la faute au son.
Il n'était pas terrible. 
Meilleur sur les côtés, moins bon en face de la scène, et limite pire à côté de la régie.
Ça la fout vraiment mal, sachant que c'était leur sondier aux commandes.
À mon avis, la faute à Krisun juste avant, qui était pour moi le gros point fort de cette soirée.
Cela n'a pas eu l'air de déranger le public par contre.
Il que faut dire que ce côté un peu crade à l'oreille fait tout de même partie de leur identité sonore.

## En bref

Si je dois retenir et vous conseiller deux noms à retenir : Naraka et Krisun ! Foncez !

## Liens

- [Naraka](https://naraka.bandcamp.com/)
- [In Element](https://www.metal-archives.com/bands/In_Element/63997)
- [Krisiun](https://www.metal-archives.com/bands/Krisiun/197)
- [Nile](https://www.metal-archives.com/bands/Nile/139)
