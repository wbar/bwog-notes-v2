+++
title = "Primordial, Swallow the Sun et Rome au CCO (Villeurbanne)"
date = 2022-04-14T09:03:03+02:00
draft = false
bands = ["Primordial", "Swallow the Sun", "Rome"]
+++

Hier soir au CCO, c'était le feu (heureusement, pas littérallement, mais j'y reviendrai).
Et pour cause : il y avait Primordial en tête d'affiche !

Le public était au rendez-vous, avec le sourire, et l'alchimie avec les groupes était là.
Ça fait rudement plaisir !

<!--more-->

On pouvait croire la salle comble d'ailleurs, mais dans les faits non.
On a scanné environ 300 entrées (je n'ai plus le nombre exact en tête), mais en termes de ressenti, on aurait dit qu'il y avait 500 personnes !

Mais trêve, de meublage, place à la soirée.

En vérité, tout s'est passé ... TRÈS vite.
À peine les portes ouvertes que nous nous sommes retrouvés débordés à la buvette.
Et ça n'a pas cessé jusqu'au début de Primordial.
Avec toutefois une légère baisse de fréquentation avant les intergroupes. Normal.

Cependant, j'ai tout de même pu profiter, au moins auditivement du premier groupe : Rome.
C'est une espèce de folk, moitié mélancolique (tiré du black métal je suppose), moitié dynamique (un côté indus' je dirais.).
Difficile à décrire, mais le public était déjà très présent devant la scène.
C'est assez rare pour un premier groupe, de manière générale.
Je pense que le fait que ce soit un premier « gros » concert de reprise a joué.
C'était tout de même un fond sonore très agréable pour accompagner le service.
Et un échauffement doux pour les oreilles : la folk, même amplifiée, rempli moins le spectre sonore et fatigue donc moins les oreilles.

Vint ensuite Swallow The Sun.
Je ne connaissais que de nom, mais la montée en gamme niveau énervement et ambiance était palpable.
D'ailleurs, on a pu avoir un peu de répit car le public a commencé à se concentrer sur le groupe plutôt que sur la buvette.
Enfin un peu, mais pas trop, car même si on a pu rapidement se relayer pour prendre l'air, on était en flux constant.

Et enfin, après une dernière cohue intergroupe : Primordial.
Bon sang, quelle bête de scène ce groupe !
Je voulais les voir depuis très longtemps en concert, et bien je n'ai pas été déçu !
Dès les premières secondes, on est captivé, on est pris dans leur univers, on se prend le rouleau compresseur dans la face !
Le temps a complètement changé son cours, distordu dans ma perception de fan et surtout leur jeu de scène.
Je pense que c'était la même chose pour le public car tout le monde était devant la scène, à répondre aux sollicitations du groupe, à bouger avec lui au fil des chansons, et à l'acclamer.
Un excellent moment qui a malheureusement été coupé (à la fin d'une chanson, marrant comme hasard) par le déclenchement de l'alarme incendie.
Une fin brütale du set, à 15 minutes de la fin.

On n'a pas cherché à tortiller, la sécu a fait évacuer tout le monde rapidement, et on s'est tous retrouvés dehors.
Au niveau de l'orga (et les équipes du CCO), l'inquiétude était surtout de savoir si oui ou non un feu était réellement parti.
Au niveau du public, la grande question était : on pourra rentrer ?
Il faut dire que vu l'heure, ça semblait compromis.
Pourtant, après une quinzaine de minutes, le verdict tombe : fausse alerte !
Tout le monde réintègre alors la salle, et surprise : Primordial reprend la scène pour terminer son concert.

Une excellente soirée donc, et malgré des problèmes côté organisation, rien n'a été perçu par le public qui a pu, malgré l'interruption, profiter pleinement de l'évènement.

Pas mal de personnes ont félicité l'association, ça fait chaud au cœur.
Ah bordel, que ça fait du bien de reprendre !

Vivement la prochaine date : Lundi 18, toujours au CCO, avec Amenra, et votre serviteur derrière le comptoir. :)

## Liens

- [Primodrial (site officiel)](https://www.primordialofficial.com/)
- [Swallow The Sun (site officiel)](http://swallowthesun.net/)
- [Rome (Wikipedia FR)](https://fr.wikipedia.org/wiki/Rome_(groupe))


