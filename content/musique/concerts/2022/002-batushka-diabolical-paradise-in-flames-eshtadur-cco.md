+++
title = "Batushka, Diabolical, Paradise in Flames et Eshtadur au CCO (Villeurbanne)"
date = 2022-04-24T11:46:50+02:00
bands = ["Batushka", "Diabolical", "Paradise in Flames", "Eshtadur"]
+++

Vendredi dernier (le 22 avril) se tenait date plus qu'intéressante avec 4 groupes,
le tout placé sous le signe du black métal.

La soirée fut folle, même si c'était mal parti.

<!--more-->

Si je pars sur une note négative d'entrée de jeu,
c'est parce que c'était mal engagé.
En effet, les groupes sont arrivés très tard, à cause d'un problème de camion sur la route.
Du coup il a fallu décharger le trailer rapidement et faire les réglages son tout aussi vite.
Même si les ingés son savent faire,
procéder aux réglages dans une salle avec aussi peu de temps avant l'ouverture des portes s'est avéré difficile, voire incomplet.
Les personnes présentes ce soir là ont pu en avoir un aperçu sonore assez flagrant.
Pour rajouter au stress ambiant, au bar, on avait une tireuse capricieuse :
celle-ci fuyait au niveau du réservoir d'eau, et impossible d'en avoir une de secours. 
On a dû faire avec.

Le comble, un groupe a annulé sa présence sur la tournée.
Initialement, il devait y avoir cinq groupes.
Ce détail nous a néanmoins permis de décaler un peu la date d'ouverture des portes,
permettant aux artistes et équipes techniques de gagner un temps précieux.

Heureusement, le reste s'est super bien passé, mais s'est très rapidement enchaîné.

## Ouverture des portes et Eshtadur

À ce moment là, le public a commencé à arriver au compte-gouttes devant la scène.
Nous avons pu commencer à servir les premières bières tranquillement avant que le groupe commence.
Enfin pas trop au final, car avec le timing serré de ce soir là, il n'y avait qu'un quart d'heure avant le début réel du concert.

Eshtadur a donc commencé devant peu de gens, mais quelque part, tant mieux.
Le son à la première chanson était franchement mauvais.
Heureusement que le sondier a pu corriger le tir rapidement.

Le début du set a d'ailleurs motivé les gens à rentrer plus vite, et même si on n'était pas débordés à la buvette, à partir de cet instant, on est restés en flux tendu.
Les spectateurs déjà présents dans la salle ont pu apprécier un son très death, assez peu black.
Visiblement, un changement de direction de la part du groupe.
Sympa pour se mettre en jambes en tout cas.

## Paradise in Flames

Le temps alloué au premier groupe était très court, de l'ordre de la demi-heure,
nous avons donc rapidement pu passer au second.
On avait un bon remplissage de la salle à ce moment là, je pense même qu'on avait atteint le maximum par rapport au public qui avait prévu de venir voir.
En tout cas, je n'ai pas trop pu me concentrer sur le groupe (nous étions toujours en flux constant de service),
mais de ce que j'ai pu décortiquer, on avait là un son qui me faisait penser à du Old Man's Child avec une choriste, ou bien du Cradle Of Filth, mais en plus moderne.
Une bonne continuation donc, mais encore avec ce problème de son très audible à la première chanson.

## Diabolical

Alors là, on a eu une petite baisse de fréquentation de la buvette, ce qui m'a permi de pouvoir un peu regarder ce troisième groupe,
qui m'a d'ailleurs bien charmé les oreilles : un bon gros mélange Death / Black des familles !

Ce groupe n'avait d'ailleurs aucun problème de réglages au début de son set.
Et la salle, qui était déjà bien dans l'ambiance a pu monter d'un cran encore.

D'un cran donc, mais à l'aveugle !
Diabolical ayant _légèrement_ abusé de la machine à fumée, au bout d'un moment on ne voyait plus grand-chose. :D

Le groupe ne devait plus voir que la moitié du public, nous depuis le bar on ne voyait quasiment plus la scène,
et à mon avis, dommage pour celles et ceux au fond de la salle qui ne devaient plus rien voir du tout.

Un très bon moment musical en tout cas.
Il faudra que je prenne le temps de creuser la discographie de ce groupe !

## Batushka

Alors là, comme d'hab, un super groupe, avec un super jeu de scène.
Bon, faut aimer le délire messe orthodoxe, mais ça claque, l'ambiance colle vraiment aux chants et l'univers est cohérent.

Ça commence par allumer silencieusement des bougies sur quelques piédestaux,
ça répand ensuite de l'encens (un moment bienvenu pour nos naseaux, car la fumée artificielle, quoi qu'on en dise, ça pue),
une arrivée encapuchonnée et ... ÇA PART ! Vite, fort ! Et ça ne s'arrête quasiment pas.
Avec un son au poil dès le début. Chapeau !
Le set était un bon mélange du nouvel album et quelques morceaux phares de l'ancien.

En vrai, je trouve ça assez difficile de rendre à l'écrit la manière dont ça s'est déroulé.
Tout ce que je peux dire, c'est que j'étais très content que la quasi-totalité du public soit devant la scène,
et pas accoudé au bar ni dehors.
Public d'ailleurs bien à fond pour Batushka qui semblait être attendu depuis leur dernier passage à Lyon.

Avec le nombre de gens devant la scène, concentrés sur le groupe, on a pu commencer à anticiper sur la fin du service,
tout en prenant le temps de bien profiter.
Et ça, c'était super chouette.

## Fin du concert et rangement

Au sortir de cette soirée, tout le monde semblait content.
Le public, et l'équipe.
Après un départ difficile on a terminé sur une note super positive et ça nous a bien motivés pour ranger rapidement et efficacement.

On est toujours ravis quand le public a passé un super concert,
mais il faut avouer qu'avec l'ambiance interne et l'apréhension du début, qu'on a rarement eues,
forcément on avait l'impression d'être plus contents que d'habitude.

## Liens

- [Batushka (site officiel)](https://batushkaofficial.com/)
- [Diabolical (site officiel)](https://www.diabolical.se/)
- [Paradise in Flames (Encyclopaedia Metallum)](https://www.metal-archives.com/bands/Paradise_in_Flames/28485)
- [Eshtadur (site officiel)](https://eshtadur.com/)

