+++
title = "Summer Breeze - jour 4"
date = 2022-09-19T20:51:16+02:00
bands = ["Fiddler's Green","Heaven Shall Burn", "Dark Funeral", "Blind Guardian", "Hypocrisy"]
header_img = "sboa.png"
+++

Nous sommes enfin samedi 20, c'est-à-dire le dernier jour de cette édition 2022.
Il a beaucoup plu mais il reste encore toute une journée de concerts.
Est-ce qu'on va pouvoir en profiter comme on l'entendait, malgré la fatigue globale ?

<!--more-->

C'était mal parti.
La grosse pluie est revenue nous voir une grosse partie de la matinée et est restée jusqu'au début d'après midi.

Et puis, sur les coups de 14 h 30, la pluie semble se calmer.
Ça tombe bien, c'est le moment de partir pour la main stage.
On enfile les impers et en avant !
On traverse la boue, encore, et encore.
Mais heureusement, on sait qu'on est plus proche de la fin que du début.

## Fiddler's Green

Nous voici donc devant des Allemands qui font du punk-rock celtique.
Contre toute attente, ça rend super bien.
Il n'y a pas trop de problèmes de son pour l'instant et l'ambiance est instantanément festive.
C'est aussi à ce moment là que la pluie cesse totalement, pour nous laisser profiter pleinement de nos derniers concerts.
Chouette !
Grâce à ça, mais surtout à l'énergie du groupe, c'était le feu pendant une heure !

Le punk-rock celtique c'est déjà chouette en soirée,
mais en concert ça rend encore plus fort.
Surtout quand on n'a pas pu profiter autant qu'on voulait.
Là, clairement, avec le retour d'un temps clément, la foule s'est encore plus déchaînée que la veille.
C'était un énorme moment !

## Dark Funeral

Initialement programmé à 1 h du matin, le groupe se retrouve finalement sur le créneau de 16 h 10,
sur la main stage.
Ça fait bizarre de voir du bon gros black métal en plein après midi,
mais quelque part ça m'arrange,
car je ne sais pas si j'aurais eu encore assez d'énergie pour les voir à ce moment-là.

Malgré les codes du genre et un jeu de scène censé être minimaliste, le groupe a tout de même pas mal échangé avec la foule entre deux morceaux.
La set-list tabassait sévère et personnellement, j'ai été transporté tout le long.
Encore plus sur mes morceaux préférés, ceux issus de l'album « Where Shadows Forever Reign ».

Pour moi, l'heure est passée toute seule.
J'ai été réellement content de pouvoir enfin les voir sur scène.
Et pour une fois, les soucis de réglage son n'ont pas été gênants (les plus facétieux diront que c'est normal avec du black métal :p).

## P'tite pause

Hé oui, n'oublions pas qu'entre les concerts, il y a des petites choses à voir à côté pour patienter.
Bon, ce sont surtout des boutiques de merch et autres trucs typés métal.
Mais ça fait passer le temps de se balader et voir 
quelques petits stands d'artistes et d'artisans au milieu de toutes ces fringues et accessoires.

## Blind Guardian

Il est bientôt 19 h 10, et nous revoilà dans la zone de la main stage.
Un peu au loin car il y a déjà beaucoup de monde devant la scène, et que nous sommes plus curieux que fans.
Également car nous mangeons un petit bout.
Encore merci aux écrans géants qui permettent de bien profiter même un peu loin.

Si vous avez déjà écouté ou vécu un concert de Blind Guardian, vous savez comment ça commence : War of Wrath, puis enchainement avec Into The Storm.
La suite comprendra beaucoup de morceaux en commun avec l'album live, astucieusement appelé « Live », qui date quand même de 2003.
Morceaux dans le même ordre d'ailleurs, même s'ils ont remplacé certaines chansons par des plus récentes (probablement du dernier album qui était annoncé pour bientôt à ce moment là).

Il faut avouer que même si j'ai arrêté de les écouter il y a longtemps,
le fait que je m'en souvienne aussi bien est bizarre.
Mais le moment est plaisant. Très.
En plus les nuages se dispersent et le peu de soleil restant nous éclaire.

La quantité de gens devant la fosse montre que le groupe est toujours un nom assez imposant malgré son âge.
L'expérience se sent.
Le show est très efficace.

## Heaven Shall Burn

Avant le début du concert, le chanteur arrive seul sur scène et commence à haranguer la foule qui continuait de chanter « Valhalla ».
Là, il leur demande justement de ne pas s'arrêter et leur donne le rythme pour bien chanter le refrain.
Cette petite blague a bien pris.
Il faut dire que le groupe a fait une reprise de cette chanson, en invitant justement Hansi Kürsch (le chanteur de Blind Guardian).
Un chouette moment d'échange entre le public et Marcus Bischoff.
Un habile moyen de faire patienter, le temps que l'heure vienne et que le reste du groupe soit prêt.

Et ensuite, ça a tabassé.
Sévèrement.
Et sans s'arrêter pendant 1 h 30.
Rien à ajouter, c'est Heaven Shall Burn après tout.

Enfin sans s'arrêter, pas tout à fait.
Au début du concert, pendant la deuxième ou troisième chanson, je ne sais plus très bien,
il y a eu une coupure totale du son pendant près d'une minute !
Une. Minute.
C'est long, et incroyablement frustrant.
On ne sait pas ce qui s'est passé, mais le groupe a choisi de continuer à jouer plutôt que s'arrêter pour reprendre ensuite.
Sans doute parce que c'est bien plus facile à gérer,
mais peut-être aussi parce qu'ils ne s'en sont tout simplement pas rendus compte.
Les retours son pour les musiciens étaient encore fonctionnels.
À noter que malgré ce moment très gênant pour le public, tout le monde s'est mit subitement à applaudir à l'unisson plutôt que de se mettre à huer l'ingé son.
J'ai trouvé que c'était une excellente réaction.

Une fois le son revenu (même si des fois, main stage oblige, la voix du chanteur était régulièrement trop faible), la claque pouvait reprendre.
Car oui, pour moi, c'était un sacré moment.
Pas une découverte en tant que telle, mais comme je n'écoute pas énormément de morceaux de ce groupe, j'en ai pris plein les oreilles,
et c'était vraiment énorme !
Le groupe est dynamique, les compos percutantes, le jeu de lumière collait parfaitement à l'ambiance.
Je vais en écouter plus à partir de maintenant grâce à ça.

Le seul truc que je peux reprocher au groupe, c'est son utilisation un peu trop abusive des stroboscopes.
Ça me défonce mes petits yeux.

## Hypocrisy

Dernier groupe prévu, et dernier « gros » nom que l'on attendait de voir.
Je pourrais presque dire « le meilleur pour la fin » mais ça serait mentir.
On a déjà vu tellement de groupes énormes, ça serait bête de comparer sur ce terrain.
Dans tous les cas, après un concert désastreux (en termes d'ambiance dans la fosse) à Lyon il y a quelques années,
voici une revanche plus que bienvenue.
D'autant plus que nous sommes très bien placés pour pouvoir en profiter pleinement.

Avant même que le groupe ne démarre, un détail attire notre attention : la machine à fumée.
De manière étrange, elle souffle en continu.
Ce n'est pas un test, ni une mise en ambiance, car elle ne s'arrêtera pas de tout le set.
Un drôle de problème car rapidement la scène et le groupe se retrouveront derrière un épais rideau de fumée.

Malgré cela, le groupe nous délivre ses morceaux avec sa puissance habituelle, mais le charisme de Peter en moins.
Quoique, sa silhouette à travers ce brouillard artificiel, ça donne quelque chose de particulier.
Cela dit, le plaisir est légèrement gâché par le niveau de la voix de Peter souvent trop faible,
et toujours ces enceintes qui craquent. Rhaaaaaa !

Malgré les soucis, ce fut un excellent moment pour nous.
Et en plus on pourra de nouveau les voir le 16 octobre à Lyon ! \m/

## Et c'est la fin !

On a vu gros pour la reprise des fests, mais il fallait au moins ça !
C'est fatigués mais heureux qu'on quittera cette expérience.
Et si l'on refait le Summer Breeze un jour, ça sera avec des potes.
C'est toujours mieux à plusieurs les festivals !

## Liens

- [Fiddler's Green](https://www.fiddlers.de/en/)
- [Heaven Shall Burn](https://www.heavenshallburn.com/)
- [Dark Funeral](https://www.darkfuneral.se/)
- [Blind Guardian](https://fr.wikipedia.org/wiki/Blind_Guardian)
- [Hypocrisy](https://hypocrisyband.com/)
- [Les photos officielles des concerts](https://www.summer-breeze.de/en/bilder/festival/summer-breeze-2022/)

