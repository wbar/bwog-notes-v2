+++
title = "Obscura, Persefone et Disillusion au CCO (Villeurbanne)"
date = 2022-09-21T18:10:02+02:00
bands = ["Disillusion", "Persefone", "Obscura"]
+++

La saison des concerts en salle reprend sur les chapeaux de roues !
C'est donc tout naturellement qu'on a eu droit à une soirée death / prog vendredi 16 au CCO.

Seulement 3 groupes, mais avec quand même du lourd !

<!--more-->

La soirée est passée assez vite de mon point de vue, alors ce billet sera plus court qu'à l'accoutumée.

## Disillusion

Bon, on ne va pas se mentir, c'était sympa, mais sans plus pour moi.
Même si les compos du groupe sont bonnes,
j'ai trouvé l'ambiance trop légère, et ce n'est pas du tout ce que je recherchais ce soir là.
Trop prog et pas assez death peut-être.
Je tenterai de les écouter une autre fois.

Cependant du côté du public c'était un peu plus unanime.
Il y avait peu de personnes dehors, et pas trop de monde du côté du merch.
Même si ça ne bougeait pas beaucoup dans la fosse.

## Persefone

La superbe découverte à laquelle je ne m'attendais pas !
Pourtant toujours un peu prog, mais bien plus death et death mélo.
Un combo gagnant pour mes oreilles !

Le groupe a donné beaucoup d'énergie pendant son passage du scène et le public y a bien répondu.
On a pu commencer à voir les premiers pogos et circle pits.

## Obscura

C'est ce groupe là qui m'intéressait le plus.
C'est aussi à de moment que la foule s'est le plus désintéressée du bar.
Parfait pour en profiter depuis ma position.
Franchement, c'était la grosse claque !
Et de ce que j'ai vu, le public a été très réceptif également vu comment ça bougeait dans la fosse.

Un très bon groupe que je vais réécouter en CD avec plaisir !

## Côté orga

Enfin surtout côté buvette.
Avec environ 200, peut-être 250 personnes (je ne me souviens plus du nombre exact),
nous avons terminé 4 fûts de 30 L et 2 de 20.

Un joli score pour une reprise !

## Liens

- [Disillusion](https://www.metal-archives.com/bands/Disillusion/11560)
- [Persefone](https://www.metal-archives.com/bands/Persefone/12779)
- [Obscura](https://www.metal-archives.com/bands/Obscura/63100)

