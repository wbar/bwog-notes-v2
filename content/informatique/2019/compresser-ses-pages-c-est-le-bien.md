+++
Categories = [""]
tags = ["blog"]
title = "Compresser ses pages : c'est le bien !"
date = "2019-08-31"
draft = false
+++

En général il vaut mieux compresser des pages en `.gz` et configurer son serveur web pour les distribuer si le client les demande. Et maintenant c'est chose faite ici !
<!--more-->

J'aurais largement pu le faire avant, mais je voulais d'abord savoir si framagit (qui tourne sous gitlab) permettait de le faire.
La [réponse est oui !](https://framagit.org/help/user/project/pages/introduction.md#serving-compressed-assets)

Du coup, vu que j'utilise un blog statique et que je ne suis pas sur mon propre hébergement[^1], autant compresser à l'avance !
Pour ça, j'ai rajouté cette petite ligne dans mon script de publication[^2] :

{{< highlight shell >}}
find ./ -type f -regex '.*\.\(htm\|html\|txt\|csv\|xml\|svg\|js\|css\)$' -exec gzip -f -k {} \;
{{< / highlight >}}

Et HOP !
Les clients qui en feront la demande recevront les pages compressées depuis le serveur.
Ca économisera toujours ça de bande passante pour tout le monde.
Et du temps machine pour Framagit, qui n'a pas à compresser lui même les objets avant de les envoyer.

## Pour aller plus loin

[L'article de Lord sur la compression des pages](https://lord.re/posts/178-compression-gzip-static-nginx/),
qui m'a fait me décider.

[^1]: Et pour le jour où je le serais, je n'aurais qu'à configurer un p'tit coup le serveur.
[^2]: J'aurais pu le mettre dans le makefile de Pelican, mais j'ai pas réussi. Et j'ai la flemme !

