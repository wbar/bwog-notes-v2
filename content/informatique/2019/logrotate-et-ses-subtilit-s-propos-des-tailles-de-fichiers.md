+++
Categories = [""]
tags = ["logrotate"]
title = "Logrotate et ses subtilités à propos des tailles de fichiers"
date = "2019-08-14"
draft = false
+++

Pour éviter de remplir les disques de son système avec des fichiers de log, cet outil est efficace et simple à utiliser.
A quelques subtilités près, notamment avec l'option `size`.
<!--more-->

## Spécifier la taille ET la période ##

Il arrive qu'on ait des tâches cron qui lancent logrotate plus qu'une fois dans une journée.
Ou bien qu'on veuille faire une rotation hebdomadaire, sauf si un fichier dépasse une certaine taille.
Dans tous les cas, l'option `size` peut sembler intéressante et c'est souvent celle qui saute aux yeux à la lecture du manuel.
Mais elle ne peut pas être couplée avec une période (`daily`, `weekly`, etc.) car elle est prioritaire, quelle que soit la date de rotation du fichier.
D'ailleurs le manuel n'est pas clair là dessus, et ne le mentionne que pour une autre option : `minsize` :

> minsize size
>
>   Log files are rotated when they grow bigger than size bytes, but not  before  the  additionally  specified  time
>   interval  (daily,  weekly,  monthly,  or yearly).  The related size option is similar except that it is mutually
>   exclusive with the time interval options, and it causes log files to be rotated  without  regard  for  the  last
>   rotation time.  When minsize is used, both the size and timestamp of a log file are considered.

Bon, ça tombe bien, c'est cette dernière qui est inéressante pour le cas mentionné.
Il y a même une petite soeur bien sympathique depuis la version 3.8.1 en la présence de `maxsize` :

> maxsize size
>
>   Log  files  are rotated when they grow bigger than size bytes even before the additionally specified time interval (daily,
>   weekly, monthly, or yearly).  The related size option is similar except that it is mutually exclusive with the time interval
>   options, and it causes log files to be rotated without regard for the last rotation time.  When maxsize is used, both
>   the size and timestamp of a log file are considered.

Surtout sur les grosses périodes.
Pour les petites, je trouve `minsize` plus pertinente.

## Utilisation en ligne de commande ##

Plutôt que d'attendre que la prochaine tâche cron s'exécute, on peut lancer logrotate à la main, pour valider les règles rédigées.
La forme à utiliser est `logrotate -d /etc/logrotate.d/fichier_regles`.[^1]
L'option `-d` servant au débug, pas besoin d'utiliser `-v`, qui est ajouté implicitement dans ce cas.
La sortie produite est suffisamment explicite pour trouver d'éventuels problèmes.

### Cas particulier ###

Il y a aussi l'option `-f` pour forcer la rotation des fichiers, même s'il n'y en a pas besoin (par rapport aux règles de rotation).
Ça parait bête comme ça, mais j'ai beaucoup vu d'exemples sur internet avec cette option, et de questions liées vu les fichiers étaient modifiés de manière innatendue.

Aussi, pensez à vérifier les lignes de cron[^2] si jamais vous avez des rotations plus nombreuses que prévues.
Un script d'appel à cron possède peut-être cette option.

A noter qu'il peut être utile d'utiliser `-f` en cas de purge manuelle des logs, d'ajout de nouvelles règles, ou de suppression du fichier d'index.


[^1]: on peut aussi indiquer le fichier global, ce qui aura pour effet de tester TOUTES les règles présentes dans `/etc/logrotate.d/`.
[^2]: sur RHEL, le fichier `/etc/cron.daily/logrotate` est déposé par le rpm lors de l'installation de logrotate.

