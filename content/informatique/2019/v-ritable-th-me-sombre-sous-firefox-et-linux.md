+++
Categories = [""]
tags = ["firefox"]
title = "Véritable thème sombre sous Firefox et Linux"
date = "2019-08-18"
lastmod = "2019-09-19"
draft = false
+++

Depuis un certain temps, il y a un thème sombre pré-livré sous Firefox, mais il est incomplet.
Heureusement j'ai trouvé la manipulation grâce à un post twitter qui permet de rectifier le tir.

_MàJ_ : Ces manipulations sont maintenant obsolètes avec l'arrivée de la version 69.
Je laisse ici au cas où vous ne puissiez pas mettre à jour tout de suite.
<!--more-->

Tout d'abord, il faut avoir activé le thème sombre.

Ensuite il faut ouvrir un onglet `about:config`.

Chercher ensuite `allow-gtk-dark-theme` et le passer à `true`.

Enfin, dans le fichier `~/.config/gtk-3.0/settings.ini` il faut ajouter :

{{< highlight ini >}}
[Settings]
gtk-application-prefer-dark-theme = true
{{< / highlight >}}

Enregistrer le fichier, redémarrer Firefox, et tadaaaam !
Toute l'interface est désormais en thème sombre.

Petit effet kiss-cool rigolo : certains champs de formulaires sur des sites webs passent en mode sombre.

## En cas de champs illisibles

Parce que des fois on se retrouve avec des champs à textes foncés sur fond sombre, il faut ruser encore un peu.
Retourner dans le `about:config`, vérifier que `browser.display.use_system_colors` est bien à `false`, 
rajouter une clef `widget.content.gtk-theme-override` et ajouter un thème, genre `Adwaita:light`.
Redémarrer ensuite firefox.

## C'est casse-pieds ton truc !

Ouaip ! Mais à priori, à partir de la MàJ 69 (prévue pour le 3 septembre) il n'y en aura plus besoin car Mozilla fournira un thème sombre complet.
Je mettrai cet article à jour après avoir mis à jour et testé.

_MàJ_ : Et je confirme, plus besoin de faire tout ça. :)

## Sources

- [Le tweet en question de @romanzolotarev](https://twitter.com/romanzolotarev/status/1162070327742148609)
- [Le pouet de @Aznorth](https://framapiaf.org/@Aznorth/102643819020739029) qui m'a passé la solution en cas de champs illisibles.
- [Le bon lien du wiki ArchLinux](https://wiki.archlinux.org/index.php/Firefox/Tweaks#Unreadable_input_fields_with_dark_GTK_themes) sur cette astuce du `gtk-theme-override`[^1]

[^1]: Bon lien à la date du 18 aout 2019

