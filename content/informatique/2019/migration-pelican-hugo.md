+++
Categories = [""]
tags = ["blog","hugo", "pelican"]
title = "Migration de Pelican à Hugo"
slug = "Migration Pelican Hugo"
date = 2019-10-14T15:19:15+02:00
draft = false
+++

Ca y est !
Voici la nouvelle version du BWog-notes ! Youpi !
En plus du changement d'hébergement, la pile technique a changé.
Auparavant, le générateur était [Pelican](https://blog.getpelican.com/), maintenant c'est [Hugo](https://gohugo.io/).
Voici mes raisons, et mes observations liées.

<!--more-->

## Déjà, pourquoi un site statique ?


J'ai arrêté les sites persos dynamiques voilà un moment déjà.
Flemme de maintenir, flemme d'apprendre à utiliser des frameworks, flemme de gérer des BDD et des quotas d'espace disque, etc.
Et à l'époque, pas assez de sous / connaissances pour me lancer sur mon propre hébergement.
Je n'ai toujours été que sur des espaces mutualisés gratuit, sans réel nom de domaine (jusqu'à présent).

Dans ces conditions, le plus simple était donc, pour rester en ligne, d'avoir un site statique.

## Du coup, c'était comment avant ?

Déjà, c'était pas mieux.
J'avais bricolé un ensemble de scripts python tout gros, tout moche, pas optimisé, qui me gobait des pages html fabriquées à la main,
et qui assemblait le tout pour en faire un site complet, directement hébergeable, sans prise de tête autre que celle de bien écrire le html
 (manuellement, à chaque fois, sur chaque page).
Rustique, artisanal quoiqu'un peu dégueulasse, mais ça faisait le boulot.

M'enfin je me rendais bien compte que c'était pas terrible, ça me demandait aussi bien du temps pour la rédaction, que la mise en page.
Et oui, y'avait pas de templates ! Ni aucune fonction « moderne » pour itérer magiquement sur le contenu.
Sans compter les nombreux bugs qui freinaient le processus.
J'ai voulu tout réécrire de zéro, mais je me suis plutôt demandé s'il n'existait pas quelque chose de déjà fait, et surtout mieux.

Vint alors Pelican sur la table.
C'était en python, mon langage phare de l'époque[^1], il y avait des templates, et ça semblait plutôt facile à prendre en main.
Ni une, ni deux, j'adopte !
Et je lance le BWog-notes par la même occasion.

## Pourquoi changer de générateur ?

Et bien parce que c'était cool, mais pas top !
Même si les virtualenvs, ça fait grave le café, pour avoir un Pelican fonctionnel, il faut installer Pelican (`pip install pelican`),
et rapidement d'autres trucs.
Exemple, rien que pour rédiger en markdown, faut rajouter `pip install Markdown`.
Ensuite, pour les bouts de code `pip install pygments`, et lui faire générer un CSS à inclure sur le site.
Faut pas oublier d'activer le virtualenv avant ça d'ailleurs, et même pour chaque rédaction.

Et puis ça génère ouate mille fichiers de conf, d'automatisation de génération, etc.

Heureusement, il existe plein de plugins pour étendre les fonctionnalités, mais bon, c'est soit trop restreint (en termes de fonctionnalités),
soit overkill.
Et c'est pas pour jeter la pierre aux mainteneurs, je sais que ça prend du temps, mais ils sont souvent datés.
Par exemple pour les figures avec légende, et lien cliquable vers la grande version de l'image,
j'ai du [bidouiller le plugin figureAltCaption]({{< ref "pelican-python-markdown-images-dans-bateau.md" >}}).

Du coup, même si j'ai passé beaucoup de temps avec Pelican, en étant très content hein, j'ai fini par avoir des envies de changement.

## Le changement de générateur

J'ai migré récemment Pintes et Growls (lien dans la page « À propos »), et j'en ai profité pour tester Hugo.
Après tout, quitte à faire des tests, autant partir sur un projet neuf, afin de bien faire le tour de l'outil.

Pourquoi ce générateur ?

Au delà du bête « pourquoi pas », c'est surtout parce que dans mes flux RSS, il y a plusieurs personnes qui l'utilisent.
J'avais donc déjà sous le coude des exemples et des retours d'expérience.

Alors, l'installation par rapport à Pelican ?
Et bien installer le binaire (`pacman -Sy Hugo` pour moi) et ... C'est tout !

Et maintenant, pour moi, à l'utilisation, c'est bien plus simple.
Moins d'étapes (surtout dans mes scripts de synchro / publication), plus rapide, aucune dépendance, tout est inclus.
Et si on veut faire ajouter dans une page quelque qui n'est pas prévu à la base par le Markdown (même exemple, les figures cliquables) : 
zou ! Un `shortcode` (sorte de mini template à appeler dans les sources des articles via une balise qu'on définit soi même) et c'est parti !


## Le nouveau processus

Avant c'était :

- `source .venv/bin/activate`
- `./create_article.sh`[^2]
- rédaction
- `pelican build`
- regarder le résultat
- `cd content`
- `git add . && git commit -m "nouvel article" && git push origin master`
- `cd ..`
- `./publish.sh`
- vérifier en ligne le résultat
- `deactivate`

Maintenant c'est :

- `hugo new post/nouvel-article.md`
- rédaction
- `hugo serve`
- vérification
- `git add . && git commit -m "nouvel article" && git push origin master`
- `./publish.sh`
- vérifier en ligne le résultat

Plus besoin du script de création d'article, hugo, via un système de templates d'articles (appelés `archetypes`),
génère un fichier avec préambule et éventuellement début de contenu.
Le script de publication en ligne est également plus court.
Et encore, je pourrais même virer une étape en mettant un `git hook` pour publier dès que je pousse les sources.

## Conclusion

Alors sans mauvaise foi, ça va beaucoup plus vite, que ce soit dans mon processus de rédaction,
pour la génération des pages, ou la maintenance / évolution du site.

**MAIS** ! Parce qu'il y en a toujours un, et un gros sur ce point :
il faut noter que l'investissement technique (pour moi) est bien plus gros à l'entrée pour Hugo qu'avec Pelican.
Il a fallut recréer le thème et les templates[^3], des `layouts` dans le jargon Hugo, et c'est pas facile à prendre en main,
même en reprenant une structure déjà existante.
Il y a eu un gros temps d'adaptation aux fonctions des templates, à la logique d'organisation des fichiers,
ainsi qu'un certain temps à investir pour convertir / corriger les articles.

Maintenant que c'est fait, je ne regrette pas car je me sens plus à l'aise dans l'utilisation.

A vous de jouer, vous avez tout en main pour choisir ! Alors, n'attendez plus, lancez vous !

[^1]: Entre nous, ça l'est toujours dès que je dois prototyper un truc, ou scripter un truc trop haut niveau pour Bash.
[^2]: Un script perso qui pré-génère un squelette d'article.
[^3]: Pas envie de prendre un thème tout fait, j'avais déjà le mien après tout.

