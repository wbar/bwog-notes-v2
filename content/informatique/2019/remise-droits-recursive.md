+++
Categories = [""]
tags = ["shell", "chmod"]
title = "Remise à plat de droits récursivement"
slug = "Remise Droits Recursive"
date = 2019-11-01T23:55:15+01:00
draft = false
+++

Parfois quand on transfère une arborescence **perso** entre plusieurs disques/systèmes, on aimerait bien remettre à plat les droits.

La commande `chmod` possède la belle option `-R` déjà, mais là, c'est plutôt dans la notation des droits qu'on lui passe que je veux plonger.
Et surtout la notation symoblique (`[ugoa...][[-+=][perms...]...]`).

<!--more-->

Histoire de pas tout péter ou d'avoir une base correcte, en général, on applique au max du 644.
Et là c'est le drame : même en étant propriétaire des dossiers, on ne peut plus y accéder.

Avant de recourir à un `sudo find -type d -exec ...`, penchons nous sur le manuel de `chmod`, qui a tout prévu.

## La partie qui nous intéresse

Le manuel est assez court comparé à beaucoup de commandes.
La partie intéressante saute vite aux yeux, c'est celle qui décline les lettres utilisables dans la représentation littérale : `rwxXst`.
Ce `X` assez énigmatique au premier abord est en fait notre meilleur ami dans ce cas là.
Il va nous permettre de réparer les permissions sur les dossiers, sans mettre le bazar ni avoir à taper une commande compliquée.

Globalement ce que `X` fait, c'est de rajouter le droit d'exécution si l'objet est un dossier (ça tombe bien !) ou bien s'il y a déjà des droits en place.
Citation du man :

> execute/search only if the file is a directory or already has execute permission for some user

## Et donc

Après le drame, la victoire : `chmod -R u+X mon_dossier/`

## Avertissement

Evidemment, si on a pété une arbo système, il va falloir être beaucoup plus regardant sur les droits.
Mais ce n'est pas le sujet de ce billet.

