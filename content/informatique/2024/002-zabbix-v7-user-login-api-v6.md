+++
Tags = ["zabbix"]
title = "Zabbix v7 et le point d'API user.login comme en v6"
date = 2024-10-08T15:20:00+02:00

[comments]
  show = true
  host = "pleroma.chagratt.site"
  id = "AmnYkJ6Lv0bRuAsrGS"

+++

Dans la version 7 de Zabbix,
il y a de nombreux changements.
Certains cassent la compatibilité, et celui qui m'intéresse dans ce billet
concerne le point d'API `user.login`.

En effet, depuis la v6, le paramètre `user` est déprécié,
et a été supprimé en v7 au profit de `username` (déjà présent en v6 pour préparer).

Si vous utilisez déjà les jetons pour vos appels API,
ou ce nouveau paramètre,
vous n'aurez aucun impact.
Mais si vous trainez de vieux script, vous, ou des partenaires,
qui utilisent la méthode `user.login` et le paramètre `user`,
vous allez vous retrouver avec des scripts qui ne fonctionnent plus.

La solution est simple : corriger ces scripts.
Mais si ce n'est pas possible immédiatement ?

---

_Avertissement_ :

La solution proposée ici est sale à mes yeux,
mais permettra aux scripts utilisant l'ancienne méthode de continuer à fonctionner.
Le temps de les mettre à jour.

Cette solution est bien à considérer comme temporaire,
car elle sera écrasée à votre prochaine mise à jour.
Sauf si vous la remettez manuellement en place après coup.

Forcez les partenaires à mettre leurs scripts à jour !

---

Pour proposer de nouveau _temporairement_ l'accès au paramètre user,
ça va se passer dans le fichier `include/classes/api/services/CUser.php` à la ligne 2051.
Cherchez la fonction `login(array $data)` :

```php
/**
    * @param array $data
    *
    * @return string|array
    */

public function login(array $data) {
    $api_input_rules = ['type' => API_OBJECT, 'fields' => [
        'username' =>   ['type' => API_STRING_UTF8, 'flags' => API_REQUIRED, 'length' => 255],
        'password' =>   ['type' => API_STRING_UTF8, 'flags' => API_REQUIRED, 'length' => 255],
        'userData' =>   ['type' => API_FLAG]
    ]];

    if (!CApiInputValidator::validate($api_input_rules, $data, '/', $error)) {
        self::exception(ZBX_API_ERROR_PARAMETERS, $error);
    }

$db_users = self::findLoginUsersByUsername($data['username']);
// ... snip ...
```

En v6, c'était (même fichier, mais ligne 1512) :

```php
/**
    * @param array $data
    *
    * @return string|array
    */

public function login(array $data) {
    $api_input_rules = ['type' => API_OBJECT, 'fields' => [
        'username' =>   ['type' => API_STRING_UTF8, 'flags' => API_REQUIRED, 'length' => 255],
        'user' =>       ['type' => API_STRING_UTF8, 'flags' => API_DEPRECATED, 'length' => 255],
        'password' =>   ['type' => API_STRING_UTF8, 'flags' => API_REQUIRED, 'length' => 255],
        'userData' =>   ['type' => API_FLAG]
    ]];

    if (array_key_exists('user', $user)) {
        if (array_key_exists('username', $user)) {
            self::exception(ZBX_API_ERROR_PARAMETERS, _s('Parameter "%1$s" is deprecated.', 'user'));
        }
        $user['username'] = $user['user'];
    }

    if (!CApiInputValidator::validate($api_input_rules, $user, '/', $error)) {
        self::exception(ZBX_API_ERROR_PARAMETERS, $error);
    }

// ... snip ...
```

Comme documenté et attendu, la différence se joue sur la disparition du paramètre `user`.
Il faut donc, pour la v7, remettre dans la variable `$api_input_rules` la ligne le déclarant :

```php
'user' =>       ['type' => API_STRING_UTF8, 'flags' => API_DEPRECATED, 'length' => 255],
```

Et reprendre le mapping qui met la valeur de user dans username (mais il faudra changer `$user` en `$data`):

```php
if (array_key_exists('user', $user)) {
    if (array_key_exists('username', $user)) {
        self::exception(ZBX_API_ERROR_PARAMETERS, _s('Parameter "%1$s" is deprecated.', 'user'));
    }
    $user['username'] = $user['user'];
}
```

Et donc la fonction v7 avec la rustine :

```php
/**
    * @param array $data
    *
    * @return string|array
    */

public function login(array $data) {
    $api_input_rules = ['type' => API_OBJECT, 'fields' => [
        'user' =>       ['type' => API_STRING_UTF8, 'flags' => API_DEPRECATED, 'length' => 255], // FIX Compat v6
        'username' =>   ['type' => API_STRING_UTF8, 'flags' => API_REQUIRED, 'length' => 255],
        'password' =>   ['type' => API_STRING_UTF8, 'flags' => API_REQUIRED, 'length' => 255],
        'userData' =>   ['type' => API_FLAG]
    ]];

    // FIX Compat v6
    if (array_key_exists('user', $data)) {
        if (array_key_exists('username', $data)) {
            self::exception(ZBX_API_ERROR_PARAMETERS, _s('Parameter "%1$s" is deprecated.', 'user'));
        }
        $data['username'] = $data['user'];
    } // ---- END FIX

    if (!CApiInputValidator::validate($api_input_rules, $data, '/', $error)) {
        self::exception(ZBX_API_ERROR_PARAMETERS, $error);
    }

$db_users = self::findLoginUsersByUsername($data['username']);
// ... snip ...
```

Et voilà.
L'appli ou le script historique fonctionne à nouveau.

Comme dit plus haut, il s'agit d'une rustine qui ne doit pas être considérée comme définitive !

## Lien

- [La documentation de user.login en v6](https://www.zabbix.com/documentation/6.0/en/manual/api/reference/user/login)
- [La documentation de user.login en v7](https://www.zabbix.com/documentation/7.0/en/manual/api/reference/user/login)
- [Liste des changements API entre v6 et v7](https://www.zabbix.com/documentation/7.0/en/manual/api/changes)
- [Le code source v6 de user.login](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/ui/include/classes/api/services/CUser.php?at=refs%2Fheads%2Frelease%2F6.0#1512)
- [Le code source v7 de user.login](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/ui/include/classes/api/services/CUser.php?at=refs%2Fheads%2Frelease%2F7.0#2051)

