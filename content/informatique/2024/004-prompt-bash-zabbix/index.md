+++
Tags = ["zabbix","shell"]
title = "Un prompt Bash qui se voit bien pour Zabbix"
date = 2024-11-29T07:55:07+01:00

[comments]
  show = true
  host = "pleroma.chagratt.site"
  id = "AoWr8eOCrJdk0xjb8q"

+++

Pour compléter l'histoire [du prompt bash qui se voit bien]({{< ref "en-vrac#un-prompt-bash-qui-se-voit-bien" >}}),
voici quelques petites modifications du `PS1` que j'utilise depuis des années pour distinguer facilement les machines de l'infra Zabbix.

À noter que je n'ai pas encore rencontré de personnes avec un souci de perception des couleurs.
Si c'est votre cas, mon choix de couleurs peut ne pas vous convenir,
mais la base des réglages reste la même.

## Avant de commencer

Partons sur une base simple de `PS1` :

```shell
PS1="\[\033[01;32m\] \u \[\033[00m\]@\[\033[01;32m\] \h \[\033[01;34m\] \w \$\[\033[00m\] "
```
{{< image src="prompt-de-base.png" caption="Un prompt simple : utilisateur @ serveur, localisation dans l'arborescence. Deux couleurs." >}}

Les modifications seront apportées au début de la variable.
En fonction de l'environnement d'abord,
puis de l'utilité de la machine (serveur, proxy, BDD, front web).

## Différencier les environnements

Là, ça peut servir pour d'autres choses, mais je m'en sers aussi, alors voici.

Pour la production, un fond rouge sur le texte « PROD » en blanc :

```shell
\[\e[1;48;5;196m\] PROD \[\e[0m\]
```

{{< image src="base-prod.png" caption="Un prompt amélioré pour la PROD : étiquette PROD en blanc sur fond rouge au début, puis le classique utilisateur @ serveur, localisation dans l'arborescence." >}}

Et pour les autres environnements, par exemple « DEV », la pastille est bleue :

```shell
\[\e[1;48;5;31m\] DEV \[\e[0m\]
# on peut mettre un autre texte, évidemment. :)
\[\e[1;48;5;31m\] déclassé \[\e[0m\]
```

{{< image src="base-dev.png" caption="Un prompt amélioré pour le DEV : étiquette DEV en blanc sur fond bleu au début, puis le classique utilisateur @ serveur, localisation dans l'arborescence." >}}

## Différencier les rôles

Maintenant que l'on sait si l'on est connecté à la prod ou pas,
affichons rapidement sur quel type de machine nous sommes.

Je place cette nouvelle indication après l'étiquette d'environnement.

### Serveur

Du rouge en fond, comme pour l'environnement de production :

```shell
\[\e[1;48;5;196m\] SERVEUR \[\e[0m\]
```

{{< image src="serveur.png" caption="Un prompt amélioré pour le serveur : étiquette SERVEUR en blanc sur fond rouge placée après l'environnement. Exemple en DEV et PROD." >}}

### Base de données

Du vert en fond.
Ça n'est pas la meilleure idée que j'ai pu avoir,
mais ça fait le boulot jusqu'ici.

```shell
\[\e[1;48;5;2m\] BDD \[\e[0m\]
```

{{< image src="bdd.png" caption="Un prompt amélioré pour la BDD : étiquette BDD en blanc sur fond vert placée après l'environnement. Exemple en DEV et PROD." >}}

### Proxy

Cette fois, le texte est gris, et l'étiquette jaune.

```shell
\[\e[38;5;0m\]\[\e[48;5;11m\] PROXY \[\e[0m\]
```

{{< image src="proxy.png" caption="Un prompt amélioré pour le PROXY : étiquette PROXY en gris sur fond jaune placée après l'environnement. Exemple en DEV et PROD." >}}

### Front web

Pour finir, texte blanc sur fond bleu.

```shell
\[\e[1;48;5;31m\] FRONT \[\e[0m\]
```

{{< image src="front.png" caption="Un prompt amélioré pour le FRONT : étiquette FRONT en blanc sur fond bleu placée après l'environnement. Exemple en DEV et PROD." >}}

## Où faire ces modifications ?


D'abord dans votre `.bashrc`,
ensuite celui de _root_ (`/root/.bashrc`),
et enfin n'oubliez pas, pour les futurs collègues qui se connecteraient pour la première fois : `/etc/skel/.bashrc`.
Pour les autres, donnez-leur le prompt à appliquer (version raisonnable), ou allez éditer **prudemment** leur `.bashrc` (mais prévenez avant tout de même ...).

