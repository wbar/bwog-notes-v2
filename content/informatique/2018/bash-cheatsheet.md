+++
tags = ["shell"]
title = "Aide mémoire Bash"
date = "2018-10-11"
lastmod = "2024-11-22"
draft = false
show_toc = true
+++

Quelques éléments de Bash que j'utilise de temps à autre mais que parfois j'oublie. Cet article ne remplacera jamais le manuel de Bash.
<!--more-->

Bash possède beaucoup d'éléments intéressants, beaucoup sont méconnus.

La lecture de son manuel est fastidieuse mais très intéressante.
Cet article n'a pas vocation à le remplacer, juste à noter les éléments que je suis susceptible d'utiliser de temps à autre.

Pour mes notes sur les commandes générales, dirigez-vous plutôt vers cet article : 

[Aide mémoire Shell]({{< ref "/informatique/2015/shell.md" >}})

## Variables magiques

`$*` : Tous les arguments passés au script ou à la fonction.
_Attention_, c'est piégeux s'il y a des espaces dans les arguments.
Préférer la forme `"$@"`

`$?` : Code de retour de la dernière commande.

`$!` : PID de la commande / du script.

`$@` : Tous les arguments. Utiliser sous la forme `"$@"`.
Cela conservera correctement les args, même s'il y a un espace dedans.

`$0` : Nom de la fonction, ou du script.

`$#` : Nombre d'arguments de la fonction, du script.

`$$` : PID du shell courant.

`$_` : Expansion du dernier argument de la dernière commande.

Exemple :

{{< highlight bash >}}
mkdir pouet/foo/bar
cd $_

touch pouet/foo/bar/fichier.txt
vim $_
{{< / highlight >}}


## Expansion de variables

### Message d'erreur custom en cas de variable non définie

```bash
${toto:?Erreur pwet pwet var toto undefined}
```

## Interactif

{{< keys "Ctrl" "t" >}} pour échanger de position 2 caractères ;
celui sous le curseur avec le précédent.


{{< keys "Ctrl" "x" >}} puis {{< keys "Ctrl" "e" >}} :
ouvrir l'éditeur (utilise la variable `$EDITOR`), et à la sortie, exécuter la commande puis supprimer le fichier temporaire.

Changer la casse sur une commande :

{{< keys "Alt" "u" >}} pour passer un mot en majuscules.

{{< keys "Alt" "l" >}} pour passer un mot en minuscules.

{{< keys "Alt" "c" >}} pour passer la première lettre en majuscule et les autres en minuscules.

_Remarque_ : Si le curseur est placé au milieu d'un mot,
le changement de casse se fera à partir du curseur jusqu'à la fin du mot.


## Gestion des Jobs

{{< keys "Ctrl" "z" >}} :
stoppe un job et l'envoie en arrière plan

`jobs` :
Renvoie la liste des jobs en arrière plan avec le statut un ID et parfois un tag :


{{< highlight bash >}}
[1]+  Stopped                 man bash
[2]   Stopped                 python
[3]-  Stopped                 vim
{{< / highlight >}}

`fg [id]` :
ramène le job `[id]` au premier plan.
Si non spécifié, le dernier passé en arrière plan est ramené.
Il est possible d'écrire `%<id>` (qui est un alias de `fg <id>`).

`%+` et `%-` peuvent aussi être utilisés, et relanceront le job indiqué par ce caractère (dans la liste des jobs).

Remarque : ce tag change de job en fonction de l'ordre de pause / relance des jobs.

## Tester des flux réseau

`/dev/tcp/nom ou IP/port`
ou
`/dev/udp/nom ou IP/port`.
[Article dédié ici]({{< ref "tester-un-flux-r-seau-avec-bash.md" >}}).

## Un peu plus ?

J'ai aussi [des notes diverses à cet endroit](https://chagratt.site/partage/brouillon_notes_formation_linux.pdf).

Elles parlent notamment de substitution de variables, des descripteurs de fichiers, ...

Il me faudrait les mettre au propre sur de site un jour, mais elles peuvent néanmoins être utiles.

