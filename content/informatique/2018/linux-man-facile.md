+++
Categories = [""]
tags = ["documentation"]
title = "Rédiger une page de man facilement"
date = "2018-07-27"
draft = false
+++

La rédaction d'une page de `man` sous Linux est un peu complexe. Grâce à Perl et l'outil `pod2man`, le processus est grandement simplifié.
<!--more-->


Ayant dû rédiger de la documentation pour un gros script sous Linux, je me suis tourné vers les pages de `man`.
Cependant, j'ai trouvé que les macros à utiliser étaient complexes et que le fichier source était difficile à écrire, lire et éditer.

Après quelques recherches, j'ai trouvé un outil très intéressant lié à Perl : `pod2man`.

POD est une convention de documentation pour les scripts Perl, assez simple, mais permettant d'écrire des fichiers clairs.
Avec un rendu final propre.
Autre avantage, l'outillage POD fait partie de la distribution standard Perl.
Il est donc utilisable et accessible sur n'importe quelle machine Linux.


## Fichier source

Petit exemple, avec un fichier `fichier.pod` :

{{< highlight perl >}}
=encoding UTF8

=head1 NAME

superbin - an awesome binary tool

=head1 SYNOPSIS

superbin [B<--awesome>] [B<--foo>=F<bar>]

=head1 DESCRIPTION

B<superbin> compute elements and produce output on F<stdout>.

=head1 OPTIONS

=over 4

=item B<--avesome>

Change output, because you deserves it.

=item B<--foo>=F<bar>

Add an element to compute. It can be F<baz> or F<dummy>.

=back

=cut
{{< / highlight >}}
    
Ce fichier est tout aussi lisible que du Markdown, et pourra être facilement lu tel quel, édité, et ajouté dans le gestionnaire de version du projet.

Pour les sections à mettre dans une page de man, ou l'ensemble des éléments utilisables avec POD, voyez les documentations respectives.

## Compiler en page man

Une seule commande à connaitre : `pod2man`. Voici un exemple un peu étoffé :

{{< highlight shell >}}
# au cas où
podchecker "fichier.pod"
# convertion
# --center and --release are optional, but pod2man will put some Perl attributes if not used.
pod2man --center="doc header" --release="v x.y.z" "fichier.pod" > "fichier.1"
gzip "fichier.1"
{{< / highlight >}}


Personnellement, je rajoute le fichier man gzippé dans l'arbre Git des projets où j'en produis, en indiquant `*.gz binary` dans le `.gitattributes`.

Petite subtilité : `pod2man` rajoutera à la fin du manuel les éventuelles erreurs liées au format POD.

## Avantages et inconvénients

Le fichier man produit contient beaucoup de choses ! Bien plus que s'il avait été écrit à la main.
Mais je trouve que c'est un moindre mal par rapport au côté pénible d'avoir à utiliser soi-même l'ensemble des macros présentes dans `groff` (ou `troff`, etc.).

Au niveau avantages, il y en a plusieurs.

Notamment la lisibilité du fichier source, la facilité à écrire rapidement une page de man, et le fait que l'outillage soit disponible dès qu'une distribution Perl est présente sur une machine.
En plus, il devient facile de distribuer cette documentation et de la consulter.
Plus besoin de coder des appels aux outils tels que `more`, `less` ou un éditeur de texte en mode lecture seule.
Ni de s'embêter à faire des mises en forme textuelles à la main.
Il suffit de déposer le fichier man au bon endroit, et le tour est joué.

## Sources

[Article à propos de pod2man sur le blog Linux Tidbits](https://linuxtidbits.wordpress.com/2013/08/21/wtfm-write-the-fine-manual-with-pod2man-text-converter/)

[Article pod2man sur le blog de Richard VM Jones](https://rwmj.wordpress.com/2010/03/30/pod2man-a-great-way-to-write-unix-man-pages/)

[La doc Perl sur POD](http://perldoc.perl.org/perlpod.html)

[La page Wikipedia sur POD](https://en.wikipedia.org/wiki/Plain_Old_Documentation)

Les pages de man, et man-pages : `man 7 man`, `man 7 man-pages`

Le manuel de `pod2man` : `man pod2man`

