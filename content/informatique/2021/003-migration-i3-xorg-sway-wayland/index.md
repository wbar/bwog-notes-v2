+++
Tags = ["sway","wayland"]
title = "Migration de i3wm (Xorg) à Sway (Wayland)"
date = 2021-03-19T22:50:09+01:00
modified = 2022-12-17
draft = false
show_toc = true
+++

Cela fait un bon moment que je vois passer des articles sur sway, présenté comme le digne successeur de i3.
J'en suis un utilisateur depuis longtemps maintenant, et avant ça j'étais sur wmii.
C'était donc logique que je cherche à en savoir plus, voire le tester pour éventuellement l'adopter.

Seulement il n'est compatible qu'avec Wayland. Et ça, je ne connais pas bien. En plus quand j'ai commencé à voir le projet, c'était tout récent, et donc forcément ça m'a un peu refroidi.
Mais depuis, l'eau a coulé sous les ponts, et la stabilité semble être au rendez-vous.

Il est donc temps que je teste. Que dis-je, migre du vénérable i3 vers le fougueux sway.
Nombreuses (mais simples) bidouilles en perspective ! C'est parti !

<!--more-->

## Packages installés

- sway
- xorg-xwayland
- xorg-xeyes
- qt5-wayland
- glfw-wayland
- swaylock
- bemenu
- bemenu-wlroots
- waybar
- grim
- slurp
- mako
- brightnessctl
- gammastep

Ça fait un certain nombre vu comme ça, mais en vrai ils ne sont pas si lourds.
Xeyes n'est là que pour détecter les applis qui ne tournent pas nativement sous wayland.
Quand c'est le cas, vous pouvez voir les yeux bouger, sinon ils perdent la souris et ne bougent plus.
Vous noterez qu'il n'y a pas de terminal ici, mais c'est parce que j'utilise alacritty depuis un moment et il est compatible wayland.

Accessoirement, il manque aussi le paquet principal pour wayland.
Il était déjà sur mon système depuis un moment.

## Préconf de sway

Avant, un premier lancement, quelques petites manipulations, histoire de ne pas avoir de mauvaises surprises comment par exemple le gestionnaire qui ne démarre pas ou un clavier dans une autre disposition.
D'abord, copier le fichier de `/etc` dans mon home :

{{< highlight shell >}}
cd ~/.config
mkdir sway
cd sway
cp /etc/sway/config .
{{< / highlight >}}

Et la conf clavier FR avec variante :

{{< highlight txt >}}
input "1:1:AT_Translated_Set_2_keyboard" {
    xkb_layout "fr"
    xkb_variant "latin9"
    xkb_options "grp:win_space_toggle"
    xkb_model "pc104"
}
{{< / highlight >}}

### Manipuler les workspace avec un clavier FR

Là il va falloir ruser, car les chiffres ne sont pas accessibles immédiatement sur nos clavier.
Il va donc falloir modifier les affectations dans cette section.

Mon astuce, vu que je renomme aussi mes espaces, c'est de déclarer les touches via des variables :

{{< highlight txt >}}
    set $numkey1 ampersand
    set $numkey2 eacute
    set $numkey3 quotedbl
    set $numkey4 apostrophe
    set $numkey5 parenleft
    set $numkey6 minus
    set $numkey7 egrave
    set $numkey8 underscore
    set $numkey9 ccedilla
    set $numkey0 agrave
{{< / highlight >}}

[Si vous êtes en bépo, tournez-vous vers cette astuce](https://www.getrichordevtryin.com/sway.html#b%C3%A9po%C3%A8te-la-solution)

À partir de là vous pouvez faire vos réglage de cette manière (exemple en ne prenant que l'espace numéro 1):

{{< highlight txt >}}
    # déclaration des espaces
    set $workspace1 "1 "
    # naviguer entre les espaces
    bindsym $mod+$numkey1 workspace $workspace1
    # déplacer une fenetre vers un autre espace
    bindsym $mod+Shift+$numkey1 move container to workspace $workspace1
{{< / highlight >}}

### Forcer l'activation de xwayland

Le wiki Arch dit, à l'heure de l'écriture, que c'est activé par défaut, mais j'ai eu des crashs au lancement d'une app xwayland ... Hum hum.
Donc, pour contrer ça il faut rajouter `xwayland enable` dans la conf de sway.

## Pour le lancement auto de sway au login (tty)

Je n'utilise pas de gestionnaire de connexion, donc j'ai simplement rajouté ceci dans le `~/.bash_profile` :

{{< highlight shell >}}
if [ -z $DISPLAY ] && [ "$(tty)" == "/dev/tty1" ]; then
    export QT_QPA_PLATFORMTHEME="qt5ct"
    # forcer firefox à utilier wayland nativement
    export MOZ_ENABLE_WAYLAND=1
    exec sway
fi
{{< / highlight >}}

### Les autres lancements qui étaient dans le .xinitrc

Je n'avais pas grand-chose en plus de la variable d'environnement pour QT, juste le lancement de udiskie et xcompmgr.
Le premier est maintenant lancé par sway (via `exec --no-startup-id udiskie`), le second n'est plus appelé.
Ces choses sont directement gérées par le gestionnaire de fenêtres.

On a maintenant le minimum vital : le gestionnaire de fenêtres qui se lance, et les paquets d'utilitaires qui sont installés.
Il est temps de paramétrer tout ça aux petits oignons.

## Migrer la conf

Vu que sway est (quasiment) compatible avec i3, on peut maintenant se mettre à reprendre les bouts de configuration de l'ancien vers le nouveau.
À quelques détails près.

### Le menu pour lancer les programmes

J'adorais dmenu, alors en remplacement je me suis tourné vers bemenu qui propose la même simplicité.
Pour l'utiliser j'ai mis :

{{< highlight shell >}}
set $menu bemenu-run -p '»' --hb=#285577 --hf=#ffffff --tb=#285577 --tf=#ffffff| xargs swaymsg exec --
{{< / highlight >}}

Notez que là, il s'agit de la customisation de quelques couleurs et du prompt, mais vous pouvez simplement mettre `bemenu-run` sans aucun argument.

Je l'ai d'ailleurs passé en mode vertical :

{{< highlight shell >}}
set $menu bemenu-run -l 15 -p '>' --hb=#285577 --hf=#ffffff --tb=#285577 --tf=#ffffff| xargs swaymsg exec --
{{< / highlight >}}

Et si jamais vous utilisez `bemenu` dans un script, pour lui donner la même apparence sur l'ensemble du système, vous pouvez exporter une variable `BEMENU_OPTS` avec les options.

Exemple :

{{< highlight shell >}}
# dans mon fichier .bash_profile
export BEMENU_OPTS='--hb=#285577 --hf=#ffffff --tb=#285577 --tf=#ffffff'
{{< / highlight >}}

Du coup la ligne de conf Sway devient :

{{< highlight shell >}}
set $menu bemenu-run -l 15 -p '>' | xargs swaymsg exec --
{{< / highlight >}}

### Les règles de placement auto dans les workspaces

C'est là qu'il faut se rappeler que certaines applications fonctionnent sous xwayland.
Cette différence se fait sentir au niveau de la propriété à utiliser pour la règle d'assignation :

- le natif a une propriété `app_id`
- le xwayland se base toujours sur `class`

Pour vous aider, voici une petite commande à passer pour que sway vous dise qui utilise quoi : `swaymsg -t get_tree`.
Grep conseillé sur le nom de votre appli. :)

### Le fond d'écran

Cette partie est gérée nativement par sway. On peut donc retirer l'appel à feh.
Il y a une ligne toute prête dans la conf d'origine pour nous montrer comment faire.

### Les screenshots

Il faut simplement remplacer l'appel à scrot par grim.
Et pour avoir le choix de la zone à capturer : grim + slurp.

{{< highlight shell >}}
bindsym Print exec grim /home/chagratt/MesDocs/Images/captures/capture-$(date +%Y-%m-%d_%H-%m-%S).png
bindsym $mod+Print exec grim -g "$(slurp)" /home/chagratt/MesDocs/Images/captures/capture-$(date +%Y-%m-%d_%H-%m-%S).png
{{< / highlight >}}

### Les notifications

Activer mako au démarrage pour les notifications de bureau : `exec --no-startup-id mako`

### Le verrouillage de l'écran

Ça va se faire avec swaylock, et plus précisément dans mon cas via un script externe vu que je joue avec une capture d'écran et ImageMagick.
L'appel au script à mettre dans la conf est simple :

{{< highlight shell >}}
bindsym $mod+Shift+x exec /home/chagratt/MesDocs/Developpement/shell/snippets/swaylock.sh
{{< / highlight >}}

Et le script qui permet de jouer avec la capture qu'on floute, d'insérer une icone et un petit texte :

{{< highlight shell >}}
#!/bin/bash
# packages needed :
# - grim
# - imagemagick
# - swaylock
# thanks to : https://thomas-leister.de/en/sway-window-manager/#lockscreen

readonly ICON="${HOME}/MesDocs/Images/lock-icon.png"
readonly TMPBG=$(mktemp /tmp/bg.XXXXXXXXX.png)
readonly TMPTXT=$(mktemp /tmp/txt.XXXXXXXXX.png)
readonly SCREENWIDTH=1366

grim "${TMPBG}"
convert "${TMPBG}" -scale 25% -blur 0x2 -scale 400% -fill black -colorize 50% "${TMPBG}"
convert "${TMPBG}" "${ICON}" -gravity center -composite -matte "${TMPBG}"
convert -size ${SCREENWIDTH}x60 xc:black -font Liberation-Sans -pointsize 26 -fill white -gravity center -annotate +0+0 'Type password to unlock' ${TMPTXT};
convert ${TMPBG} ${TMPTXT} -gravity center -geometry +0+200 -composite ${TMPBG}
swaylock -s fill -i "${TMPBG}"

rm -f "${TMPBG}"
rm -f "${TMPTXT}"
{{< / highlight >}}

### Le jeu de couleurs en fonction de l'heure

Redshift ne fonctionnant pas sous wayland sans patch, je me suis dirigé vers un remplaçant natif.
[Voilà donc gammastep](https://gitlab.com/chinstrap/gammastep).
Un fork pour lequel on peut carrément copier coller la conf, en remplaçant toutefois la référence à `randr` par `wayland`.
Et en plus c'est packagé dans Arch. Nickel !

## Ce qui n'était pas dans la conf, enfin pas tout à fait

Allez, c'est presque terminé, on a fait le plus dur.
Même si on est sûr de l'ajustement depuis un moment.

### La luminosité

Pour remplacer xbacklight j'ai choisi brightnessctl.

Pour utiliser les touches de raccourci, j'ai fait comme tout le monde :

{{< highlight shell >}}
bindsym XF86MonBrightnessDown exec --no-startup-id brightnessctl set 5%-
bindsym XF86MonBrightnessUp exec --no-startup-id brightnessctl set 5%+
{{< / highlight >}}

### Le son

Tant qu'à faire, j'ai été creuser les touches reconnues, et je me suis aperçu que je pouvais utiliser aussi les raccourcis pour controler alsa :

{{< highlight shell >}}
bindsym XF86AudioRaiseVolume exec --no-startup-id amixer set Master 5%+
bindsym XF86AudioLowerVolume exec --no-startup-id amixer set Master 5%-
bindsym XF86AudioMute exec --no-startup-id amixer set Master toggle
{{< / highlight >}}

Et puis tiens, en creusant un peu, j'ai aussi trouvé les touches multimédia :

{{< highlight shell >}}
bindsym --locked XF86AudioPlay exec mpc toggle
bindsym --locked XF86AudioPause exec mpc toggle
bindsym --locked XF86AudioNext exec mpc next
bindsym --locked XF86AudioPrev exec mpc prev
bindsym --locked XF86AudioStop exec mpc stop
{{< / highlight >}}

_Note_ : J'ai du installer `mpc` en plus également.
Mais ça vaut le coup !
Ah, et le `--locked` est là pour rendre ces raccourcis utilisables même quand l'écran est verouillé.

### La barre de statut

Là c'est le plus long, surtout une fois qu'un a choisi, car la plupart des softs ont des possibilités de paramétrage très poussés.
Et comme c'est selon les gouts de chacun ...

Dans mon cas c'est donc waybar qui vient en remplaçant.
Après m'être rapproché le plus possible de ce que j'avais avec i3bar au départ,
j'ai _un peu_ dévié.
Ci dessous des exemples de ce que ça peut donner :

{{< image src="waybar.png" caption="Ma barre de statut Waybar au début de mon utilisation de Sway. Elle est en bas de mon écran. À gauche, les espaces de travail, à droite les différents modules (T° processeur, utilisation processeur, utilisation RAM, la batterie, l'espace disque disponible, le réseau sur lequel je suis connecté, MPD, le volume sonore, la date, l'heure, et enfin le « tray »). Le tout faussement groupé par thématique." >}}

{{< image src="waybar-2.png" caption="Ma barre de statut Waybar actuelle. Elle est en bas de mon écran. À gauche, un bouton avec le symbole « power », la date, l'heure. Au milieu les espaces de travail. À droite MPD, le volume sonore, un symbole d'un cœur traversé par un cardiogramme (au survol de la souris il donne CPU, RAM, température), le réseau sur lequel je suis connecté et enfin le « tray » (non visible ici). Aucun séparateur particulier." >}}

[Pour d'autres exemples, suivez ce lien (en anglais).](https://github.com/Alexays/Waybar/wiki/Examples)

Waybar est packagé pour Arch, d'où mon choix, et il permet d'avoir facilement les informations dont j'ai besoin.
Petit point négatif toutefois, la conf visuelle est faite via du CSS. Ce n'est pas un défaut en soi, mais on peut vite partir dans toutes les directions si on ne fait pas attention.
Le mieux est d'aller voir le dépôt git du projet qui contient moult exemples, histoire de partir sur une base proche de ce que l'on cherche à obtenir.

Le petit truc en plus à connaitre (qui existe peut être avec d'autres programmes, je n'ai pas cherché), ce sont les infobulles au survol des modules.
Là encore c'est paramétrable.

### Mettre à jour mon dépôt dotfiles

Alors ça, c'est totalement en fonction de vous, enfin, si vous avez un dossier qui centralise vos fichiers de conf ou pas.
Si la réponse est oui, voici donc les éléments à récupérer :

- sway
- waybar
- mako
- gammastep

### Le reste

Il faut tester que ça marche. Le son, le micro, la vidéo vos jeux ...
Et pour ça, aucun secret : faut utiliser. Jusqu'ici, ça va, tout fonctionne, c'est chouette !

## Et quand on est sûrs que TOUT fonctionne nickel

Simple : il faut ... Désinstaller.

- i3 (c'est le groupe de paquets qui contient toute la suite i3)
- feh
- dunst
- dmenu
- scrot
- redshift
- xcompmgr
- xrandr
- xbacklight
- xorg-xeyes
- xorg sauf xwayland (? pas certain là ... à vérifier)

Et virer les vieux fichiers de confs associés :

- .xinitrc (n'oubliez pas de vérifier que vous n'avez rien oublié dedans)
- les dotfiles liés si on en veut plus (mais je sens qu'ils vont rester un moment dans mon dépôt git. :D )

## La semi-conclusion

Terminé ! Ouf !
En vrai ça à l'air long, mais pas tant que ça de mon point de vue.
Le plus long, comme toujours, ce sont les ajustements cosmétiques (coucou waybar !).

Plus qu'à profiter.

Bon et bien pour le moment, mis à part la satisfaction d'avoir un pile technique plus moderne, rien à signaler ni à ajouter.
Ça tourne au moins aussi bien (heureusement), je ne change pas mes habitudes, je n'ai eu aucun problème notable.

J'ai même pu avoir un fonctionnement plus moderne sur quelques points.
Par exemple avec waybar il y a des petits info-bulles complémentaires (paramétrables) au survol de certaines informations, ce qui fait un bon gain de place.
Pour les captures d'écran avec le combo grim + slurp, la sélection de la zone se fait facilement. Avant j'avais juste tout l'écran.

Après j'ai la chance d'avoir une utilisation et un matériel simples (pas de carte graphique externe).

La section du dessus est juste là en prévision.
Mais pour le moment, au cas où, je n'ai pas encore viré Xorg ni i3, sait-on jamais.

## Quelques liens qui m'ont été utiles

- [Page de base que j'ai utilisée pour me lancer (En)](https://www.fosskers.ca/en/blog/wayland)
- [Des infos en plus pour fignoler le tout (En)](https://thomas-leister.de/en/sway-window-manager/)
- [Comment configurer les screenshots, mais pas que](https://www.getrichordevtryin.com/sway.html#des-captures-d%C3%A9cran-pour-mettre-sur-reddit)
- [Une liste pour trouver des équivalents d'apps compatibles avec wayland (En)](https://github.com/swaywm/sway/wiki/i3-Migration-Guide)

