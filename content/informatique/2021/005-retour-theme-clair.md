+++
Tags = ["blog"]
title = "Le retour du thème clair"
date = 2021-04-13T12:05:55+02:00
draft = false
+++

Enfin presque !
Je me suis décidé à jouer avec la media-query `prefers-color-scheme` pour vous proposer une version claire du site.
Elle sera donc proposée à la demande, en fonction de vos réglages de système, ou de navigateur.

<!--more-->

[Cette propriété CSS](https://developer.mozilla.org/en-US/docs/Web/CSS/@media/prefers-color-scheme)
plutôt bien supportée me permet d'éviter à maintenir 2 feuilles séparées (j'avais déjà des variables, j'ai bien fait de le faire finalement),
et d'ajouter du javascript.

Le gros piège dans cette démarche est de ne pas bêtement faire une inversion de couleurs, ça ne marchera pas en un claquement de doigts :
hé oui, il faut penser par exemple au contraste entre le texte et le fond, mais pas que.

J'ai tenté des choses pour garder un maximum d'accessibilité où j'ai pensé à regarder, mais il y aura probablement des loupés.
N'hésitez pas à me les remonter.


