+++
Tags = ["sigal", "logiciel libre"]
title = "Sigal : un générateur de  galerie statique"
date = 2021-09-03T09:53:41+02:00
modified = 2021-09-04
draft = false
+++

Pour partager des photos ou des images, il existe plein d'outils.
Dans mon cas je voulais quelque chose que je puisse déployer sur mon serveur sans avoir à installer de langage dynamique ni de BDD.
Je me suis donc naturellement tourné vers la génération statique.

Après pas mal de recherches où je me suis même intéressé à quelques thèmes Hugo conçus pour cela, j'ai fini par tomber sur un nom : Sigal.

A priori plus simple et plus léger que beaucoup d'autres outils j'ai décidé de lui donner sa chance.
Voyons cela ensemble. C'est parti !

<!--more-->

## Installation

Facile et rapide. Pour moi un simple `pacman -Sy sigal`.

Le programme est également installable via `pip` si vous n'avez pas de paquet pour votre distribution ou préférez ce moyen.

## Première utilisation

Placez-vous dans un dossier dédié de travail et lancez `sigal init`, qui  va créer un fichier de conf : `sigal.conf.py`.
Le manipuler est optionnel, mais il reste nécessaire pour que Sigal s'exécute.

Faisons une première galerie pour tester :

- Créer un dossier `images`, y déposer quelques photos, éventuellement éditer le fichier de conf à la ligne 16 pour donner le nom du dossier.

- Lancer la commande `sigal build images` ou juste `sigal build` si vous avez édité le fichier (en vrai, faites-le pour ce test sinon c'est chiant)[^1].

- Laisser le traitement se faire.

Un dossier `_build` est créé.
À la racine : les images, un fichier `index.html`,
un dossier avec les miniatures,
et un autre avec les éléments nécessaires pour que ça fonctionne une fois envoyé sur votre serveur (CSS, javascript, etc.).

Pour tester en local lancer `sigal serve`, puis utiliser dans son navigateur l'adresse indiquée dans le terminal.

Tadaaaam, une galerie fonctionnelle et sans fioritures !

{{< image src="sigal_exemple01.png" caption="Première génération de la galerie, avec toutes les images à la racine, sans aucune personnalisation." >}}

Bon c'est un peu brut, on va tenter d'améliorer ça.

Pour couper le serveur de test : le classique `ctrl-c`.

## Étoffer sa galerie

Retour au dossier `images`.
Créons à l'intérieur un fichier `index.md` avec comme contenu :

{{< highlight text >}}
Title: Ma superbre galerie perso !
Author: Moi, évidemment

Et une description *avec* de la syntaxe *Markdown*.
{{< / highlight >}}

Re un petit coup de `build` puis `serve`, et d'un coup, des informations un peu plus sympas.

{{< image src="sigal_exemple02.png" caption="Seconde génération de la galerie, en ayant personnalisé l'en-tête et le bas de la page." >}}

Certes on peut le faire dans le fichier de conf, mais on peut aussi utiliser cette technique pour chaque sous-dossier.
Tiens, testons.
Créons deux sous-dossiers, répartissons les images et mettons dans chacun un fichier `index.md` avec un texte spécifique.

Si vous souhaitez juste mettre une description, sans préciser de titre spécifique ni d'auteur, c'est possible également, 
mais dans ce cas, le nom du dossier sera utilisé à l'affichage.

Avant de refaire `build` puis `serve`, un petit `rm -rf _build` sera de rigueur.

Et là, on a quelque chose de propre et utilisable en à peine quelques minutes.

{{< image src="sigal_exemple03.png" caption="Troisième génération de la galerie, en ayant créé deux sous-dossiers à la racine." >}}

Si vous regardez le contenu du dossier `_build`, vous remarquerez que les dossiers contenant les miniatures sont isolés dans chaque album leur correspondant.
Le contenu est organisé proprement, c'est un très bon point.

## Avant d'uploader la galerie

Un dernier petit tour dans le fichier de conf ne fera pas de mal, notamment, assez bas, changer la langue par exemple.
Votre pseudo et le titre peuvent aussi être directement indiqués là, faisant devenir inutile le fichier `index.md` principal.
Ce fichier permet de surcharger ce qu'il y a dans la conf ou d'indiquer des choses en plus (genre juste un texte descriptif dans les `index.md`, c'est possible), ça peut être pratique.

Et c'est à peu près tout.
C'est assez chouette, ça fait le boulot, sans fioritures, et on a une galerie propre et fonctionnelle. Pile-poil ce que je cherchais !

Plus qu'à envoyer sur votre serveur et c'est terminé.
Pas de conf en plus si vous avez déjà un serveur web qui tourne.

## D'autres petites choses

On peut imbriquer des sous-dossiers comme on le souhaite, mais il n'est pas possible d'avoir dans un même album des images et des sous-dossiers au même niveau.
Ce n'est pas gênant, ça force à bien s'organiser, mais il faut le savoir.
Notez que si vous le faites, Sigal vous l'indiquera pendant la génération de la galerie.

Si jamais vous changez la structure ou l'arborescence de votre galerie, pensez à supprimer le dossier `_build` sinon vous aurez des doublons.
Si vous ne faites que rajouter des images, lancer juste `sigal build` suffira.

En autre détail, si vous ne voulez pas vous embêter à devoir modifier le thème pour l'affichage de l'auteur
(parce qu'afficher « © votre nom / pseudo » en pied de page ne vous intéresse pas), laissez le champ vide dans la conf.

Enfin il est possible pour chaque dossier de spécifier quelle sera l'image utilisée pour l'illustrer.
Pour cela, dans le fichier `index.md` de cet album il faut mettre `Thumbnail: Le_nom_du_fichier.extension`.

### Et si jamais rien de s'affiche une fois en ligne

Vérifiez la console de débug de votre navigateur, il y a de fortes chances que ce soit à cause de l'en-tête HTTP `Content-Security-Policy`.

Vous pourriez avoir besion d'ajouter `script-src 'self' 'unsafe-inline'` et `img-src 'self' data:`.

## Conclusion

Sigal est simple, rapide, efficace, permet même de faire plusieurs galeries indépendantes au besoin.
L'essayer c'est l'adopter ?
En tout cas, pour moi qui voulais de la génération statique sans que ce soit compliqué à utiliser, c'est un grand oui.

## Liens

- [La documentation de Sigal](https://sigal.saimon.org/en/latest/)
- [Un article d'un utilisateur et contributeur (en anglais) qui présente Sigal](https://lwn.net/Articles/748629/)

[^1]: Dans la configuration par défaut, le dossier porte un autre nom, mais pour montrer quelques possibilités, je l'ai changé.
