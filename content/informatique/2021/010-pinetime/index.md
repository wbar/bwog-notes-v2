+++
Tags = ["matériel", "PineTime"]
title = "J'ai commandé (et reçu) une montre connectée PineTime"
date = 2021-10-13T16:07:57+02:00
modified= 2022-02-17
show_toc = true
+++

Les montres connectées, vaste sujet.
Utiles ? Inutiles ? Chères !
Fortement liées à un constructeur qui oblige à utiliser une application privative qui vous espionne ...
À peine quelques questions ou idées, et déjà on peut avoir beaucoup à dire.
Pourtant nous n'avons qu'effleuré la surface avec ces quelques mots.

Au milieu de tout ce bazar, il y a Pine64.
Connus pour leurs matériels réellement libres, les voici proposant une montre dans la même veine, à un tarif plus qu'abordable.

Du coup, j'ai craqué.
Voici mes premières observations, du déballage aux premiers jours d'utilisation.

<!--more-->

## Vous prendrez bien un peu de contexte ?

Malgré tous les problèmes qu'on peut trouver à propos de ce genre d'objet, j'étais quand même curieux.
Mais pour m'en procurer, j'avais des critères très précis :

- Indépendant d'une grosse boite qui fait du matériel fermé. C'est important, car si la boite coule ou abandonne le produit, je me retrouve avec un objet inutilisable.
- Pas cher, ou tout du moins à un tarif honnête.
- Suivi du nombre de pas.
- Application _libre_ pour paramétrer et suivre les données. Hors de question de ne pas être maître de mes données ou d'installer un mouchard sur le téléphone.
- (bonus) Suivi du rythme cardiaque.
- (bonus 2) Contrôler la musique sur mon portable sans avoir à le manipuler. Oui, celui-ci est très précis.

Bingo : ça existe chez Pine64, et à un prix raisonnable (35€ en comptant les frais de port, wouahou !).
Alors, pourquoi pas ?

À noter d'ailleurs, j'ai prit la version scellée.
Ça change quoi ?
Par rapport au kit de développeur proposé, il s'agit de la montre seule avec le couvercle arrière collé et sans les câbles pour la raccorder à son PC.
Comme je sais que je ne développerai pas sur la montre, je n'ai pas jugé utile de prendre ce kit.
Mais si vous le faites, il est largement possible de sceller soi-même la montre après coup.
À vous de voir en fonction de ce que vous voulez faire avec.

## Commande et réception

J'ai passé commande le 21 septembre 2021.
Il y avait du stock à ce moment-là.
Le petit détail sympa : le paragraphe sur la version scellée par rapport à la version du kit de développement pour expliquer la différence.

6 jours plus tard, je reçois un mail avec une notification d'expédition et un numéro de suivi.
Le délai peut sembler long par rapport aux autres achats que l'on peut faire en ligne, mais ça ne me choque pas.

Par contre, l'envoi réel de la montre a été beaucoup plus long.
On dirait que Pine64 attendre d'avoir un certain nombre de commandes à expédier en même temps.

La patience a donc été de mise.
Et finalement, réception le 6 octobre.

## Déballage

Le paquet est arrivé dans une enveloppe dont l'intérieur est tapissé de papier bulle.
Dedans, une petite boite en carton marron sans fioritures.
Il comporte juste une étiquette indiquant ce qui m'intéresse : à l'intérieur se trouve ma « PineTime SmartWatch (sealed) »

{{< image src="IMG_20211006_140316.jpg" caption="Cette petite boite en carton ne paye pas de mine." >}}

En l'ouvrant, on découvre une mini boite nous présentant la montre sous un couvercle transparent.

{{< image src="IMG_20211006_140337.jpg" caption="Boite-ception. Mais la montre est bien là !" >}}

Sous la montre, un emplacement contenant le chargeur ainsi qu'un petit mot de prise en main et d'autres informations complémentaires
(à quoi connecter sa montre, où trouver le dernier firmware, etc.).

{{< image src="IMG_20211006_140611.jpg" caption="Et voilà ! C'est tout ? Oui. Pas besoin de plus." >}}

Détail rigolo mais qui a tout de même son importance : le chargeur est aimanté pour garder la montre correctement installée dessus.

Pensez à retirer le petit film de protection qui est sur le capteur de pouls. Il n'est pas aussi bien visible que celui pour protéger l'écran, mais il est là.

Globalement la montre est de bonne facture pour ce prix.

## Premier allumage

Une pression pendant quelques secondes sur le bouton, et hop !
L'écran s'allume, le cône blanc se rempli en vert quand le démarrage progresse, puis on se retrouve avec l'écran principal : l'horloge.

Horloge qui pour le moment n'indique pas l'heure ni la date, allons bon.

Pour y remédier c'est simple : il suffit d'installer Gagdetbgride sur le téléphone puis d'appairer la montre.
La date et l'heure sont synchronisées automatiquement quand les deux appareils sont connectés ensemble.

{{< image src="IMG_20211006_181625.jpg" caption="Une montre qui donne l'heure ! C'est quand même vachement bien !" >}}

C'est juste dommage que Gagdetbgride demande d'avoir le GPS activé pour la découverte initiale, mais on peut la désactiver ensuite.

Intéressons-nous maintenant à la version de la montre.
Au déballage elle est en 1.2.0, mais le projet propose la 1.6.0.

Allons bon, ne restons pas aussi loin dans le passé.


## Mise à jour du firmware de la montre

Tout d'abord il faut télécharger [le dernier firmware sur le github d'InfiniTime](https://github.com/InfiniTimeOrg/InfiniTime/releases/) (prendre le .zip) sur son ordiphone.
Ensuite il faut naviguer avec votre explorateur de fichiers jusqu'à ce dernier.

_Subtilité_ : avant de faire « ouvrir avec », renommez le fichier du micrologiciel et ajoutez « .fw » à la fin, sinon Gadgetbridge vous enverra bouler ([source de cette info](https://wiki.pine64.org/wiki/Upgrade_PineTime_to_InfiniTime_1.0.0#Gadgetbridge), mais elle marche aussi si vous avez déjà une version plus récente.).

Une fois le « ouvrir avec » fait, choisissez Gadgetbridge qui lancera la mise à jour.
La PineTime devra aussi montrer un écran de mise à jour.

Quand la mise à jour est terminée, la montre redémarre toute seule.
Allez à ce moment là dans les paramètres pour vérifier la version indiquée.
Le bouton de validation de firmware est à activer rapidement, sinon au prochain redémarrage et retournera à l'ancienne version.

En vrai, ça fait beaucoup de petites étapes mais ça prend moins de cinq minutes.

### Si vous utilisez Simple File Manager

Avec ce [gestionnaire de fichiers](https://f-droid.org/en/packages/com.simplemobiletools.filemanager.pro/),
il ne faut pas rester appuyer et passer par le menu d'actions supplémentaires.
En faisant ça j'ai eu une popup Android d'erreur me disant qu'aucune application ne pouvait gérer ce genre de fichier, sans aucune proposition.

Au lieu de ça, un simple appui dessus va ouvrir l'invite permettant de sélectionner Gadgetbridge.

## Configuration de la montre

Je n'ai pas eu à faire grand-chose, et je trouve ça chouette.
L'affichage de l'heure était déjà au format 24 heures (sûrement grâce à la synchronisation avec Gadgetbridge), la luminosité à 2 sur 3.

J'ai été mettre le réveil de l'écran sur « single tap ».
Mais une pression sur le bouton ça marche aussi quoi qu'il arrive.

J'ai placé le temps d'exctinction de l'écran sur 5 secondes.

À noter, le capteur cardiaque est désactivé par défaut.

## Ressenti et observations après une semaine

Bon déjà, j'en suis content.
J'avais des appréhensions, mais plus maintenant.

Le bracelet, bien qu'en caoutchouc est agréable, la montre n'est pas lourde et par rapport à mon petit poignet son gabarit n'est pas trop gros.
L'écran est lisible, même en plein jour alors que la luminosité est paramétrée sur « moyen ».

L'historique des pas et du rythme cardiaque ne sont pas encore implémentés, mais c'est en bonne voie dans le projet InfiniTime.
D'ailleurs, on peut mettre un objectif de pas en allant dans les paramètres.
Pas besoin de passer par le téléphone pour ça, et c'est un très bon point à mon sens.
L'application elle ne fait que le décompte, avec une petite jauge circulaire.
D'ailleurs, ce compteur se remet à zéro [autormatiquement à minuit](https://github.com/InfiniTimeOrg/InfiniTime/issues/694#issuecomment-931590775). Pour le moment :)
Il me semble plutôt précis, et c'est une bonne surprise. Même si c'est compliqué à vérifier au pas près.
Après, je ne porte pas la montre de manière très ajustée sur mon poignet, ça joue peut-être.

À propos du capteur cardiaque, il faut savoir qu'il s'éteint quand la montre est en veille.
Du coup c'est pas dingue car à moins d'une évolution future qui va changer (paramétrer ?) ça, je vois mal comment on pourra avoir des stats là-dessus.
Même s'il demande un peu de temps pour calculer correctement, il ne renvoie pas des valeurs trop aberrantes, mais reste un peu capricieux.
Que la montre soit bien ajustée au poignet ou non, si vous avez le cuir trop épais, la montre ne lira rien et il vous faudra alors la porter sous le poignet pour avoir un retour de valeur.

Le fait qu'on puisse couper la synchro entre son téléphone et la montre est aussi un gros plus pour moi.
Une fois l'heure et la date synchronisée, on peut couper le lien, la montre continue de fonctionner sans aucun soucis.
Et heureusement me direz-vous !
Perso je ne le rallume que pour contrôler la musique sur mon tél. Ou faire les éventuelles mises à jour.

Pour la musique, une fois lancée sur le téléphone puis l'app de la montre en cours c'est pas mal.
Je peux utiliser les contrôles de chanson suivante ou précédente, pile-poil ce que je voulais.
Vous pouvez même laisser la montre s'éteindre sur cet écran afin qu'elle y soit toujours au réveil pour gagner du temps.

Et enfin, après une semaine, la batterie est encore au-dessus de 45 %.
C'est pas mal du tout par rapport aux 4 jours annoncés par PineTime.
Après, il faut garder en tête que j'ai une petite utilisation.
Si vous l'utilisez intensivement il est possible qu'elle dure effectivement seulement 4 jours.

### Les fonctionnalités que je n'utilise pas

Mais qui peuvent être utiles à d'autres.
Je vous liste donc quelques éléments:

- Il y a 2 autres écrans disponibles pour présenter l'heure et les informations. Moi j'ai laissé celui de base, il me plait bien.
- Les notifications sur la montre : car je ne laisse pas tout le temps mon téléphone à portée ni avec le bluetooth d'activé
- Les informations de guidage GPS sur la montre : je n'ai pas encore testé, mais il y a visiblement cette possibilité de prévue.
- Le réveil : j'utilise cette fonctionnalité sur mon téléphone car je ne dors jamais avec un truc au poignet

### Les petits trucs pas dingues

Parce que oui, tout n'est pas parfait, mais bon entre nous, c'est pas si pire.
Il ne faut pas oublier ce qu'est la PineTime par rapport aux autres produits du même type.

- Le compte à rebours : une seule secousse sur la montre une fois le décompte arrivé à zéro. Je ne la sens pas toujours ([et je ne suis pas le seul](https://github.com/InfiniTimeOrg/InfiniTime/issues/685)). Mais en vrai, pour ce genre de cas j'utilise celui du téléphone ou un autre dans la cuisine qui eux sonnent fort et longtemps.
- La reconnexion au bluetooth : je ne sais pas si c'est Gadgetbridge qui a du mal, si c'est mon téléphone ou la montre. Toujours est-il que c'est un peu galère, mais ça se fait, en insistant un peu. Quoique des fois, il faut redémarrer la montre, ou alors refaire l'appairage depuis Gadgetbridge, et puis des fois, ça passe tout seul. C'est un problème connu. Il y a déjà eu pas mal d'améliorations, notamment avec les v1.5 et surtout 1.6.
- L'affichage du temps et du titre de la chanson actuellement en cours de lecture sur le téléphone. Des fois ça se met à jour en changeant de chanson, des fois pas. Bon. Tant pis c'est pas spécialement grave, et de toute façon l'écran étant petit, c'est pas spécialement pratique de lire le titre quand il est long, surtout avec un délai de 5 secondes avant extinction de l'écran.
- Le câble du chargeur n'a pas l'air super solide aux jointures avec la prise ni le socle. Il faudra être soigneux avec. Là par contre ça peut être embêtant.
- Le capteur de rythme cardiaque : il se coupe quand la montre passe en veille. Du coup, au réveil de la montre, il y a besoin d'attendre avant d'avoir une valeur, et en dehors de l'application dédiée, la montre repasse vite en veille. C'est sûrement voulu pour des raisons d'économie de batterie de la montre, mais c'est dommage.
- Le format de la date : y'en a que ça peut embêter. Mais perso je m'en fous.

### Le machin inexplicable

C'est probablement de la faute de mon téléphone, mais quand un autre appareil en bluetooth spamme pour demander l'appairage,
les contrôles musicaux depuis la montre sont soit lents à réagir, soit inopérants.

Étrange, car ça marche super bien sinon.

L'astuce que j'ai trouvée : avant d'être là où il va y avoir cette perturbation, il faut s'assurer que la montre est bien connectée, puis lancer au moins une fois la musique.
Enfin, éteindre l'écran du téléphone, puis retester.
Normalement là, plus de problèmes.

_Ajout du 04/11/2021_ : en fait si, toujours des problèmes, mais [j'ai une piste intéressante]({{< ref "011-pintetime-gadgetbridge-musique.md" >}}).

## Conclusion de ces premiers temps

Et bien je suis très content.
Pour mon petit usage, franchement, c'est cool, surtout à ce prix !

Globalement quand on a une utilisation simple, les petits défauts actuels sont mineurs je pense.
À voir à quelle vitesse ça va s'améliorer et surtout quelles seront les nouveautés dans les prochaines mises à jour !

Je referai un point plus tard, notamment la longévité de la batterie
(pour rappel : chargée à bloc le 6 après midi, soit il y a une semaine), et puis aussi en cas de mise à jour du firmware s'il apporte des choses intéressantes.

## Liens

- [le site de Pine64 (en anglais)](https://www.pine64.org/)
- [le github d'InfiniTime (l'OS de la montre)](https://github.com/InfiniTimeOrg/InfiniTime)
- [la page F-Droid de Gadgetbridge](https://f-droid.org/fr/packages/nodomain.freeyourgadget.gadgetbridge/)
- [Et sinon Pine64 est sur mastodon](https://fosstodon.org/@PINE64)

### Encore plus ?

- Une revue en vidéo par WeKeys : [Avis PineTime après 2 mois d'utilisation](https://peertube.we-keys.fr/w/84669a33-97d4-451e-950a-2aa90c6cb3c2)
- [Pinetime : j'ai craqué pour une montre connectée](https://www.dadall.info/article761/pinetime-jai-craque-pour-une-montre-connectee) sur [le blog de dada](https://www.dadall.info/)

Cherchez « PineTime review » dans votre moteur de recherche favori, vous aurez largement de quoi faire.

