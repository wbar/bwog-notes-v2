+++
Tags = ["gemini","hugo"]
title = "Amusons-nous avec Gemini et Hugo"
date = 2021-02-23T22:17:01+01:00
modified = "2022-05-01"
draft = false
+++

Gemini est un protocole réseau récent un peu en mode « low tech » qui se concentre sur le texte. Ni plus, ni moins.

En pleine evervesence mais intéressant, ou au moins rigolo à essayer, je m'y met aussi, y'a pas de raisons.

<!--more-->

Un nouveau protocole donc, qui va nécessiter un client spécifique pour y aller, et un serveur pour distribuer le contenu.
J'ai jeté mon dévolu sur amfora comme client (utilisable en console) et satellite pour le serveur.
Et pour rédiger le contenu ? Mon fidèle éditeur de texte de toujours, vim, et Hugo pour la génération des pages.

## Et donc ?

Concrètement, les documents servis sous gemini sont rédigés et présentés sous une forme épurée de markdown. Que du texte donc, avec une mise en forme basique.
C'est plus qu'il n'en faut pour partager des documents uniquement remplis de phrases et de quelques liens.
D'ailleurs quand on utilise un générateur de site statiques qui propose déjà ce format, c'est finalement assez simple : copier, coller et basta !

Avec quelques subtilités néanmoins : pour les liens par exemple, la forme change. On passe de :

{{< highlight txt >}}
[texte du lien](adresse)
{{< / highlight >}}

à

{{< highlight txt >}}
=> adresse Texte du lien
{{< / highlight >}}

Il vaut mieux également oublier les images intégrées en plein milieu du document, mais est-ce si grave ?

En vrai, on en revient à un simple partage d'information, de rédactions, et la manière dont a été pensé gemini évite les surenchères de CSS, javascript, et des travers liés à l'addition outrancière des deux.

## Les bidouilles côté Hugo

Elles sont assez courtes en vérité : quelques lignes dans la conf, un fichier `index.gmi` et un autre `_default/single.gmi` dans le layout.

Quoi, c'est tout ? Ben, ça peut oui, en tout cas pour une première mouture.
Se concentrer juste sur une page d'accueil et une seconde de présentation des articles, c'est largement suffisant. C'est même rafraichissant.

[Un exemple que j'ai suivi se trouve ici.](https://doc.huc.fr.eu.org/fr/web/hugo/hugo-gemini-gopher/)

Ça fonctionne bien ! À quelques exceptions près : j'ai plusieurs shortcodes qui vont un peu pourrir le contenu qui sort en `.RawContent`.
J'ai vaguement réussi à mitiger ça de la manière suivante :

```go-html-template
{{ $content := .RawContent -}}
{{ $content := $content | replaceRE "`(.+?)`" "$1" -}}
{{ $content := $content | replaceRE "`" "```" -}}
{{ $content := $content | replaceRE "{{</*(.+?)highlight(.+?)*/>}}" "```" -}}
{{ $content := $content | replaceRE "{{</*(.+?)fold(.+?)*/>}}" "$2" -}}
{{ $content := $content | replaceRE "\\*(.+?)\\*" "$1" -}}
{{ $content := $content | replaceRE " \\[(.+?)\\]\\((.+?)\\)" "\n\n=> $2 $1\n\n" -}}
{{ $content := $content | replaceRE "\\[(.+?)\\]\\((.+?)\\)" "=> $2 $1" -}}
{{ $content := $content | replaceRE "<!--more-->" "" -}}
{{ $content }}
```

Si jamais vous avez des liens internes, avant le `{{ $content }}` final, il vous faudra ajouter :


```go-html-template
{{ $matches := findRE "{{</* ref(.+?)*/>}}" $content }}
{{ range $matches -}}
    {{ $match := . | replaceRE "{{</* ref \"(.+?)\".?*/>}}" "$1" -}}
    {{ $url := ref $.Page $match }}
    {{ $url := replace $url "/gemini" "" 1 }}
    {{ $content = $content | replaceRE . $url -}}
{{ end -}}
```

Pas terrible, mais ça limite la casse. Enfin presque. Si vous êtes sur le site web, vous voyez parfaitement le code ci dessus.

Si vous êtes sur la capsule gemini, les lignes 4 et 5 sont cassées. Les regex sont sensées contenir (sur une seule ligne)

```go-html-template
"{{
<
(.+?)highlight(.+?)
>
}}" "```"
```
et

```go-html-template
"{{
<
(.+?)fold(.+?)
>
}}" "$2"
```

## Les bidouilles côté serveur


[Satellite donc pour le soft côté serveur.](https://sr.ht/~gsthnz/satellite/)

Comme beaucoup pour ce nouveau protocole, c'est simple, efficace, sans fioritures.
J'ai particulièrement aimé le fait qu'il gère tout seul les certificats qu'il faut normalement générer (et renouveler) soi même.

Pour le faire tourner en tant que service avec systemd, je me suis servi de [l'astuce glanée sur la capsule gemini de alterzorg](gemini://gmi.alterzorg.fr/informatique/installer_agate.gmi).

Il a juste fallu compiler le serveur, mais ça prend moins d'une minute environ. Donc en quelques frappes au clavier, c'est plié.
Il faut juste éviter d'oublier d'ouvrir le port 1956, qui est celui utilisé par gemini sur le firewall.

### Mise à jour du 05/01/2022

Avec un peu de retard, je corrige cette partie de l'article.

[J'utilise maintenant Stargazer comme serveur Gemini]({{< ref "002-stargazer-server-gemini-alpine.md" >}})

## Tadaaaaam

Alors, tout n'est pas rose au pays des bisounours. Certes il est facile de monter sa capsule et et rédigrer du contenu, mais ...

Mais quand on maintient un site classique **et** une capsule, forcément la source markdown, dans mon cas, mais je pense ne pas être le seul, il y a des ratés.

Déjà, je n'intercepte pas tous les contenus qui sont des bouts de shortcodes hugo non transformés (utilisation de RawContent oblige), et visiblement avec certains clients gemini, ma capsule refuse de s'afficher. Diantre.

La véritable difficulté, avec une source unique conçue avant-tout pour le ouaibe, quand bien même elle reste la plus légère possible amène forcément à ce genre de choses.

Mais finalement, l'expérience est intéressante :

- Un nouveau protocole plus léger ;
- des clients petits, réactifs ;
- un contenu. Point. Toute façon ça n'a pas été pensé autrement, et ça, c'est cool ;
- des softs serveurs forcément économes.

Pour le moment, je vais continuer à proposer les deux, en mode blog d'abord, et je tenterais d'ajuster au fur et à mesure. Je proposerai peut être même une capsule pour Pintes et Growls, qui sait ? Que du texte, c'est vachement bien en fait.

Cela dit, la démarche va avoir ses limites : avec une source pensée ouaibe qui passe à une moulinette pour tenter de la placer en gemini, il va soit falloir faire des concessions (mise en forme, images éventuelles), soit faire bien plus d'efforts.
Je pense à des détails comme tester le rendu des articles à venir. Avec hugo c'est TRÈS facile. Avec gemini ... il faut que je fasse une génération du site, et donne à gober au client le chemin **complet** vers le fichier gmi, en préfixant par `file://`

Enfin, repenser certaines pages comme les nuages de tags, ou même le blogroll. Je ne sais pas encore comment faire ça sans ruiner le tout. Mais ... on verra. Et si vous avez une idée, n'hésitez pas à m'en faire part.


## Liens en plus

- [La page du protocole qui regorge de liens (en anglais)](https://gemini.circumlunar.space/)
- [Managing this site and my gemini capsule with Hugo sur bacardi55.io (en anglais)](https://bacardi55.io/2021/02/13/managing-this-site-and-my-gemini-capsule-with-hugo/)

