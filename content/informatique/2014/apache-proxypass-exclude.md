+++
Categories = [""]
tags = ["apache-httpd"]
title = "Reverse proxy Apache et alias qui se chevauchent : il faut une exclusion"
date = "2014-08-05"
draft = false
+++

En utilisant le drapeau `!` en second argument de la directive `ProxyPass` il est possible d'exclure un chemin.
<!--more-->

Petite subtilité Apache du jour.

Soit un _virtualhost_ classique avec ce contenu :

{{< highlight apacheconf >}}
Alias /data/db/products/ /opt/app/public/pictures/

ProxyPass /uneRoute http://localhost:1234/uneRoute
ProxyPassReverse /uneRoute http://localhost:1234/uneRoute

ProxyPass /unService http://localhost:5678/uneService
ProxyPassReverse /unService http://localhost:5678/uneService

ProxyPass / http://localhost:8000/frontEnd
ProxyPassReverse / http://localhost:8000/frontEnd
{{< / highlight >}}

Dans ce cas précis, les images qui seront demandées à partir du chemin `/data/db/products/` ne seront pas trouvées 
car elles vont être traitées dans la dernière règle de mandataire inverse.

Pour éviter cela, il faut l'exclure en ajoutant (avant la règle fautive) une autre règle de mandataire, avec le drapeau d'exclusion `!` : 

{{< highlight apacheconf >}}
ProxyPass /data/db/products !
{{< / highlight >}}

