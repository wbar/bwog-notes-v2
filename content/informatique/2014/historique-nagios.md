+++
Categories = [""]
tags = ["nagios"]
title = "Supprimer l'historique Nagios"
date = "2014-04-17"
draft = false
+++


Voici un moyen simple de remettre à zéro l'historique des états.
<!--more-->

Sous Debian il faut aller dans `/var/lib/nagios3/` et supprimer le fichier `retention.dat`.
*Attention* à ne pas oublier de couper le service avant.

Le chemin peut être différent sur les autres distributions, mais le nom de fichier ne devrait pas changer. 
En cas de doute, consulter le fichier de configuration de Nagios qui indique l'emplacement.
