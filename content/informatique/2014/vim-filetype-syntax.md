+++
Categories = [""]
tags = ["vim"]
title = "Coloration syntaxique de fichiers non reconnus par VIM"
date = "2014-08-03"
draft = false
+++

Si VIM ne reconnait pas correctement un fichier, il est possible de forcer le type de langage à utiliser avec `autocmd`.
<!--more-->

Il peut arriver que VIM reconnaisse mal (voire pas du tout) certains types de fichiers.
J'avais eu le soucis il y a longtemps avec des fichiers ProC (extension `.pc`) que je voulais traiter comme des sources de C.
Plus récemment je l'avais avec des fichiers Markdown terminés en `.md`, mais pas en `.mkdn`.

Pour le résoudre de manière ponctuelle, il est possible d'entrer la commande `:set syntax=LANGAGE`,
mais il faut la retaper à chaque ouverture / changement d'onglet.
Une meilleure solution pour les fichiers que l'on traite régulièrement est d'ajouter cette ligne à la fin du `.vimrc` :

{{< highlight vim >}}
" Dans mon cas pour le markdown
autocmd BufRead,BufNewFile *.md set filetype=markdown
{{< / highlight >}}

Cette astuce nécessite d'avoir activé `autocommands` et d'avoir une version complète de VIM.
Les utilisateurs d'une distribution Linux qui installe `vim-tiny` sauront de quoi je parle.

## Source

[Stack Overflow : Enable syntax highlighting for various filetypes in vim](https://stackoverflow.com/questions/5437471/enable-syntax-highlighting-for-various-filetypes-in-vim)

