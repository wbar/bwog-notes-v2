+++
tags = ["vim"]
title = "Aide mémoire VIM"
date = "2014-04-17"
modified = "2024-02-08T08:40:00+01:00"
draft = false
show_toc = true
+++

Une (asez longue en fait) liste de commandes VIM pour les retrouver facilement.
<!--more-->

Les commandes sont à effectuer en mode interactif (appuyer sur échap pour y revenir).
Autre pré-requis, avoir désinstallé vim-tiny (si ce dernier est installé par défaut sur votre OS) et installé vim.

## Configurer

Le fichier de base est /etc/vim/vimrc.
Il faut le copier dans son home et le renommer en .virmc pour que les modifs soient locale à l'utilisateur qui va utiliser l'éditeur.
Éditer ce fichier et voir les options intéressantes.
Un exemple de fichier de config se trouve sur mon dépôt git nommé `dotfiles`.

## Sauver / quitter

`:w` pour sauver.

`:q` pour quitter.

`:wq` pour sauver et quitter.

Pour forcer l'action, ajouter un `!` en fin de commande. Cela supprime les éventuelles alertes.

Et sinon `:wa :xa :qa` pour respectivement tout sauver, tout sauver et quitter, tout quitter sans sauver.

## Édition

{{< keys "i" >}} pour passer en mode insertion (_avant_ le caractère courant).

{{< keys "I" >}} pour aller en début de ligne puis passer en mode insertion _avant_ le tout premier caractère.
Remplace avantageusement la combinaison `0i`.

{{< keys "a" >}} pour passer en mode ajout (« insertion » mais _après_ le caractère courant).
Par rapport à {{< keys "i" >}} le curseur va donc se déplacer d'un caractère.

{{< keys "A" >}} pour aller en fin de ligne puis passer en mode insertion _après_ le tout dernier caractère.
Remplace avantageusement la combinaison `$a`.

## Insérer un caractère littéralement (exemple avec des tabulations)

En mode insertion, en fonction des réglages de Vim, l'appui sur Tab peut insérer des espaces.

En cas de besoin ponctuel de caractères de tabulation : faire {{< keys "Ctrl" "v" >}} puis {{< keys "Tab" >}}.

Fonctionne même avec [l'insertion en mode colonne]({{< ref "#insertion-en-mode-colonne" >}}).

## Revenir au mode normal

{{< keys "Esc" >}} ou bien {{< keys "Ctrl" "c" >}}.

## Déplacements

{{< keys "0" >}} pour aller en début de ligne.

{{< keys "$" >}} pour la fin de ligne.

{{< keys "w" >}} pour avancer mot par mot.

{{< keys "b" >}} pour reculer mot par mot.

{{< keys "G" >}} simple pour sauter à la dernière ligne.

`<num ligne>G` pour sauter à une ligne précise.
`:<num ligne>` fonctionne également.

`<num colonne>|` pour se déplacer dans la ligne au caractère voulu. Exemple : `12|` pour se déplacer sur le 12e caractère de la ligne.
Peut se chaîner avec le déplacement à une ligne. Exemple : `42G12|` pour aller sur le 12e caractère de la ligne numéro 42.

`gg` pour sauter à la première ligne.

{{< keys "Ctrl" "f" >}} Défiler d'une page vers le bas.

{{< keys "Ctrl" "b" >}} Défiler d'une page vers le haut.

{{< keys "Ctrl" "d" >}} Défiler d'une demi-page vers le bas.

{{< keys "Ctrl" "u" >}} Défiler d'une demi-page vers le haut.

{{< keys "H" >}} Placer le curseur en haut de la page.

{{< keys "L" >}} Placer le curseur en bas de la page.

{{< keys "M" >}} Placer le curseur au milieu de la page.

{{< keys "%" >}} en étant sur un caractère ouvrant (ex, `{`, `(`, `[`) pour sauter au caractère fermant associé.
Fonctionne aussi depuis un caractère fermant vers un caractère ouvrant.

`zz` Défiler pour placer la ligne courante au milieu de la fenêtre.

`zt` Défiler pour placer la ligne courante en haut de l'écran.

## Nouvelles lignes

Utiliser ces commandes fait automatiquement passer en mode insertion.

{{< keys "o" >}} Insérer une nouvelle ligne sous la ligne courante.

{{< keys "O" >}} Insérer une nouvelle ligne au dessus de la ligne courante.

_Remarque_ : Il s'agit de la lettre, pas du chiffre zéro.

## Mode visuel

Le mode visuel permet de sélectionner du texte (en déplaçant le curseur) pour ensuite le copier, le couper et le coller ensuite.

{{< keys "v" >}} pour passer en mode visuel.

{{< keys "V" >}} pour sélectionner toute une ligne d'un coup. Déplacer pour sélectionner plusieurs lignes.

{{< keys "Ctrl" "v" >}} pour sélectionner en mode colonne. Descendre ou monter pour travailler sur plusieurs lignes en même temps.

{{< keys "y" >}} pour copier la sélection.

{{< keys "x" >}} pour couper la sélection.

{{< keys "C" >}} pour couper la sélection et passer en mode insertion.

{{< keys "s" >}} pour entrer en mode substitution (voir ci-dessous).

### Insertion en mode colonne

Après avoir sélectionné un bloc, {{< keys "I" >}} fait passer en mode insertion.
Le texte tapé n'apparaitra que sur la première ligne.

Un double appui sur la touche {{< keys "Échap" >}} l'ajoutera aux autres lignes du bloc précédemment sélectionné.

_Remarque_ : L'indentation des lignes du bloc doit être cohérente.

### Substitution en mode colonne

Après avoir sélectionné un bloc, {{< keys "s" >}} active le mode substitution.
Le bloc sélectionné sera supprimé et le texte entré n'apparaitra que sur la première ligne.

Un double appui sur la touche {{< keys "Échap" >}} l'ajoutera aux autres lignes du bloc précédemment sélectionné.

## Split d'écran

`:sp` pour séparer horizontalement. Il est possible de spécifier un fichier.

`:vsp` pour séparer verticalement. Il est possible de spécifier un fichier.

{{< keys "Ctrl" "w" >}} ou {{< keys "Ctrl" "W" >}} puis une touche parmi
{{< keys "W" >}}, {{< keys "w" >}}, {{< keys "h" >}}, {{< keys "j" >}}, {{< keys "k" >}}, {{< keys "l" >}} sauter d'un split à l'autre.

Et pour redimensionner : {{< keys "Ctrl" "w" >}} puis {{< keys "-" >}} ou {{< keys "+" >}} pour retailler un split horizontal.

{{< keys "Ctrl" "w" >}} puis {{< keys "<" >}} ou {{< keys ">" >}} pour retailler un split vertical.

## Onglets

Remarque : Nécessite VIM 7.0 ou plus.

Il est possible de lancer plusieurs onglets au lancement de VIM via l'option `-p` avant de donner plusieurs noms de fichiers.

`:tabnew <fichier>` ouvrir un nouvel onglet contenant le fichier spécifié.
_Remarque_ : omettre le nom du fichier ouvre un nouvel onglet vide.

`gt` aller à l'onglet suivant.

`gT` aller à l'onglet précédent.

## Complétion de mots

Une fois n'est pas coutume, pour ces deux raccourcis, il faut être en mode insertion.

{{< keys "Ctrl" "n" >}} rechercher les mots déjà écrits après le curseur.

{{< keys "Ctrl" "p" >}} rechercher les mots déjà écrits avant le curseur.

Si VIM fait plusieurs propositions, refaire le raccourci jusqu'à obtenir le bon mot.

## Couper/Copier, coller et effacer

### Couper des lignes complètes

`dd` pour couper une ligne entière. Il est possible d’indiquer le nombre de lignes. Exemple `8dd` coupera 8 lignes.

La commande `dd` supporte également les intervalles, en passant par le mode commande.
La syntaxe est :

```vim
:<début>,<fin>d
```

Par exemple, `:4,12d` va supprimer les lignes 4 à 12.
Les raccourcis `.` pour la position du curseur et `$` pour la fin de fichier sont utilisables.

Exemples pour supprimer du curseur jusqu'à la fin, et l'inverse :

```vim
" position jusqu'à fin du fichier
:.,$d
" à l'inverse, du début jusqu'à la position (incluse)
:0,.d
```

`:%d` pour vider le tampon (tout supprimer).

`dgg` ou `dG` pour supprimer la ligne courante jusqu'au début du fichier, ou jusqu'à la fin. (voir déplacements).


### Couper dans une ligne

`dw` pour couper un mot du curseur jusqu'au prochain espace.
Comme pour `dd` on peut spécifier le nombre de mots (exemple : `d3w`).
Si le curseur est au milieu d’un mot, la fin du mot sera effacée.

`db` pour l'inverse : couper un mot du curseur jusqu'à l'espace précédent.
On peut également spécifier un nombre de mots (exemple : `d3b`)

`d0` ou `d$` pour supprimer le début ou la fin de ligne à partir du curseur (voir déplacements).

### Copier des lignes

`yy` pour copier une ligne entière. Les équivalents `yw, yb, ygg, yG, y0 et y$` existent aussi (voir `dd`).
Les intervalles fonctionnent également.

`yi<sep>` copie tout le contenu du bloc délimité par le séparateur `<sep>`.
Fonctionne sur tout type de caractère ouvrant/fermant. Parenthèses, crochets, accolades, guillemets, etc...

### Coller

{{< keys "p" >}} pour coller le contenu après le curseur.
Il est possible de coller plusieurs fois le contenu.
Exemple `5p` collera 5 fois les caractères sauvés.

{{< keys "P" >}} pour coller le contenu avant le curseur.
Possibilité de spécifier le nombre de fois également.

### Supprimer des caractères

{{< keys "x" >}} pour supprimer le caractère situé **sous** le curseur.

{{< keys "X" >}} pour supprimer le caractère situé **avant** le curseur.

`<nombre>x` pour supprimer plusieurs caractères depuis le curseur et vers la fin de la ligne.

`<nombre>X` pour supprimer plusieurs caractères depuis celui placé **avant** le curseur et vers le début de la ligne.

## Supprimer des lignes entières en fonction d'un motif

Une autre forme, héritée de `ex` et `sed` est la suivante :

```vim
:[range]g/{pattern}/[cmd]
```

Elle agit sur les lignes entières, et par défaut sur tout le fichier si `[range]` n'est pas spécifié.

Avec la commande `d`, on va pouvoir supprimer toutes les lignes qui contiennent le motif entré.

```vim
:g/motif/d
```

Et pour faire l'inverse, ne garder que les lignes contenant le motif voulu :

```vim
:g!/motif/d
```

Ou bien, en pensant à `grep -v` :

```vim
:v/motif/d
```

Exemples :

- Supprimer toutes les lignes qui contiennent le caractère « ` » :

```vim
:g/`/d
```

- Ne garder que les lignes de cet article contenant « vim » :

```vim
:v/vim/d
```

En passant, si `[cmd]` n'est pas spécifié, c'est `p` qui est utilisé,
pour afficher les lignes qui correspondent.
Pratique pour tester ses motifs de recherche !

Pour plus de détails :

```vim
:help :v
:help :g
```

## Remplacer

{{< keys "r" "<carac>" >}} après avoir placé le curseur sur le caractère à remplacer. Le caractère surligné sera remplacé.

{{< keys "R" >}} pour passer en mode de remplacement. Taper directement le mot à remplacer, appuyer sur échap pour quitter le mode de remplacement.

{{< keys "s" >}} pour supprimer le caractère sous le curseur et passer immédiatement en mode insertion.

{{< keys "S" >}} pour supprimer la ligne courante et passer immédiatement en mode insertion.

{{< keys "C" >}} pour supprimer à partir du curseur jusqu'à la fin de la ligne. Passe ensuite automatiquement en mode insertion.

`ce` pour supprimer du curseur jusqu'à la fin du mot. Passe ensuite en mode insertion.

`cw` pour supprimer un mot et passer immédiatement en mode insertion.

`ci<sep>` supprime tout le contenu du bloc délimité par le séparateur sep et passe en mode insertion. Fonctionne sur tout type de caractère ouvrant/fermant. Parenthèses, crochets, accolades, guillemets, etc...

`:%s/unMot/unAtreMot/gc` substitution du premier mot par le second de manière globale avec demande de confirmation pour chaque occurrence.
Retirer le `c` final pour tout remplacer sans confirmation.
Retirer le `%` pour ne remplacer que sur la ligne courante.
[Petite remarque à propos des sauts de ligne]({{< ref "informatique/2020/002-vim-sautsligne-remplacement.md" >}}).

## Rechercher

`/<mot>` pour rechercher vers le bas du fichier.

`?<mot>` pour rechercher vers le haut.

{{< keys "*" >}} pour rechercher toutes les occurrences du caractère ou mot sur lequel le curseur est actuellement placé.

{{< keys "n" >}} pour aller au résultat suivant.

{{< keys "N" >}} pour le résultat précédent.

`:nohl` ou `:nohlsearch` pour arrêter de mettre les résultats en surbrillance.

## Passer une commande tout en étant en mode insertion

{{< keys "Ctrl" "o" >}} puis la commande.
Vim exécutera la commande donnée et en rendant la main, restera en mode insertion.

Exemple : `Ctrl+o zz`

## Explorer des fichiers

VIM possède un explorateur de fichiers intégré.
Le moyen le plus simple de l'invoquer est d'utiliser la commande `:Explore`.
Ceci va ouvrir l'explorateur à la place du fichier courant s'il n'a pas été modifié.
Pour de déplacer il suffit d'utiliser les flèches haut ou bas et d'appuyer sur la touche entrée pour commencer à éditer un fichier.

Une vue d'explorateur est comme un fichier en lecture seule.
La commande habituelle de fermeture s'applique ici également.

Il est également possible d'ouvrir VIM directement en vue explorateur en indiquant un dossier en argument lors du lancement.

### Variantes disponibles

`:Lexplore` ouvrira une vue à gauche.
Les fichiers sélectionnés pour l'édition s'ouvriront dans la vue d'origine.
Si le fichier a été fermé, une nouvelle vue à droite sera créée.

`:Texplore` ouvre un nouvel onglet pour afficher l'explorateur.

`:Vexplore` et `:Sexplore` ouvriront l'explorateur dans respectivement une vue verticale et horizontale.

### Commandes disponibles dans l'explorateur

{{< keys "i" >}} bascule entre les différents types d'affichage disponibles.

{{< keys "o" >}} ouvre une vue en séparation horizontale avec le fichier sélectionné.

{{< keys "t" >}} ouvre un nouvel onglet avec le fichier sélectionné.

{{< keys "v" >}} ouvre une vue en séparation verticale avec le fichier sélectionné.

### Informations affichées

En fin de nom d'élément, certains symboles sont parfois ajoutés :

- `/` pour un dossier
- `*` pour un fichier exécutable
- `@` pour un lien

## Vimdiff

Appeller `vimdiff fichier_A fichier_B` va lancer vim de manière spéciale permettant de comparer deux fichiers.
Ces deux fichiers sont éditables comme n'importe quel autre.

De nouvelles commandes sont disponibles :

`do` : diff obtain : récupérer la différence depuis le second fichier vers l'actuel.

`dp` : diff put : envoyer la différence du fichier courant dans l'autre.

`[c` : Revenir à la différence précédente.

`]c` : Aller à la prochaine différence.

`:diffupdate` : recalculer les différences, mettre à jour leur affichage.

## Autres commandes utiles

`:make [options]` est possible directement dans vim.

{{< keys "u" >}} permet d'annuler des modifications. Possibilité d'annuler plusieurs fois. Exemple `4u` annulera les 4 dernières modifications.

{{< keys "Ctrl" "r" >}} permet de restaurer une annulation.

`!<commande>` (sans espace entre `!` et la commande) appeler une commande shell dans vim.

{{< keys "K" >}} appeler le man pour le mot sur lequel est placé le curseur.

## Forker / copier-coller en masse

`:sh` Forker un shell à partir de vim

`vi` fichier Editer un autre fichier

`:N,Mw autreFichier` Ecrire (w) de la ligne N à la ligne M dans le autreFichier

`:q!` Quitter le second vim.

`exit` Quitter le shell forké (revenir au premier vim).

`:r autreFichier` Insérer les lignes provenant de autreFichier (ne pas oublier de placer le curseur à la fin d'une ligne).

## Copier-coller depuis l'extérieur

Pour éviter les effets indésirables de type indentation automatique :

- `:set paste`
- passer en mode insertion
- coller son texte
- `:set nopaste`

Avant je faisais `:set noai`, mais d'après l'aide interne de vim, jouer avec `paste` est plus fiable.

## Changer la casse

Sélectionner des mots avec le mode visuel, puis (toujours en mode visuel) :

{{< keys "u" >}} : pour passer la sélection en minuscules.

{{< keys "U" >}} : pour passer la sélection en majuscules.

Sinon il y a aussi :

{{< keys "~" >}} : changer la casse du caractère sous le curseur.
Puis déplacer le curseur d'un cran vers la droite.
On peut ajouter un nombre devant pour traiter autant de caractères. 
Exemple : `5~` pour inverser la casse de 5 caractères, en commençant par celui sous le curseur.

`g~~` pour inverser la casse sur toute la ligne courante.

`guu` pour passer toute la ligne courante en minuscules.

`gUU` pour passer toute la ligne courante en majuscules.

## Aide

`:help <commande>` ouvrira l'aide associée.
La commande à entrer peut contenir le caractère `:`.
Exemple :

{{< highlight text >}}
:help :Explore
{{< / highlight >}}

## Encore plus d'astuces ?

- [Quick Vim Tips to Generate and Increment Numbers](https://irian.to/blogs/quick-vim-tips-to-generate-and-increment-numbers/)

_Remarque_ : cette astuce nécessitera peut-être de faire `:set nrformats+=alpha` pour jouer avec les caractère alphabétiques.


