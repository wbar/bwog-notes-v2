+++
Categories = [""]
tags = ["apache-httpd","selinux"]
title = "Reverse proxy Apache et services locaux"
date = "2014-07-24"
draft = false
+++

Avec Apache comme reverse proxy, les redirections entre back-ends (pourtant locaux) échouaient.
<!--more-->

En ce moment je travaille sur une application web assez simple au niveau architecture : 
un front-end en AngularJS et quelques services en Node.js à aller interroger.
Un serveur web se charge de faire la liaison entre les deux parties.

Dans un cas bien précis, le front-end appelle un des back-ends, qui en interroge un second, en passant par la case reverse proxy.
Le reste du temps le front appelle un service qui répond directement.
Jusqu'ici rien de révolutionnaire.

Là où ça devient intéressant, c'est à la migration de Nginx vers Apache (httpd pour les intimes de RHEL et dérivées).
Le cas particulier ne fonctionnait plus, et aucun indice quant à la cause du problème ne semblait vouloir se manifester.
Seule une erreur HTTP 503 remontée par le premier service m'était donnée sans plus d'explications.

Premier réflexe, aller lire le journal d'erreurs d'Apache.
Ce dernier indiquait laconiquement :

{{< highlight text >}}
[date] [error] (13)Permission denied: proxy: HTTP: attempt to connect to 127.0.0.1:5000 (ip du client) failed
[date] [error] ap_proxy_connect_backend disabling worker for (ip du client)
{{< / highlight >}}

Je n'ai pas prêté attention à la seconde ligne au début (erreur ...) et j'ai perdu du temps à re-vérifier plusieurs fois la configuration apache.
Ainsi que le bon fonctionnement du service ne répondant pas.

Après pas mal d'essais et après une dernière tentative (et un log d'erreurs remis à zéro pour faciliter la lecture), je me suis intéressé à l'énigmatique seconde ligne.
Quelques recherches rapides m'ont mené [à un vieux post de Server Fault](http://serverfault.com/questions/329355/issues-with-proxypass-and-proxypassreverse-when-proxying-to-localhost-and-a-diff)
relatant les mêmes problèmes.

SELinux bloquait en fait les requêtes du processus `httpd` dans le cadre d'un script l'appelant pour interroger un autre service web.
Il a ensuite _juste_ fallu autoriser le bon port dans la politique de sécurité :

{{< highlight shell >}}
semanage port -a -p tcp -t http_port_t 5000
{{< / highlight >}}

Et voilà !
Pas mal de temps perdu sur ça, mais des connaissances gagnées.

Je vous renvoie au manuel de `semanage` pour plus d'informations et à [cette procédure d'installation](http://www.cyberciti.biz/faq/redhat-install-semanage-selinux-command-rpm/)
si la commande n'est pas présente sur votre serveur.
