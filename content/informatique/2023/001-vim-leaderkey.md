+++
Tags = ["vim"]
title = "La « leader key » de Vim"
date = 2023-01-19T09:42:17+01:00
+++

Vim possède beaucoup de raccourcis clavier et permet même de créer les siens via les directives `map` et [toutes ses variantes](https://vimhelp.org/map.txt.html).
Cependant, comment faire pour en avoir des courts,
facile à retenir,
sans pour autant empiéter sur d'éventuels déjà existant ?

Une réponse proposée par Vim et que je n'ai découverte que très récemment est : la « leader key ».

C'est une touche spécifique permettant de préfixer ses raccourcis personnalisés,
afin d'avoir un risque de collision bien plus faible, voire nul.

Pour cela, Vim propose une séquence à placer dans nos configurations de raccourcis : `<Leader>`.
Par défaut, elle est paramétrée sur {{< keys "backslash" >}}.
Ce n'est pas très pratique sur des claviers non-QWERTY.

## Paramétrons

Dans notre `.vimrc`, il suffit d'affecter une valeur à la variable globale `mapleader` :

```vim
" vim 9 ou plus
let g:mapleader = ","
" sinon
let mapleader = ","
```

Si vous préférez la barre d'espace :
```vim
let g:mapleader = " "
```

Maintenant, on peut faire nos raccourcis. En voici 3 que j'utilise :

```vim
nnoremap <Leader>g :GrammalecteCheck<CR>
nnoremap <Leader>G :GrammalecteClear<CR>
nnoremap <Leader>h :nohl<CR>
```
Maintenant, je n'ai plus que 2 touches à presser pour utiliser ces commandes.
Youpi !

Notez au passage l'utilisation de `nnoremap` pour que le raccourci ne soit activable qu'en mode normal.
C'est pour éviter que la virgule ne soit interceptée pendant le mode insertion.
Si vous utilisez une autre touche, rien ne vous empêche d'utiliser une autre variante pour avoir ce raccourci utilisable dans tous les modes.

## Aller plus loin

Vous pouvez également paramétrer plusieurs touches différentes dans votre configuration.
Que ce soit pour avoir plus de raccourcis, ou bien pour avoir des exécutions différentes sur la même touche (activation puis désactivation d'une fonctionnalité par exemple).

Ceci est possible grâce à l'interprétation des raccourcis qui se fait à la lecture de sa déclaration.
Ainsi, en paramétrant une _leaderkey_ différente avant chaque utilisation de `map`, on peut faire, par exemple :

```vim
let mapleader = ","
nnoremap <Leader>g :GrammalecteCheck<CR>
let mapleader = "-"
nnoremap <Leader>g :GrammalecteClear<CR>
```

Ici j'ai donc comme commandes : `,g` pour lancer Grammalecte, et `-g` pour nettoyer le résultat.

## Et voilà !

Puisque ce mécanisme repose sur le mapping de touches,
il est possible d'utiliser n'importe quelle commande interne à Vim,
mais également vos fonctions personnalisées si vous en avez.
Ainsi que, comme montré dans l'exemple précédent, des commandes de plugin.

Pratique !

Attention toutefois au piège de vouloir (re)mapper tout et n'importe quoi.

## Plus d'informations

- `:help mapleader`
- Votre moteur de recherche favori avec les termes « vim leader key »[^1]

[^1]: Astuce de fainéant pour vous laisser choisir votre meilleure source sur le sujet, tellement il y en a (en anglais surtout).
