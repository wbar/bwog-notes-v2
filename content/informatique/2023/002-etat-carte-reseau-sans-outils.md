+++
Tags = ["shell","réseaux"]
title = "Avoir l'état des cartes réseaux sans programme externe"
date = 2023-02-23T08:16:24+01:00
+++


Sans utiliser les commandes classiques (`ip`, le NetworkManager, etc.),
il reste possible d'avoir l'état de ses cartes réseau.
Utile notamment en shell de secours ou autres environnements limités.
Voire script.

- `cat /proc/net/dev` 
Très visuel quoiqu'un peu indigeste, on aura là les statistiques de toutes les cartes d'un coup.
Difficile pour le scripting. Exemple : 

```txt
cat /proc/net/dev
Inter-|   Receive                                                |  Transmit
 face |bytes    packets errs drop fifo frame compressed multicast|bytes    packets errs drop fifo colls carrier compressed
    lo:3612535985016 1504871864    0    0    0     0          0         0 3612535985016 1504871864    0    0    0     0       0          0
  eth0:7018766441878 6754565612    0 4507    0   577          0 258427669 7457131069430 7742050090    0    1    0     0       0          0
  eth1:45188287103 531585456    0    0    0     0          0 259548075        0       0    0    0    0     0       0          0
  eth2:       0       0    0    0    0     0          0         0        0       0    0    0    0     0       0          0
  eth3:       0       0    0    0    0     0          0         0        0       0    0    0    0     0       0          0
 bond0:7063954728981 7286151068    0 4507    0   577          0 517975744 7457131069430 7742050090    0    1    0     0       0          0
```

Si on veut extraire les statistiques plus facilement via script,
il vaut mieux se tourner vers les fichiers présents dans `/sys/class/net/*/statistics/`.

- `cat /sys/class/net/INTERFACE/carrier` et `cat /sys/class/net/INTERFACE/operstate`
Peu pratique à la main si l'on a beaucoup d'interfaces, mais très pratique pour le scripting,
ces fichiers permettent d'avoir l'état du branchement de la carte.

Exemples (avec commandes bonus) :

```bash
$ grep "" /sys/class/net/*/carrier
/sys/class/net/bond0/carrier:1
/sys/class/net/eth0/carrier:1
/sys/class/net/eth1/carrier:1
grep: /sys/class/net/eth2/carrier: Argument invalide
grep: /sys/class/net/eth3/carrier: Argument invalide
/sys/class/net/lo/carrier:1
# ---
$ grep "" /sys/class/net/*/operstate
/sys/class/net/bond0/operstate:up
/sys/class/net/eth0/operstate:up
/sys/class/net/eth1/operstate:up
/sys/class/net/eth2/operstate:down
/sys/class/net/eth3/operstate:down
/sys/class/net/lo/operstate:unknown
```

Et enfin, au cas où, pour trouver l'adresse IP (attention, le contenu peut être indigeste) :

```bash
cat /proc/net/fib_trie
```

L'adresse physique de la carte sera dans : `/sys/class/net/INTERFACE/address`.

