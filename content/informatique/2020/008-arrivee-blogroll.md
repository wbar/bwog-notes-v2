+++
Tags = ["blog", "openring"]
title = "Arrivée d'un blogroll"
date = 2020-09-10T19:39:51+02:00
modified = 2023-09-05
draft = false
+++

_Mise à jour du 5 septembre 2023_ : Ne cherchez plus le blogroll en bas de mes articles,
il a été retiré au profit [d'une page avec plusieurs liens]({{< ref "005-au-revoir-blogroll" >}}) issus de mes flux RSS.

Je laisse toutefois la démarche utilisée à l'époque si vous souhaitez mettre ça en place de votre côté.

---

L'idée de faire des liens vers d'autres blogs me trotte dans la tête depuis un moment.
Après tout, le but des blogs c'est le partage, alors renvoyer vers d'autres adresses ça fait partie du truc.
Surtout si, de proche en proche, ça vous fait découvrir d'autres blogs qui vous intéressent.
Et les blogs indépendants, c'est toujours cool.

La question était : comment faire ça bien ?
Une réponse qui me satisfait est : avec [openring](https://git.sr.ht/~sircmpwn/openring).

<!--more-->

Openring est un petit binaire codé en Go.
Vous lui donnez quelques flux RSS en entrée, il vous sort (via un pitit template) un html à inclure où vous voulez sur votre site statique.
L'outil aura prit soin d'aller chercher le dernier article de chaque flux puis d'extraire les URLs, le titre et le début du contenu.
Voilà c'est tout.
Peu importe votre générateur.
Et ça c'est chouette !


J'ai décidé d'inclure ce résultat en bas des articles uniquement (pour le moment, on verra plus tard si ça change).
Je présente 3 sites issus d'une liste de mes RSS.
Il sont tirés au hasard via un script (appelé à la publication, mais pour tests on peut l'utiliser à la main),
honteusement piqué à Lord, que voici :

{{< highlight shell >}}
#!/usr/bin/env bash
# idée honteusement pompée sur
# https://lord.re/posts/181-deploiement-hugo/
set -euo pipefail

readonly BASEDIR=$(pwd)
readonly OPENRING="${HOME}/MesDocs/Developpement/openring"
selected_feeds=""

feeds=( $(awk '/openring/ { print $1 }' "${HOME}/.newsboat/urls") )

rand_feeds=( $(shuf -e "${feeds[@]}") )

choose_feeds(){
for index in 0 1 2
do
  selected_feeds="-s ${rand_feeds[$index]} $selected_feeds"
done
}

choose_feeds
echo $selected_feeds

${OPENRING}/openring -n 3 \
$selected_feeds \
< ${OPENRING}/in.html \
> ${BASEDIR}/layouts/partials/openring.html
{{< / highlight >}}

Le fichier `layouts/partials/openring.html` est dans mon gitignore.
Vu qu'il va changer à chaque publication, je ne vais pas m'embêter à le versionner.

Le template `in.html` de openring ressemble à ça, à date d'écriture de cet article :

{{< highlight go-html-template >}}
<aside class="blogroll">
    <h3>Ailleurs sur le ouaibe francophone</h3>
    <ul>
        {{range .Articles}}
        <li>
            <h4><a href="{{.Link}}" target="_blank" rel="noopener">{{.Title}}</a></h4>
            {{.Summary}}
            <br/>
            Via <a href="{{.SourceLink}}">{{.SourceTitle}}</a>
            le {{.Date.Format "2006-01-02"}}
        </li>
        {{end}}
    </ul>
    <p class="attribution">
    Généré avec
    <a href="https://git.sr.ht/~sircmpwn/openring">openring</a>
    </p>
</aside>
{{< / highlight >}}

Et donc, via un petit `{{ partialCached "openring.html" . }}` en bas du layout des posts, sous vos yeux ébahis, une liste de 3 autres blogs.
J'utilise `partialCached` car le contenu ne change pas entre les pages.

J'ai failli oublier le soupçon de CSS :

{{< highlight CSS >}}
.blogroll {border-bottom: 1px solid #b3b3b3; font-size:0.9rem;}
.blogroll ul {list-style:none; padding: 0px;}
.blogroll ul li {margin-bottom: 0.5rem;}
.blogroll .attribution {text-align: right; }
.blogroll h4 {margin-bottom:0px;}
{{< / highlight >}}

Bons sauts dans les interouaibes !

