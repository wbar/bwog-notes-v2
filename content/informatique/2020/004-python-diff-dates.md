+++
Tags = ["python"]
title = "Différence entre 2 dates en python"
date = 2020-06-16T22:01:10+02:00
draft = false
+++

Ce problème simple peut se poser de manière ponctuelle et pour le résoudre en python,
on a tout ce qu'il faut dans la bibliothèque `datetime`.
Elle permet directement de faire des soustractions (ou l'inverse au besoin) entre deux objets de ce type.
Pas besoin forcément de faire d'innombrables conversions via des timestamps (quoique ...).

Avec le résultat, la manipulation est facile vu qu'on a plétore de fonctions pour faire ce qu'on veut ensuite.

Alors oui, comme je viens de le dire c'est trivial, mais je n'ai pas trouvé ça simple à avoir en lisant la doc.
D'où ce petit partage rapide.

<!--more-->

Ci dessous un exemple complet, qui permet même d'avoir un affichage du type `Xj, Yh, Zm`.
Il prend en entrée des timestamps (parce que c'est avec ça que j'ai du travailler),
mais comme dit précédemment peu importe : des objets datetime peuvent directement faire l'affaire.

{{< highlight python >}}
from datetime import datetime

def _problem_duration(problem_start, problem_end):
    """Calculate diff (days, hours, minutes, seconds) between 2 timestamps
    :arg: problem_start
    :arg: problem_end"""
    pbstart = datetime.fromtimestamp(problem_start)
    pbend = datetime.fromtimestamp(problem_end)
    if problem_end == 0:
        pbend = datetime.now()

    duration_data = []

    duration = pbend - pbstart  # For build-in functions
    duration_in_s = duration.total_seconds()  # Total number of seconds between dates
    days = divmod(duration_in_s, 86400)  # Get days (without [0]!)
    hours = divmod(days[1], 3600)  # Use remainder of days to calc hours
    minutes = divmod(hours[1], 60)  # Use remainder of hours to calc minutes
    seconds = divmod(minutes[1], 1)  # Use remainder of minutes to calc seconds

    if days[0] > 0:
        duration_data.append("{}j".format(int(days[0])))
    if hours[0] > 0:
        duration_data.append("{}h".format(int(hours[0])))
    if minutes[0] > 0:
        duration_data.append("{}m".format(int(minutes[0])))
    if seconds[0] > 0:
        duration_data.append("{}s".format(int(seconds[0])))

    # "{} days, {} hours, {} minutes {} seconds".format(int(days[0]), int(hours[0]), int(minutes[0]), int(seconds[0]))
    return " ".join(duration_data)
{{< /highlight>}}

Alors oui, dans la seconde partie de la fonction, après la soustraction, on fait des calculs.
Pour l'affichage final surtout.
En fonction du but recherché, on est pas obligé de tout faire.

## Source

[Un post sur Stackoverflow](https://stackoverflow.com/a/47207182) datant de 2017, mis à jour en 2019

