+++
tags = ["ssh","rsync"]
title = "Alias SSH avec Rsync"
date = 2020-04-08T11:25:57+02:00
draft = false
+++

La commande `rsync` sait travailler avec ssh.
Il est même possible de définir des options comme le port, l'utilisateur, une clef, etc.
Jusque là, rien de nouveau.

Par contre si on ne fait pas gaffe, on se retrouve vite avec des commandes longues, du style :

{{< highlight shell >}}
rsync -av --delete -e 'ssh -p 2222 -i ~/.ssh/ma_clef' ${PUBLICDIR} user@mon_host:${REMOTE_WEBDIR}
{{< / highlight >}}

Quand bien même ces lignes sont placées dans des scripts (ou des alias shell ... beuark),
ça va rapidement devenir un enfer à gérer en cas de changement de paramètres.

Heureusement, on peut configurer ssh pour se faciliter la vie.
Et rendre le tout bien plus propre aussi.

<!--more-->

Pour tout avouer, j'ai longtemps fait ça sans rien changer ...
Même après avoir vu passer [l'article de Lord à ce sujet](https://lord.re/fast-posts/48-alias-ssh/).
Mais aujourd'hui, je me suis enfin décider à nettoyer tous ces scripts et à les rendre plus propre.

Même s'il y a énormément d'options possibles, concrètement c'est très simple à faire.
Dans mon cas, un simple `vim ~/.ssh/config` avec le contenu suivant[^1] :

{{< highlight txt >}}
Host mon_host
    HostName mon_host.tld (ou bien l'IP)
    User user
    Port 2222
    IdentityFile ~/.ssh/ma_clef
{{< / highlight >}}

Après ça, la ligne dans les script devient beaucoup plus courte et lisible :

{{< highlight shell >}}
rsync -av --delete -e ssh ${PUBLICDIR} mon_host:${REMOTE_WEBDIR}
{{< / highlight >}}

Et pour se connecter en ssh, on peut directement faire `ssh mon_host`.
Bonus sympa : il y a (en fonction de la configuration de votre shell) de l'auto complétion sur le nom défini dans l'alias.

Plus d'excuses pour ne pas en faire !

[^1]: Ouais enfin un exemple hein, n'exagérons pas !

