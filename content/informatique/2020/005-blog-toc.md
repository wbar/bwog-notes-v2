+++
Tags = ["blog", "hugo"]
title = "L'arrivée des tables des matières dans les articles"
date = 2020-07-01T17:55:37+02:00
draft = false
+++

Petite mise à jour du blog : j'ai ajouté une table des matières sur certains articles un peu longs.
Notamment les quelques aides mémoires que j'ai pu rédiger au fil du temps.

Au passage, celle qui était sur la page « En Vrac » indique maintenant « contenu » ... Comme sur les articles.

<!--more-->

La liste des articles qui ont été modifiés :

- [Aide mémoire vim]({{< ref "/informatique/2014/vim.md" >}})
- [Aide mémoire Shell]({{< ref "/informatique/2015/shell.md" >}})
- [Aide mémoire Bash]({{< ref "/informatique/2018/bash-cheatsheet.md" >}})

Et pour l'activer, si jamais vous voulez faire pareil, voici comment j'ai fait.

- Dans mes templates j'ai rajouté :

{{< highlight go-html-template >}}
{{ with .Params.show_toc }}
    <div id="toc">
        <h2>Contenu</h2>
        {{ $.TableOfContents }}
    </div>
{{ end }}
{{< / highlight >}}

- Dans le préambule des articles où je veux l'afficher j'ajoute juste :

{{< highlight toml >}}
show_toc = true
{{< / highlight >}}

