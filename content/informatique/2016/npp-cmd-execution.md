+++
tags = ["npp", "logiciel libre"]
title = "Excecution d'une commande dans notepad++"
date = "2016-02-19"
lastmod = "2016-02-25"
draft = false
+++

Petite astuce pour exécuter un script python depuis notepad++ sans plugin.
<!--more-->

J'utilise le plugin de preview HTML et un script python pour avoir un rendu des fichiers markdown que j'écris sous notepad++,
et j'ai eu besoin d'exporter une version du contenu.

Plutôt que de modifier mon script existant et d'installer un plugin pour le lancer, je suis passé par l'option d'éxécution de commandes intégrée.
Voici comment.

Depuis le menu il faut aller dans : Exécution > Exécuter, puis Entrer la ligne de commande désirée.
Exemple :

{{< highlight bat >}}
cmd /C C:\Python34\python.exe C:\workspace\divers\mkdn2htmlPlusCSS.py "$(FULL_CURRENT_PATH)" > "$(FULL_CURRENT_PATH).html"
{{< / highlight >}}

Exécuter au moins une fois pour voir.
Si le résultat est bon, cliquer sur Enregistrer.
Une fenêtre demandant un nom pour la commande et les raccourcis à lui associer (étape optionnelle) va s'ouvrir.

La commande se retrouvera alors dans la liste du menu Exécution et pourra être relancée à volonté.

_Remarque_ : à partir de ce point, il ne sera plus possible de changer la commande.
Juste le nom et les raccourcis éventuels.

