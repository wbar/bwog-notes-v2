+++
Categories = [""]
tags = ["rpm"]
title = "RPM : Dépendance non satisfaite sur un chemin"
date = "2016-02-29"
draft = false
+++

En voulant installer un paquet sur un système Linux utilisant RPMv5, je suis tombé sur un problème apparemment rare : il y avait une dépendance non satisfaite sur un chemin.
<!--more-->

Sur un serveur tournant sous Linux, j'ai voulu installer `htop`.
Le système est un peu particulier, mais utilise néanmoins RPM, en version 5.

## Le problème et sa solution

Pour l'installation proprement dite, j'ai récupéré une archive et lancé la
commande de test :

{{< highlight shell >}}
root@localhost:~# rpm -Vvp htop-1.0.3-1.el5.rf.x86_64.rpm
warning: htop-1.0.3-1.el5.rf.x86_64.rpm: Header V3 DSA signature:
NOKEY, key ID 6b8d79e6
missing     /usr/bin/htop
missing     /usr/share/applications/htop.desktop
missing     /usr/share/doc/htop-1.0.3
missing   d /usr/share/doc/htop-1.0.3/AUTHORS
missing   d /usr/share/doc/htop-1.0.3/COPYING
missing   d /usr/share/doc/htop-1.0.3/ChangeLog
missing   d /usr/share/doc/htop-1.0.3/INSTALL
missing   d /usr/share/doc/htop-1.0.3/NEWS
missing   d /usr/share/doc/htop-1.0.3/README
missing   d /usr/share/man/man1/htop.1.gz
missing     /usr/share/pixmaps/htop.png
Unsatisfied dependencies for htop-1.0.3-1.el5.rf.x86_64:
/usr/share/applications
/usr/share/doc
{{< / highlight >}}

Curieux comme erreur, mais je tente tout de même en pensant que le paquet va créer de lui-même les dossiers.

{{< highlight shell >}}
root@localhost:~# rpm -ivh --test htop-1.0.3-1.el5.rf.x86_64.rpm
warning: htop-1.0.3-1.el5.rf.x86_64.rpm: Header V3 DSA signature:
NOKEY, key ID 6b8d79e6
error: Failed dependencies:
/usr/share/applications is needed by htop-1.0.3-1.el5.rf.x86_64
/usr/share/doc is needed by htop-1.0.3-1.el5.rf.x86_64
{{< / highlight >}}

Raté.

Je m'occupe à la main de la création des dossiers en pensant que cela va régler le problème :

{{< highlight shell >}}
mkdir /usr/share/applications
mkdir /usr/share/doc
{{< / highlight >}}

Je relance ensuite la commande d'installation.
Toujours la même erreur.

Après pas mal de recherches je trouve une solution : ajouter les chemins dans le fichier `/etc/rpm/sysinfo/Dirnames`.
Je m'exécute et lance à nouveau la commande.

Victoire ! Htop est installé et fonctionne convenablement.

Pour tester, j'ai désinstallé le paquet, supprimé les dossiers créée manuellement et relancé l'installation
(en les laissant déclarés dans `Dirnames`).
Il était inutile de les créer manuellement.

## Réflexions après coup

Htop fonctionne correctement, rien à redire.
Mais la méthode n'est peut être pas des plus élégantes, et la solution n'est sûrement pas pérenne.
D'autant plus que le paquet est prévu pour RHEL 5.
Des paquets plus récents pourraient ne pas être installables même avec cette méthode.

Dans le cas d'une installation à répliquer sur de multiples serveur il serait sans doute mieux de récupérer (si possible) les sources,
de compiler le programme à partir d'une machine, et de distribuer ensuite une archive classique sur les autres.
Ou plus simplement de re-packager l'application.


## Sources
[Liste de diffusion de RPMv5, message daté de 2011](http://rpm5.org/community/rpm-users/0671.html)

[Les deux versions de RPM, article Wikipedia](https://en.wikipedia.org/w/index.php?title=RPM_Package_Manager&oldid=703893908#Forks)

