+++
Tags = ["blog"]
title = "Changement des couleurs"
date = 2025-02-14T12:00:00+01:00

[comments]
  show = false
  host = "pleroma.chagratt.site"
  id = "a_changer"

+++

Ça fait un moment que j'en avais assez des couleurs employées sur ce site.
Trop austère finalement.

Je voulais tenter autre chose à la base,
avec du marron type chocolat,
mais je n'ai pas réussi à faire quelque chose d'intéressant pour moi.
Malgré des tests avec Gruvbox, Solarized ou Monokai.

À deux doigts de laisser tomber encore une fois par manque d'idée et de résultat probant (hé, designer, c'est un vrai métier !),
je suis tombé sur [Catppuccin](https://catppuccin.com/).
Après quelques tests, j'ai trouvé le bon mélange pour moi.
Sobre comme je le souhaitais, mais un poil plus joyeux à l'œil.

Et hop !

À noter que je n'ai pas oublié le thème clair !
Même si les changements sont moins visibles dessus, en dehors des liens.

Pour garder une trace des changements,
pour une fois,
parce que je ne l'ai pas fait lors des changements précédents (_insérez un petit regret_),
voici des captures d'écran avant (à gauche) / maintenant (à droite).

Le thème sombre : 
{{< image src="changement.png" caption="Avant (à gauche), maintenant (à droite). Avant c'était plutôt gris acier et jaune terne, voire marron. Les liens et les emphases sont d'un jaune similaire. Maintenant, un peu plus bleuté / indigo en fond et coloré sur le reste : les liens sont en tons de vert d'eau par exemple, les emphases en jaune. Moins austère donc." >}}

Le thème clair :
{{< image src="changement-clair.png" caption="Avant (à gauche), maintenant (à droite). Avant c'était plutôt bleuté en fond pour coller aux liens. Maintenant, c'est plus neutre, les liens sont verts, et les emphases sont orangées au lieu de rouge." >}}

## Liens

- [Les palettes Catppuccin](https://catppuccin.com/palette)
- [Le guide d'utilisation des palettes](https://github.com/catppuccin/catppuccin/blob/main/docs/style-guide.md)

