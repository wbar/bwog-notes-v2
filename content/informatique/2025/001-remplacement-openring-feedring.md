+++
Tags = ["blog","openring", "feedring"]
title = "Remplacement de Openring par Feedring"
date = 2025-01-02T12:09:40+01:00

[comments]
  show = false
  host = "pleroma.chagratt.site"
  id = "a_changer"

+++

Depuis quelque temps,
j'ai remarqué qu'un des flux sur la page Ailleurs était tout en bas avec une date farfelue,
alors que la date de publication était récente.
Probablement parce que le format ou le champ de date n'était pas correctement renseigné.
Ou reconnu par les parseurs.

Pourtant, dans mon lecteur de flux RSS, aucun souci.
J'ai même été voire rapidement la norme,
et il n'y avait pas de problème dans le format du champ.

Le problème venait donc bien de openring.
Ou d'une des bibliothèques utilisées pour traiter les flux lus.
Allons bon !

Après avoir réfléchi et cherché,
je me suis plutôt tourné vers un remplaçant plus récent : feedring.

J'aurais pu tenter de bidouiller openring puis envoyer un patch,
mais n'étant pas développeur et voyant que le projet n'a pas bougé depuis plus d'un an,
j'ai préféré changer.
Du coup, je n'ai maintenant plus de soucis sur les dates des articles.
En plus, feedring semble mieux gérer le fait de lire un fichier contenant une liste d'url qu'openring
(ou alors j'ai mal testé ... qui sait ?).

Youpi.

## Quelques modifications toutefois

Sur le script de récupération déjà, qui est maintenant bien plus simple je trouve :

```shell
#!/usr/bin/env bash
set -euo pipefail

readonly BASEDIR=$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")
readonly BLOGDIR="${BASEDIR}/blog"
readonly FEEDRING="${HOME}/MesDocs/Developpement/go/feedring"
readonly FEEDS_FILE="${BASEDIR}/feeds.txt"

/usr/bin/awk '/openring/ { print $1 }' "${HOME}/.newsboat/urls" > "${FEEDS_FILE}"
nfeeds=$(/usr/bin/cat "${FEEDS_FILE}" | /usr/bin/wc -l)

${FEEDRING}/feedring -n ${nfeeds} \
-i "${FEEDS_FILE}" \
-t "${FEEDRING}/in-webring.html" \
-o "${BLOGDIR}/layouts/partials/webring.html"
```

Et une légère modification du template, pour s'adapter aux nouveaux noms des variables utilisées :

```go-html-template
{{ range .Posts }}

<article class="card">
  <header>
    <h1><a href="{{ .Link }}">{{ .Title }}</a></h1>
    <p>
            Via <a href="{{ .SourceLink }}">{{ .SourceTitle }}</a>
            le {{ .Date.Format "2006-01-02" }}
    </p>
  </header>
  <p>{{ .Summary }}</p>
</article>

{{ end }}
```


## Liens

- [Dépôt git de feedring](https://codeberg.org/s10a/feedring)
- [Dépôt git de openring](https://git.sr.ht/~sircmpwn/openring)
- [Comment c'était avec openring]({{< ref "005-au-revoir-blogroll.md" >}})

