+++
Categories = [""]
tags = ["javascript"]
title = "Forcer uniquement certains tests unitaires avec Jasmine"
date = "2015-01-28"
draft = false
+++

Avec `iit` ou `ddescribe` il est possible d'exclure tous les autres tests unitaires.
<!--more-->

Avec le framework de tests unitaires Jasmine en Javascript, il est possible d'empêcher tous les autres tests de s'exécuter.
Utile lors de l'écriture / du débug des tests.

Pour cela il suffit de doubler la première lettre des méthodes :

- `it()` devient `iit()`
- `describe()` devient `ddescribe`

*Attention* à ne pas oublier de retirer la double lettre avant de faire un commit, sinon surprise !

