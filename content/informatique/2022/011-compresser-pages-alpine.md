+++
Tags = ["nginx","alpine"]
title = "Compresser ses pages : Alpine"
date = 2022-12-21T07:40:00+01:00
modified = 2023-11-14
+++

Cette histoire commence à avoir des titres aussi interchangeables que ceux des
[adaptations de Resident Evil par Paul W.S Anderson](https://www.imdb.com/list/ls025503255/) ...

Bref, j'ai donc activé la compression en brotli hier soir.
Comme pour gzip, je pré-compresse avant de servir le contenu.
J'ai donc commencé par ajouter une ligne qui va bien dans mon script de publication :

```sh
find "${PUBLICDIR}" -type f -regex '.*\.\(htm\|html\|txt\|csv\|xml\|svg\|js\|json\|css\)$' -exec brotli -f -k {} \;
```

`brotli` était déjà sur mon système, installé en tant que dépendance de `curl`.

Si vous voulez paralléliser tout ça, vous pouvez jouer avec `xargs`[^1], en passant bien l'option `-print0` à `find` et `-0` à `xargs` :

```sh
readonly CORES=$(grep -c '^processor' /proc/cpuinfo)
find "${PUBLICDIR}" -type f -regex '.*\.\(htm\|html\|txt\|csv\|xml\|svg\|js\|json\|css\)$' -print0 | xargs -0 -P ${CORES} brotli -f -k
```


Ensuite, après avoir (spoiler : mal) vérifié avec `nginx -V` que brotli était présent, j'ajoute simplement un `brotli_static on;` dans la configuration sur le serveur.
J'envoie le tout, puis je redémarre nginx :

```sh
rc-service nginx stop
rc-service nginx start
# COIN !
```

Allons bon, nginx refuse de redémarrer en prétextant une erreur de type directive inconnue sur `brotli_static`.
[Une rapide recherche donne le paquet supplémentaire à installer](https://pkgs.alpinelinux.org/package/v3.17/main/x86_64/nginx-mod-http-brotli) :

```sh
apk add nginx-mod-http-brotli
rc-service nginx start
```

Et voilà !

## Observations et réflexions

Le gain de place des fichiers compressés en brotli est assez intéressant, voyez plutôt :

{{< fold "Tailles de quelques fichiers, en Ko" >}}
{{< highlight "txt" >}}
  8133 	 10-erreurs-balade-moto.txt
  3330 	 10-erreurs-balade-moto.txt.br
  3609 	 10-erreurs-balade-moto.txt.gz
  
  1467 	 check-list-fest.txt
   698 	 check-list-fest.txt.br
   746 	 check-list-fest.txt.gz
  
  338 	 favicon.svg
  179 	 favicon.svg.br
  251 	 favicon.svg.gz
  
  7711 	 index.html
  1635 	 index.html.br
  1989 	 index.html.gz

216450 	 index.json
 67035 	 index.json.br
 77608 	 index.json.gz

 84006 	 index.xml
 15587 	 index.xml.br
 17653 	 index.xml.gz
 
 17665 	 sitemap.xml
  2298 	 sitemap.xml.br
  2654 	 sitemap.xml.gz
{{< / highlight >}}
{{< /fold >}}

Par contre, vu que je pré-compresse et propose maintenant 3 formats,
ça prend forcément _un peu_ plus de place sur le disque.
Mais dans mon cas, c'est négligeable.
Il y a un côté plus amusant qu'utile pour un petit site statique à proposer des fichiers déjà compressés plutôt que de le faire à la volée.

Je remarque qu'à la compression sur mon PC, ça prend un peu plus de temps.
Cela dit sur le temps de décompression, je n'ai pas remarqué ni calculé pour voir ce qui était le plus rapide.
[Il y a moult comparatifs sur internet](https://duckduckgo.com/?q=benchmark+gzip+brotli).

Utiliser la charge de ma machine à la publication plutôt que celle sur serveur est néanmoins une décision que je trouve pertinente.
Même si le serveur mettra en cache la ressource compressée, c'est toujours ça d'économisé.
Surtout sur des petites machines.
Mettre à disposition gzip **et** brotli, je ne sais pas si ça vaut réellement le coup.
Au moins, vu la majorité de navigateurs qui supportent ce format,
je peux toujours me dire que j'aide (à mon échelle) à économiser de la bande passante,
surtout pour les personnes sur mobile.

## Liens

- [L'épisode 1]({{< ref "compresser-ses-pages-c-est-le-bien.md" >}})
- [L'épisode 2]({{< ref "009-compression-pages-le-retour.md" >}})

[^1]: Il y a certainement moyen de faire mieux à l'aide de [GNU Parallel](https://www.gnu.org/software/parallel/), via un makefile, ou un autre outil mais je n'ai pas encore testé. Un jour peut-être.

