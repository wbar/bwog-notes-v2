+++
title = "Quelle est l'article / la série de ton blog que tu apprécies le plus ?"
date = 2024-10-14T15:38:25+02:00

[comments]
  show = false
  host = "pleroma.chagratt.site"
  id = "a_changer"

+++

Pour moi c'est mon tout premier, mon [aide mémoire Vim]({{< ref "/informatique/2014/vim.md" >}}).

C'est cet article qui m'a réellement mit en ligne et donné envie de noter pour retenir,
structurer et partager mes explications et observations.

Ce n'est pourtant qu'une bête liste de combinaisons de touches et de commandes,
sans vraiment d'explications,
ni aucun développement comme sur les plus récents,
mais c'est comme ça.
Sans lui, il n'y aurait pas eu les autres.

- [Bloguidien, chaque jour un nouveau sujet pour votre blog](https://bloguidien.fr/)

