+++
title = "Quel est ton setup pour écrire ?"
date = 2024-10-17T10:39:32+02:00

[comments]
  show = false
  host = "pleroma.chagratt.site"
  id = "a_changer"

+++

## Prompt complet

Quel est ton setup pour écrire ? logiciel / matériel spécifique / lieu / musique / ambiance / isolement / parsemé de pauses

## Réponse

L'avantage de ce prompt, c'est qu'il peut concerner ce blog et [Pintes et Growls](https://pintes-et-growls.chagratt.site/).

Au niveau logiciel,
je tape tout sous Vim et je m'aide de l'excellent Grammalecte.
Puis le résultat est généré par Hugo, ce qui me permet de vérifier le résultat visuel avant de publier.

Je n'ai pas de matériel spécifique, si ce n'est un carnet LEUCHTTURM1917 taille A5 avec les pages à repères en pointillés, qui me sert parfois de pré-brouillon.

En termes de lieu, je rédige chez moi avec de la musique en fond.
Parfois sur les enceintes, mais surtout avec le casque,
pour un isolement mental complet.
Ça arrive parfois que je note des choses dans mon carnet,
et là, ça peut se passer n'importe où.

Pour ma manière de rédiger,
c'est là que la différence entre ce blog et l'autre arrive.
Pour mes articles techniques, je rédige en général d'une seule traite,
puis je fais une espèce d'interruption quand je cherche les liens exacts que je vais inclure en sources.
Je repasse ensuite sur l'aperçu généré par Hugo, je corrige les fautes et problèmes éventuels de rédactions puis j'envoie.
Pour des articles techniques,
c'est bien plus long pour moi de trouver le sujet que de le rédiger.

Pour pintes et growls c'est différent.
Là, c'est vraiment parsemé de pauses.
Je débute par un premier brouillon qui ressemble plus à une liste à puces qu'autre chose en même temps que je fais une première écoute.
Je laisse reposer au moins une heure,
puis je reviens développer chaque point.
Encore une pause, puis je découpe l'article.
Ça me permet de terminer le développement de mes idées en ciblant spécifiquement certaines chansons et leurs moments précis liés aux paragraphes.
Encore une pause, et je passe à la correction grammaticale et orthographique.
Une ultime et petite pause et je passe enfin à la relecture finale, une dernière correction et zou !

- [Bloguidien, chaque jour un nouveau sujet pour votre blog](https://bloguidien.fr/)

