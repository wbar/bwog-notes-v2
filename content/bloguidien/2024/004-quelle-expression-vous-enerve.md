+++
title = "Quelle expression/terme/approximation linguistique vous énerve ?"
date = 2024-11-17T15:37:35+01:00

[comments]
  show = false
  host = "pleroma.chagratt.site"
  id = "a_changer"

+++

Je pense que c'est quand quelqu'un dit « ça bug », alors que non, ce n'est pas ça.

Je vais toutefois nuancer.

Quand il s'agit d'une personne qui n'est pas dans l'informatique,
ou qui n'a pas beaucoup de connaissances sur le sujet,
je sais passer au-delà.
Je me contente alors juste (lorsque j'ai été sollicité pour aider) de décrire rapidement ce qu'il se passe réellement.
Par exemple je vais dire « ah en effet, c'est un peu lent. Ça lague, mais ce n'est pas un bug ».
Et ça s'arrêtera là si la personne en face ne me demande pas la différence.

En revanche quand en face j'ai quelqu'un de professionnel,
alors là j'ai du mal à faire l'impasse.
Et on peut rajouter des couches d'énervements.
Quand on me dit juste « ça bug » sans un minimum de détails, ou pire « il bug _ton_ truc ».
Surtout que dans 99% des cas que j'ai rencontrés jusqu'ici,
il s'agissait d'une erreur de configuration (d'un logiciel, du système, etc.).
C'est si dur que ça d'admettre juste « Je n'arrive pas à faire fonctionner ça comme je le voulais. Je ne vois pas d'où ça peut venir » ?
Ah c'est sûr,
c'est un peu plus long.

En fait, je pense que ce qui m'énerve,
c'est que « ça bug » est devenu une expression fourre-tout pour se dédouaner immédiatement,
et ainsi tenter de masquer sa flemme ou son manque de connaissances.
Pourtant, on a le droit de ne pas savoir.
Ça arrive.
Mais balancer salement le problème à quelqu'un d'autre sans chercher,
ça, non.
Je ne suis pas d'accord[^1].

... Oups, j'ai un peu dévié du sujet je crois. :)

- [Bloguidien, chaque jour un nouveau sujet pour votre blog](https://bloguidien.fr/)

[^1]: Toujours en contexte professionnel, hein !

