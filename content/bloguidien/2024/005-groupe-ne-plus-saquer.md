+++
title = "Quel groupe de musique adoriez-vous mais que vous ne pouvez plus saquer ?"
date = 2024-11-24T20:24:49+01:00

[comments]
  show = false
  host = "pleroma.chagratt.site"
  id = "a_changer"

+++


Je pense qu'il y en a deux : Sonata Arctica et Metallica.

Pour les deux cas, il me reste quelques morceaux en mode plaisir coupable,
mais globalement je n'en peux plus.

Pour Metallica,
c'est parce que pendant _des années_, mon père ne mettait que ça en fond sonore lorsque je faisais un saut là-bas.
Et _toujours_ le même album.
Au bout d'un moment, ça a été l'overdose, et j'ai rejeté tout le groupe en bloc.
Et puis, à chaque fois que quelqu'un de plus de 45 ans me voit c'est :
« Hooooo, un métalleux ! Tu connais Métallica ? T'aimes bien ? ».
Alors, oui, c'est une manière de briser la glace, ok.
Mais bon, les gars, même de cette période, y'en a eu d'autres hein. :)

Pour Sonata, c'est un peu différent.
C'est un pote de lycée qui me l'a fait découvrir via l'album Ecliptica.
D'une, un excellent album, et enfin mon pied à l'étrier pour passer du Hard-Rock au Métal.
Cependant, avec le temps, la sortie de Unia où je trouvais que ça sentait le pâté musicalement parlant,
et mon désintérêt total pour le Power et le Sympho à partir de 2010 ou 2012,
rideau !

- [Bloguidien, chaque jour un nouveau sujet pour votre blog](https://bloguidien.fr/)

