+++
title = "Quelle chanson tu adores de la part d'un groupe de musique que tu détestes ?"
date = 2025-01-07T18:25:04+01:00

[comments]
  show = false
  host = "pleroma.chagratt.site"
  id = "a_changer"

+++

Pfouh !
La question est difficile en vrai.
_Une_ chanson d'_un_ groupe ?

J'ai mis un peu de temps à choisir,
même en me limitant [à deux groupes dont j'ai déjà parlé sur ce thème]({{< ref "005-groupe-ne-plus-saquer.md" >}})
(tiens, une petite redondance ? :P).

Et la chanson gagnante est : Fullmoon, sur l'album Ecliptica.

Je ne développerai pas sur le fait que ça me rappelle des bons souvenirs.
Je vais juste conclure par : c'est fou tous ces paradoxes et toutes ces contradictions que l'on peut avoir.

- [Bloguidien, chaque jour un nouveau sujet pour votre blog](https://bloguidien.fr/)

